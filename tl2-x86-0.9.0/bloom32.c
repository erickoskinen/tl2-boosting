#include <stdio.h>
#include "bloom32.h"

/* ************************************************************* */

unsigned int sax_hash(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int h=0;
  for (i = 0; i < key_len; i++)
    h^=(h<<5)+(h>>2)+(unsigned char)key[i];
  return h;
}

unsigned int sdbm_hash(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int h=0;
  for (i = 0; i < key_len; i++)
    h=(unsigned char)key[i] + (h<<6) + (h<<16) - h;
  return h;
}

unsigned int jenkins(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int hash = 0;
 
  for (i = 0; i < key_len; i++) {
    hash += key[i];
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

/* ************************************************************* */

unsigned int bloom32_gethash(const int val) {
  unsigned int h = (jenkins(val)) | (sdbm_hash(val)) | (sax_hash(val));
  //printf("%d, %d, %d = %d\n", (jenkins(val)), (sdbm_hash(val)), (sax_hash(val)), h);
  return h;
}
