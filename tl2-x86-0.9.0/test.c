#include<stdio.h>
#include<string.h>

#include"bloom.h"

int main(int argc, char *argv[])
{
  FILE *fp;
  char line[1024];
  char *p;
  BLOOM *bloom;
	
  if(argc<2) {
    fprintf(stderr, "ERROR: No word file specified\n");
    return EXIT_FAILURE;
  }

  if(!(bloom=bloom_create(2500))) {
    fprintf(stderr, "ERROR: Could not create bloom filter\n");
    return EXIT_FAILURE;
  }
  
  if(!(fp=fopen(argv[1], "r"))) {
    fprintf(stderr, "ERROR: Could not open file %s\n", argv[1]);
    return EXIT_FAILURE;
  }

  while(fgets(line, 1024, fp)) {
    if((p=strchr(line, '\r'))) *p='\0';
    if((p=strchr(line, '\n'))) *p='\0';

    if(line[0] == '-') {
      bloom_remove(bloom, atoi(&line[1]));
      printf("removed %s\n", &line[1]);
    }
    else {
      bloom_add(bloom, atoi(line));
      printf("added %s\n", line);
    }
  }

  fclose(fp);

  while(fgets(line, 1024, stdin)) {
    if((p=strchr(line, '\r'))) *p='\0';
    if((p=strchr(line, '\n'))) *p='\0';
    
    if(!bloom_check(bloom, atoi(line))) {
      printf("No match for word \"%d\"\n", atoi(line));
    }
  }

  bloom_destroy(bloom);

  return EXIT_SUCCESS;
}
