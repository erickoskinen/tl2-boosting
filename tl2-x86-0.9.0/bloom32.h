#ifndef __BLOOM32_H__
#define __BLOOM32_H__

#include <stdlib.h>

typedef unsigned int bloom32_t;

unsigned int bloom32_gethash(const int val);

#define bloom32_clear(filter)      ((filter) = 0)
#define bloom32_add  (filter,h)    ((filter) |= (h))
#define bloom32_test (filter,h)    ((filter) ^ (h))
#define bloom32_union(dstf,otherf) ((dstf) |= (otherf))
#define bloom32_empty(filter)      ((filter) == 0)

#endif
