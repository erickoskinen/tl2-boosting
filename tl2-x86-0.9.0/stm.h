/* =============================================================================
 *
 * stm.h
 * -- User programmer interface for STM
 *
 * =============================================================================
 *
 * Created Aug. 2006 by Chi Cao Minh (caominh@staford.edu)
 *
 * =============================================================================
 */

#ifndef STM_H
#define STM_H 1

#include "tl2.h"

#ifdef __cplusplus
extern "C" {
#endif


#define STM_THREAD_T    Thread
#define STM_SELF        &_tm_thread_context->stm
#define STM_RO_FLAG     ROFlag
#define STM_THREAD_ID   UniqID
#define STM_JMPBUF_T  jmp_buf
#define STM_JMPBUF    buf

#define STM_VALID()     (1) /* always true since aborts cause immediate rollback */

#define STM_STARTUP()    TxOnce();
#define STM_SHUTDOWN()   TxShutdown();

#define STM_INIT_THREAD(thread, id)  TxInitThread(thread,id)
#define STM_NEW_THREAD(thread)       TxNewThread(thread)

#define STM_BEGIN(isReadOnly) \
    do { \
        STM_JMPBUF_T STM_JMPBUF; \
        int STM_RO_FLAG = isReadOnly; \
        sigsetjmp(STM_JMPBUF, 1); \
        TxStart(STM_SELF, &STM_JMPBUF, &STM_RO_FLAG); \
    } while (0) /* enforce comma */
#define STM_BEGIN_RD()  STM_BEGIN(1) /* These let the programmer...   */
#define STM_BEGIN_WR()  STM_BEGIN(0) /* ... provide hints to the STM. */
#define STM_ABORT()     TxAbort(STM_SELF)
#define STM_END()       TxCommit(STM_SELF)

#define STM_READ(var) \
    TxLoad(STM_SELF, (volatile intptr_t*)&(var))
#define STM_READ_FLOAT(var) \
    intptr2float(TxLoad(STM_SELF, \
                        (volatile intptr_t*)floatptr2intptr((float*)&(var))))
#define STM_READ_PTR(var) \
    intptr2voidptr(TxLoad(STM_SELF, \
                        (volatile intptr_t*)voidptr2intptr((void*)&(var))))
#define STM_WRITE(var, val) \
    TxStore(STM_SELF, (volatile intptr_t*)&(var), (intptr_t)(val))
#define STM_WRITE_FLOAT(var, val) \
    TxStore(STM_SELF, \
            (volatile intptr_t*)floatptr2intptr(&(var)), \
            float2intptr(val))
#define STM_WRITE_PTR(var, val) \
    TxStore(STM_SELF, \
            (volatile intptr_t*)voidptr2intptr(&(var)), \
            voidptr2intptr(val))
#define STM_LOCAL_WRITE(var, val) \
    TxStoreLocal(STM_SELF, (volatile intptr_t*)&(var), (intptr_t)val)
#define STM_LOCAL_WRITE_FLOAT(var, val) \
    TxStoreLocal(STM_SELF, \
                 (volatile intptr_t*)floatptr2intptr(&(var)), \
                 float2intptr(val))
#define STM_LOCAL_WRITE_PTR(var, val) \
    TxStoreLocal(STM_SELF, \
                 (volatile intptr_t*)voidptr2intptr(&(var)), \
                 voidptr2intptr(val))

#define STM_STERILIZE(base, len) \
    TxSterilize(STM_SELF, (void*)(base), (size_t)(len))

#define STM_LOCK(lock) \
  Lock(lock, STM_SELF);

#define STM_QLOCK_ALLOC(patience) \
  QLockKey_alloc(patience);

  //#define STM_QLOCK_ALLOC() QLockKey_alloc(QUEUELOCK_PATIENCE);

#define STM_QLOCK_FREE(ql) \
  QLockKey_free(ql);

#define STM_QLOCK_LOCK(ql,key,high,fullAbort) \
  QLockKey_Lock(STM_SELF, ql,key,high,fullAbort);

#define STM_QLOCK_UNLOCK(qlh) \
  QLockKey_Unlock(STM_SELF,qlh);

#define STM_UNLOCK(lock) \
  Unlock(lock, STM_SELF);

#define STM_HIGH() \
  TxHighThread(STM_SELF);

#define STM_DL_LOCK(lock) \
  DreadLock_Lock(lock, STM_SELF);

#define STM_STATS() \
  TxStats(STM_SELF);

#ifdef XBOOST_PARTIAL
#define STM_LOG_INVERSE(inverse, numArgs, args...) \
  ComputationLogInverse(STM_SELF, (void (*)(void))inverse, numArgs, ## args);
#define STM_LOG_ABSTRACTLOCK(lock) \
  ComputationLogLock(STM_SELF, lock);
#define STM_LOG_QLOCK(handle) \
  ComputationLogQLock(STM_SELF, handle);
#define STM_LOG_DREADLOCK(lock) \
  ComputationLogDreadLock(STM_SELF, lock);
#define STM_LOG_CHECKPOINT(ctx,i) \
  ComputationLogContext(STM_SELF,ctx,i);
#define STM_ROLLBACK_LAST() \
  ComputationLogRollback(STM_SELF,-1);
#define STM_ROLLBACK_KEY(k) \
  ComputationLogRollback(STM_SELF,k);

#define STM_REPORT_DEADLOCK() \
  TxDeadlockRpt(STM_SELF);

#define STM_REPORT_TIMEOUTS() \
  TxTimeoutRpt(STM_SELF);

#define STM_ITER_SET(i) \
  ThreadIterSet(STM_SELF,i)

#define STM_ITER_GET() \
  ThreadIterGet(STM_SELF)

#define STM_SHOW_LOG() \
  ComputationLogShow(STM_SELF);

#define STM_USE_DREADLOCKS() \
  TxDreadlockTimeout(STM_SELF, -1);
#define STM_SET_TIMEOUT(i) \
  TxDreadlockTimeout(STM_SELF, i);

#else
#define STM_LOG_INVERSE(inverse, numArgs, args...) \
  InverseLogAdd(STM_SELF, (void (*)(void))inverse, numArgs, ## args);
#define STM_LOG_ABSTRACTLOCK(lock) \
  AbstractLogAdd(STM_SELF, lock);
#endif

#ifdef XBOOST_CHECKPOINTS
#define STM_RELEASE_LOCK(lock) \
  ReleaseLock(lock, STM_SELF);
#endif

#ifdef __cplusplus
}
#endif

#endif /* STM_H */


/* =============================================================================
 *
 * End of stm.h
 *
 * =============================================================================
 */
