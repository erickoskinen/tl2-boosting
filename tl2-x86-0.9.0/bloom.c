#include<limits.h>
#include<assert.h>
#include<string.h>
#include <stdio.h>
#include<stdarg.h>

#include"bloom.h"

#define SETBIT(a, n) (a[n/CHAR_BIT] |= (1<<(n%CHAR_BIT)))
#define GETBIT(a, n) (a[n/CHAR_BIT] & (1<<(n%CHAR_BIT)))

/* ************************************************************* */

unsigned int sax_hash(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int h=0;
  for (i = 0; i < key_len; i++)
    h^=(h<<5)+(h>>2)+(unsigned char)key[i];
  return h;
}

unsigned int sdbm_hash(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int h=0;
  for (i = 0; i < key_len; i++)
    h=(unsigned char)key[i] + (h<<6) + (h<<16) - h;
  return h;
}

unsigned int jenkins(const int val) {
  unsigned char *key = (unsigned char *)&val;
  size_t i, key_len = sizeof(val);
  unsigned int hash = 0;
 
  for (i = 0; i < key_len; i++) {
    hash += key[i];
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);
  return hash;
}

#define _word_of_bit(bit) ((bit) / 32)
#define _bit32_of_bit(bit) ((bit) % 32)
#define _bit32mask_of_bit(bit) ((unsigned int)1 << (_bit32_of_bit(bit)))

void SET_HASH(BLOOMFILTER *filter, unsigned int threadID) {
  memset(filter,'\0',sizeof(BLOOMFILTER));
  unsigned int A = sax_hash(threadID) % (4*32);
  (*filter)[_word_of_bit(A)] |= _bit32mask_of_bit(A); 
  A = sdbm_hash(threadID) % (4*32);
  (*filter)[_word_of_bit(A)] |= _bit32mask_of_bit(A);
  A = jenkins(threadID) % (4*32);
  (*filter)[_word_of_bit(A)] |= _bit32mask_of_bit(A);
}


/* ************************************************************* */

unsigned char * bloom_getID(BLOOM *bloom, const int val) {
  size_t n;
  unsigned char *hashID = calloc(bloom->asize, sizeof(char));
  for(n=0; n<bloom->nfuncs; ++n)
    SETBIT(hashID, bloom->funcs[n](val)%bloom->asize);

  return hashID;
}

/* ************************************************************* */

BLOOM *bloom_create_internal(size_t size, size_t nfuncs, ...) {
  BLOOM *bloom;
  va_list l;
  int n;


  if(!(bloom=malloc(sizeof(BLOOM)))) return NULL;
  //if(!(bloom->a=calloc((size+CHAR_BIT-1)/CHAR_BIT, sizeof(char)))) {
  if(!(bloom->data=calloc(size, sizeof(char)))) {
    free(bloom);
    return NULL;
  }
  if(!(bloom->funcs=(hashfunc_t*)malloc(nfuncs*sizeof(hashfunc_t)))) {
    free(bloom->data);
    free(bloom);
    return NULL;
  }

  va_start(l, nfuncs);
  for(n=0; n<nfuncs; ++n) {
    bloom->funcs[n]=va_arg(l, hashfunc_t);
  }
  va_end(l);

  bloom->nfuncs=nfuncs;
  bloom->asize=size;

#ifdef DEBUG_BAD
  fprintf(stderr,"WARNING! Highly inefficient debugging is turned on! See bloom.c\n");
  bloom->str[0] = '\0';
#endif

  return bloom;
}

BLOOM *bloom_create(size_t size) {
  return bloom_create_internal(size, 3,  sax_hash, sdbm_hash, jenkins);
}

int bloom_destroy(BLOOM *bloom) {
  free(bloom->data);
  free(bloom->funcs);
  free(bloom);
  return 0;
}

int bloom_clear(BLOOM *bloom) {
  memset(bloom->data,0,bloom->asize);
#ifdef DEBUG_BAD
  bloom->str[0] = '\0';
#endif
  return 0;
}

int bloom_add(BLOOM *bloom, const unsigned char *hashID) {
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    bloom->data[n] |= hashID[n];

#ifdef DEBUG_BAD
  char tmp[80];
  sprintf(tmp,",%d",val);
  strcat(bloom->str,tmp);
#endif
  return 0;
}

int bloom_check(BLOOM *bloom, const unsigned char *hashID) {
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    if(! (bloom->data[n] & hashID[n]) )
      return 0;
  return 1;
}

/* ************************************************************* */

int bloom_union(BLOOM *bloom, BLOOM *other) {
  assert(bloom->asize == other->asize);
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    bloom->data[n] |= other->data[n];
#ifdef DEBUG_BAD
  strcat(bloom->str," U ");
  strcat(bloom->str,other->str);
#endif
  return 0;
}

int bloom_dupe(BLOOM *src, BLOOM *dest) {
  assert(src->asize == dest->asize);
  memcpy(dest->data, src->data, src->asize);
#ifdef DEBUG_BAD
  strcpy(src->str,dest->str);
#endif
  return 1;
}

int bloom_is_empty(BLOOM *bloom) {
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    if(bloom->data[n])
      return 0;
  return 1;
}

/* ************************************************************* */

void bloom_setone(BLOOM *bloom, const unsigned char *hashID){
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    bloom->data[n] = hashID[n];
}

void bloom_setunion(BLOOM *bloom, const unsigned char *hashID, BLOOM *other) {
  size_t n;
  for(n=0; n<bloom->asize; ++n)
    bloom->data[n] = other->data[n] | hashID[n];
}

/* ************************************************************* */
