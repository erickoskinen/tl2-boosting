/* =============================================================================
 *
 * tl2.h
 * -- Transactional Locking 2 software transactional memory
 *
 * =============================================================================
 *
 * Copyright (C) Sun Microsystems Inc., 2006.  All Rights Reserved.
 * Authors: Dave Dice, Nir Shavit, Ori Shalev.
 *
 * TL2: Transactional Locking for Disjoint Access Parallelism
 *
 * Transactional Locking II,
 * Dave Dice, Ori Shalev, Nir Shavit
 * DISC 2006, Sept 2006, Stockholm, Sweden.
 *
 * =============================================================================
 *
 * Modified by Chi Cao Minh (caominh@stanford.edu):
 *   Aug 2006 -- x86/Linux port
 *   Oct 2006 -- Hashlog for write-set
 *   Dec 2006 -- setjmp/longjmp
 *
 * =============================================================================
 */

#ifndef TL2_H
#define TL2_H 1

#define DEBUG 0

//#include "../stamp-0.9.4/lib/queueLock.h"

#include "bloom.h"
#include "bloom32.h"

#include <ucontext.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <libgen.h>
#include <assert.h>
#include <pthread.h>
#include <sys/utsname.h>
#include <time.h>
#include <signal.h>         /* for emulating non-faulting load */
#include <limits.h>
#include <stddef.h>
#include <stdarg.h>

#include <setjmp.h>
#define SIGSETJMP(env, savesigs)  sigsetjmp(env, savesigs)
#define SIGLONGJMP(env, val)      siglongjmp(env, val); assert(0)

#ifdef PROFILE
#  define PROF_STM_ABORT_BEGIN()       /* marker */
#  define PROF_STM_ABORT_END()         /* marker */
#  define PROF_STM_COMMIT_BEGIN()      /* marker */
#  define PROF_STM_COMMIT_END()        /* marker */
#  define PROF_STM_NEWTHREAD_BEGIN()   /* marker */
#  define PROF_STM_NEWTHREAD_END()     /* marker */
#  define PROF_STM_READ_BEGIN()        /* marker */
#  define PROF_STM_READ_END()          /* marker */
#  define PROF_STM_START_BEGIN()       /* marker */
#  define PROF_STM_START_END()         /* marker */
#  define PROF_STM_STERILIZE_BEGIN()   /* marker */
#  define PROF_STM_STERILIZE_END()     /* marker */
#  define PROF_STM_WRITE_BEGIN()       /* marker */
#  define PROF_STM_WRITE_END()         /* marker */
#  define PROF_STM_WRITELOCAL_BEGIN()  /* marker */
#  define PROF_STM_WRITELOCAL_END()    /* marker */
#  define PROF_STM_SUCCESS()           /* marker */
#else
#  define PROF_STM_ABORT_BEGIN()       /* nothing */
#  define PROF_STM_ABORT_END()         /* nothing */
#  define PROF_STM_COMMIT_BEGIN()      /* nothing */
#  define PROF_STM_COMMIT_END()        /* nothing */
#  define PROF_STM_NEWTHREAD_BEGIN()   /* nothing */
#  define PROF_STM_NEWTHREAD_END()     /* nothing */
#  define PROF_STM_READ_BEGIN()        /* nothing */
#  define PROF_STM_READ_END()          /* nothing */
#  define PROF_STM_START_BEGIN()       /* nothing */
#  define PROF_STM_START_END()         /* nothing */
#  define PROF_STM_STERILIZE_BEGIN()   /* nothing */
#  define PROF_STM_STERILIZE_END()     /* nothing */
#  define PROF_STM_WRITE_BEGIN()       /* nothing */
#  define PROF_STM_WRITE_END()         /* nothing */
#  define PROF_STM_WRITELOCAL_BEGIN()  /* nothing */
#  define PROF_STM_WRITELOCAL_END()    /* nothing */
#  define PROF_STM_SUCCESS()           /* nothing */
#endif


// =================================================================

#define word_of_bit(bit) ((bit) / 32)
#define bit32_of_bit(bit) ((bit) % 32)
#define bit32mask_of_bit(bit) ((unsigned int)1 << (bit32_of_bit(bit)))

#ifdef DREADLOCKS_BIGBLOOM
#define SET_CONTAINS(digest,hashID) ( \
  (  (! ((*(hashID))[0])) || ( ((*(hashID))[0]) & (*(digest))[0]) ) && \
  (  (! ((*(hashID))[1])) || ( ((*(hashID))[1]) & (*(digest))[1]) ) && \
  (  (! ((*(hashID))[2])) || ( ((*(hashID))[2]) & (*(digest))[2]) ) && \
  (  (! ((*(hashID))[3])) || ( ((*(hashID))[3]) & (*(digest))[3]) ) \
)
#define SET_CLEAR(digest) \
  do { (*(digest))[0] = 0; (*(digest))[1] = 0; (*(digest))[2] = 0; (*(digest))[3] = 0; } while(0);
#define SET_SINGLE(digest,hashID) \
  do { (*(digest))[0] = (*(hashID))[0]; \
      (*(digest))[1] = (*(hashID))[1]; \
      (*(digest))[2] = (*(hashID))[2]; \
      (*(digest))[3] = (*(hashID))[3]; \
  } while(0);
#define SET_UNION(digest,hashID,odigest) \
  do { (*(digest))[0] = (*(odigest))[0] | (*(hashID))[0]; \
      (*(digest))[1] = (*(odigest))[1] | (*(hashID))[1]; \
      (*(digest))[2] = (*(odigest))[2] | (*(hashID))[2]; \
      (*(digest))[3] = (*(odigest))[3] | (*(hashID))[3]; \
  } while(0);




#elif defined(DREADLOCKS_SMALLBLOOM)
#define SET_EMPTY(digest) \
  (*(digest) == 0)
#define SET_CONTAINS(digest,h) \
  *(digest) & h
#define SET_CLEAR(digest) \
  *(digest) = 0
#define SET_SINGLE(digest,h) \
  *(digest) = h
#define SET_UNION(digest,h,otherdigest) \
  *(digest) = h | *(otherdigest)


#elif defined(DREADLOCKS_MIDVEC)
typedef unsigned int BITMAP128[4];
#define SET_CLEAR(digest) \
  do{ (*(digest))[0] = 0; (*(digest))[1] = 0; (*(digest))[2] = 0; (*(digest))[3] = 0; } while(0);
#define SET_CONTAINS(digest,bit) \
  (*(digest))[word_of_bit(bit)] & bit32mask_of_bit(bit)
#define SET_UNION(digest,bit,odigest) \
  do{ (*(digest))[0] = (*(odigest))[0]; \
      (*(digest))[1] = (*(odigest))[1]; \
      (*(digest))[2] = (*(odigest))[2]; \
      (*(digest))[3] = (*(odigest))[3]; \
      (*(digest))[word_of_bit(bit)] |= bit32mask_of_bit(bit); \
  } while(0);
#define SET_SINGLE(digest,bit) \
  (*(digest))[word_of_bit(bit)] |= bit32mask_of_bit(bit)

#elif defined(DREADLOCKS_BIGVEC)
typedef unsigned int BITMAP256[8];
#define SET_CLEAR(digest) \
  do{ (*(digest))[0] = 0; (*(digest))[1] = 0; (*(digest))[2] = 0; (*(digest))[3] = 0; \
      (*(digest))[4] = 0; (*(digest))[5] = 0; (*(digest))[6] = 0; (*(digest))[7] = 0; \
  } while(0);
#define SET_CONTAINS(digest,bit) \
  (*(digest))[word_of_bit(bit)] & bit32mask_of_bit(bit)
#define SET_UNION(digest,bit,odigest) \
  do{ (*(digest))[0] = (*(odigest))[0]; \
      (*(digest))[1] = (*(odigest))[1]; \
      (*(digest))[2] = (*(odigest))[2]; \
      (*(digest))[3] = (*(odigest))[3]; \
      (*(digest))[4] = (*(odigest))[4]; \
      (*(digest))[5] = (*(odigest))[5]; \
      (*(digest))[6] = (*(odigest))[6]; \
      (*(digest))[7] = (*(odigest))[7]; \
      (*(digest))[word_of_bit(bit)] |= bit32mask_of_bit(bit); \
  } while(0);
#define SET_SINGLE(digest,bit) \
  (*(digest))[word_of_bit(bit)] |= bit32mask_of_bit(bit)


#else
#define BitMsk(bit) (1 << (bit))
#define BOOL(x) (!(!(x)))
#define BitNset(bit,arg) ((arg) |= BitMsk(bit))
#define BitNclr(bit,arg) ((arg) &= ~BitMsk(bit))
#define BitNflp(big,arg) ((arg) ^= BitMsk(bit))
#define BitNtst(bit,arg) BOOL((arg) & BitMsk(bit))
#define BitUnion(a1,a2) ((a1) |= (a2))
#define BitAlltst(arg) ((arg) > 0)
#define BitAllClr(arg) ((arg) = 0)

#define SET_EMPTY(digest) \
  (*(digest) == 0)
#define SET_CONTAINS(digest,id) \
  *(digest) & (1 << (id))
#define SET_CLEAR(digest) \
  *(digest) = 0
#define SET_SINGLE(digest,id) \
  *(digest) = (1 << (id))
#define SET_UNION(digest,id,otherdigest) \
  *(digest) = (1 << (id)) | *(otherdigest)

#endif

// =================================================================

#ifdef DREADLOCKS_BIGBLOOM
typedef BLOOMFILTER digest_t;
typedef BLOOMFILTER* digestID_t;
#elif defined(DREADLOCKS_SMALLBLOOM)
typedef unsigned int digest_t;
typedef unsigned int digestID_t;
#elif defined(DREADLOCKS_MIDVEC)
typedef BITMAP128 digest_t;
typedef unsigned int digestID_t;
#elif defined(DREADLOCKS_BIGVEC)
typedef BITMAP256 digest_t;
typedef unsigned int digestID_t;
#else
typedef unsigned int digest_t;
typedef unsigned int digestID_t;
#endif

// =================================================================










#ifdef __cplusplus
extern "C" {
#endif


  enum tl2_config {
    TL2_INIT_WRSET_NUM_ENTRY = 1024,
    TL2_INIT_RDSET_NUM_ENTRY = 2048,
    TL2_INIT_LOCAL_NUM_ENTRY = 256,
  };

#ifdef TL2_OPTIM_HASHLOG
  enum hashlog_config {
    HASHLOG_INIT_NUM_LOG           = 4,
    HASHLOG_INIT_NUM_ENTRY_PER_LOG = 8,
    HASHLOG_RESIZE_RATIO           = 4, /* avg num of entries per bucket threshold */
    HASHLOG_GROWTH_FACTOR          = 2,
  };
  /* Enable HashLog resizing with #define TL2_RESIZE_HASHLOG*/
#define TL2_RESIZE_HASHLOG
#  ifdef __LP32
#    define HASHLOG_SHIFT           (2)
#  else
#    define HASHLOG_SHIFT           (3)
#  endif
#  define HASHLOG_HASH(addr)        ((unsigned long)(addr) >> HASHLOG_SHIFT)
#  define TL2_INIT_WRSET_NUM_ENTRY  (HASHLOG_INIT_NUM_ENTRY_PER_LOG)
#endif

  typedef enum {
    TIDLE       = 0, /* Non-transactional */
    TTXN        = 1, /* Transactional mode */
    TABORTING   = 3, /* aborting - abort pending */
    TABORTED    = 5, /* defunct - moribund txn */
    TCOMMITTING = 7,
  } Modes;

  typedef enum {
    LOCKBIT  = 1,
    NULLVER  = (int) 0xFFFFFFF0, /* CONSIDER 0x1 */
    NADA
  } ManifestContants;

  typedef int           BitMap;
  typedef uintptr_t     vwLock;  /* (Version,LOCKBIT) */
  typedef unsigned char byte;

  /* Read set and write-set log entry */
  typedef struct _AVPair {
    struct _AVPair* Next;
    struct _AVPair* Prev;
    volatile intptr_t* Addr;
    intptr_t Valu;
    volatile vwLock* LockFor; /* points to the vwLock covering Addr */
    vwLock rdv;               /* read-version @ time of 1st read - observed */
    byte Held;
    struct _Thread* Owner;
    int Ordinal;
  } AVPair;

  typedef struct _Log {
    AVPair* List;
    AVPair* put;        /* Insert position - cursor */
    AVPair* tail;       /* Pointer to last entry */
    int ovf;            /* Overflow - request to grow */
#ifndef TL2_OPTIM_HASHLOG
    BitMap BloomFilter; /* Address exclusion fast-path test */
#endif
  } Log;

#ifdef TL2_OPTIM_HASHLOG
  typedef struct _HashLog {
    Log* logs;
    int numLog;
    int numEntry;
    BitMap BloomFilter; /* Address exclusion fast-path test */
  } HashLog;
#endif

  enum boosting_config {
    INVERSE_LOG_NUM_ENTRY = 128 * 1024,
    COMPUTATION_LOG_NUM_ENTRY = 128 * 1024,
    ABSTRACT_LOG_NUM_ENTRY = 128 * 1024,
    CONTEXT_LOG_NUM_ENTRY = 128 * 1024,
    INVERSE_LOG_MAX_ARGS = 4,
    HOLDCOUNT_LOCK_EXPIRE = 128 * 1024,
  };
  /* =============================================================================
   * hold-count locks
   * =============================================================================
   */
  typedef struct _HoldCountData {
    short ct;
    short id;
  } HoldCountData;

  typedef union _HoldCountLock {
    HoldCountData pair;
    int word;
  } HoldCountLock;

#define HCLOCK_EQUAL(a,b) (a.pair.ct == b.pair.ct && a.pair.id == b.pair.id)

  /* =============================================================================
   * abstract locking data structures 
   * =============================================================================
   */
  typedef struct _AbstractLogEntry {
    struct _AbstractLogEntry* Next;
    intptr_t* mutex;
  } AbstractLogEntry;

  typedef struct _AbstractLog {
    struct _AbstractLogEntry* First;       /* first entry */
    struct _AbstractLogEntry* Last;        /* last entry */
    struct _AbstractLogEntry* Free;        /* first free entry */
    int Capacity;
  } AbstractLog;

  /* =============================================================================
   * inverse logging data structures 
   * =============================================================================
   */

  typedef void (*voidFunc)();

  typedef struct _InverseLogEntry {
    struct _InverseLogEntry* Next;
    struct _InverseLogEntry* Prev;
    voidFunc Inverse;
    int numArgs;
    void* arg[INVERSE_LOG_MAX_ARGS];  
    struct _Thread* Owner;
  } InverseLogEntry;

  typedef struct _InverseLog {
    struct _InverseLogEntry* First;       /* first entry */
    struct _InverseLogEntry* Last;        /* last entry */
    struct _InverseLogEntry* Free;        /* first free entry */
    int Capacity;
  } InverseLog;


  static void PlayInverse(int num, void *arg[], voidFunc Inverse) {
    //assert(e);
    //printf("a %d %d\n",num, (int)arg[0]);
    switch (num) {
    case 0: Inverse(); break;
    case 1: Inverse(arg[0]); break;
    case 2: Inverse(arg[0], arg[1]); break;
    case 3: Inverse(arg[0], arg[1], arg[2]); break;
    case 4: Inverse(arg[0], arg[1], arg[2], arg[3]); break;
    default:
      printf("too many args on undo: %d\n", num);
      exit(1);
    }
  }

  /* =============================================================================
   * queueLock data structures
   * =============================================================================
   */

#define QUEUELOCK_PATIENCE 8 * 1024 * 1024
#define QUEUELOCKKEY_TABLE_SIZE (24 * 1024)

#define QUEUELOCK_TABLE_SIZE (24 * 1024)
#define QUEUELOCK_TABLE_MASK (QUEUELOCK_TABLE_SIZE - 1)

#define QUEUELOCK_TIMEOUT(h) (h == NULL)

  typedef struct QLockNode {
    int locked;
    int abandoned;
    int unseen;
    struct _Thread *owner;
    struct QLockNode* pred;
  } QLockNode_t;

  typedef struct QLockKeyElement {
    QLockNode_t* Ltail;
    QLockNode_t* Htail;
    QLockNode_t* Lhead;
    QLockNode_t* Hhead;
    struct _Thread* owner;
  } QLockKeyElement_t;  

  typedef struct QLockKey {
    QLockKeyElement_t table[QUEUELOCKKEY_TABLE_SIZE];
    int patience;
  } QLockKey_t;

  static void print_element(char *msg,QLockKeyElement_t *el) {
    printf("[el. Ltail=%d, Htail=%d   [%s]\n"
	   "[el. Lhead=%d, Hhead=%d\n",
	   el->Ltail,
	   el->Htail,msg,
	   el->Lhead,
	   el->Hhead,
	   el->Ltail);
  }

  typedef struct QLockHandle {
    struct QLockNode* curr;
    struct QLockNode* pred;
    int isHigh;
    QLockKeyElement_t* el;
    QLockKey_t* lock;         // these are useful
    int key;
  } QLockHandle_t;

  static void print_handle(QLockHandle_t *h) {
    printf("curr = %d\n"
	   "pred = %d\n"
	   "high = %d\n"
	   "key  = %d\n",
	   h->curr,h->pred,h->isHigh,h->key);
  }

  /* =============================================================================
   * computation log (keys, context, inverses)
   * =============================================================================
   */

  enum computation_log_cfg {
    LOG_TYPE_INVERSE = 0,
    LOG_TYPE_CONTEXT = 1,
    LOG_TYPE_KEY     = 2,
    LOG_TYPE_QLOCK   = 3,
    LOG_TYPE_DREADLOCK = 4
  };
  
  typedef struct _ComputationLogEntry {
    struct _ComputationLogEntry* Next;
    struct _ComputationLogEntry* Prev;
    int Type;
    intptr_t* Mutex;
    QLockHandle_t* QLHandle;
    ucontext_t *Context;
    int Context_iter;
    voidFunc Inverse;
    int numArgs;
    void* arg[INVERSE_LOG_MAX_ARGS];  
    struct _Thread* Owner;
  } ComputationLogEntry;

  typedef struct _ComputationLog {
    struct _ComputationLogEntry* First;       /* first entry */
    struct _ComputationLogEntry* Last;        /* last entry */
    struct _ComputationLogEntry* Free;        /* first free entry */
    int Capacity;
  } ComputationLog;

  static void print_entry(int id, int num, ComputationLogEntry *e) {
    //if(!DEBUG) return;
    if(e->Type == LOG_TYPE_INVERSE)
      printf("%02d.inverse ", num);
    else if(e->Type == LOG_TYPE_CONTEXT)
      printf("%02d.ctx=%d ", num,e->Context);
    else if(e->Type == LOG_TYPE_KEY)
      printf("%02d.lock=%d ", num, (int)e->Mutex);
    else if(e->Type == LOG_TYPE_QLOCK)
      printf("%02d.qlh=%d,k=%d ", num, (int)e->QLHandle, e->QLHandle->key);
    else if(e->Type == LOG_TYPE_DREADLOCK)
      printf("%02d.dreadlock=%d ", num, (int)e->Mutex);
    else
      assert(0);
  }

  static void print_log(int id, ComputationLog* log) {
    int i = 0;
    printf("%d LOG: ", id);
    ComputationLogEntry* e;
    e = log->Free->Prev;
    while (e != NULL && e->Prev != NULL) {
      print_entry(id,i++,e);
      e = e->Prev;
    }
    printf("\n");
  }

  /* =============================================================================
   * Thread structure
   * =============================================================================
   */

  typedef struct _Thread {
    int UniqID;
    int NumThread;
    volatile int Mode;
    volatile int HoldsLocks; /* passed start of update */
    volatile int Retries;
    volatile vwLock rv;
    vwLock wv;
    vwLock abv;
    int* ROFlag;
    int IsRO;
    int Aborts;             /* Tally of # of aborts */
    unsigned long long rng;
    unsigned long long xorrng [1];
    int ovf;
    void* memCache;
    Log rdSet;
#ifdef TL2_OPTIM_HASHLOG
    HashLog wrSet;
#else
    Log wrSet;
#endif
    Log LocalUndo;
    ComputationLog CLog;
    int iter;
    int QL_KeyList[QUEUELOCK_TABLE_SIZE];
    int isHigh;
    int PreemptKey;
    int Timeouts;
    int TxnNo;
    InverseLog ILog;
    AbstractLog ALog;
    jmp_buf* envPtr;
#ifdef TL2_STATS
    int stats[12];
    int TxST;
    int TxLD;
#endif /* TL2_STATS */
    digest_t *digest;
    digestID_t digestID;

    int numThreads;
    int Deadlocks;
    BLOOM *TL2Filter;
    int DreadlockTimeout;

    int TL2Timeouts;
    int TL2Deadlocks;
  } Thread;


  extern pthread_key_t global_key_self;
  extern int global_txn_count;
  extern Thread* global_tid2ptr[256];

  /*
   * Prototypes
   */

#define __ATTR__    __attribute__((always_inline))
#define __INLINE__  static __inline__

  __INLINE__ void     TxStart       (Thread*, jmp_buf*, int*)               __ATTR__;
  __INLINE__ void     TxInitThread  (Thread*,int id);
  __INLINE__ void     TxHighThread  (Thread* t);
  __INLINE__ void     TxNewThread   (Thread*);
  __INLINE__ int      TxCommit      (Thread*)                               __ATTR__;
  __INLINE__ void     TxAbort       (Thread*)                               __ATTR__;
  __INLINE__ intptr_t TxLoad        (Thread*, volatile intptr_t*)           __ATTR__;
  __INLINE__ void     TxSterilize   (Thread*, void*, size_t)                __ATTR__;
  __INLINE__ void     TxStore       (Thread*, volatile intptr_t*, intptr_t) __ATTR__;
  __INLINE__ void     TxStoreLocal  (Thread*, volatile intptr_t*, intptr_t) __ATTR__;

  int     TxDeadlockRpt (Thread*);
  int     TxTimeoutRpt (Thread*);
  void    TxOnce      ();
  void    TxShutdown  ();
  int     TxStats       (Thread*);

  QLockKey_t* QLockKey_alloc(int patience);

  static void ComputationLogQLock (Thread* Self, QLockHandle_t* handle);

  void QLockKey_free(QLockKey_t* self);

  QLockHandle_t* QLockKey_Lock(Thread *self,QLockKey_t* ql,int key,int high,int fullAbort);

  void QLockKey_Unlock(Thread *self, QLockHandle_t* handle);


  /* =============================================================================
   * PLATFORM-SPECIFIC BINDINGS
   * =============================================================================
   */


  /* =============================================================================
   * Compare-and-swap
   *
   * CCM: Notes for implementing CAS on x86:
   *
   * /usr/include/asm-x86_64/system.h
   * http://www-128.ibm.com/developerworks/linux/library/l-solar/
   * http://gcc.gnu.org/onlinedocs/gcc-4.1.0/gcc/Atomic-Builtins.html#Atomic-Builtins
   *
   * In C, CAS would be:
   *
   * static __inline__ intptr_t cas(intptr_t newv, intptr_t old, intptr_t* ptr) {
   *     intptr_t prev;
   *     pthread_mutex_lock(&lock);
   *     prev = *ptr;
   *     if (prev == old) {
   *         *ptr = newv;
   *     }
   *     pthread_mutex_unlock(&lock);
   *     return prev;
   * =============================================================================
   */
#define CAS(m,c,s)  cas((intptr_t)(s),(intptr_t)(c),(intptr_t*)(m))
  __INLINE__ intptr_t
  cas (intptr_t newVal, intptr_t oldVal, intptr_t* ptr)
  {
    intptr_t prevVal;

    __asm__ __volatile__(
			 "lock \n"
#ifdef __LP32
			 "cmpxchgl %k1,%2 \n"
#else
			 "cmpxchgq %1,%2 \n"
#endif
			 : "=a"(prevVal)
			 : "q"(newVal), "m"(*((volatile long *)(ptr))), "0"(oldVal)
			 : "memory"
			 );

    return prevVal;
  }


  /* =============================================================================
   * Memory Barriers
   * -- http://mail.nl.linux.org/kernelnewbies/2002-11/msg00127.html
   * =============================================================================
   */
#define MEMBARLDLD()   /* nothing */
#define MEMBARSTST()   /* nothing */
#define MEMBARSTLD()   __asm__ __volatile__ ("" : : :"memory")


  /* =============================================================================
   * Prefetching
   * -- We use PREFETCHW in LD...CAS and LD...ST circumstances to force the $line
   *    directly into M-state, avoiding RTS->RTO upgrade txns.
   * =============================================================================
   */
  __INLINE__ void
  prefetchw (volatile void* x)
  {
    /*
     *  prefetchw is available on CPUs with 3DNOW as well as AMD x86-64.
     *  It is not available on Intel EMT64 systems.
     */
#if defined(HAVE_PREFETCHW) || defined(__amd64__)
    asm volatile ("prefetchw %0" :: "m" (x));
#endif
  }


  /* =============================================================================
   * Non-faulting load
   * -- Emulated with trap handlers on SIGSEGV, SIGBUS, SIGILL
   * =============================================================================
   */
#define LDNF(a)  *(a) /* CCM: handler will abort and retry */


  /* =============================================================================
   * MP-polite spinning
   * -- Ideally we would like to drop the priority of our CMT strand.
   * =============================================================================
   */
#define PAUSE()  /* nothing */


  /* =============================================================================
   * GENERIC INFRASTRUCTURE
   * =============================================================================
   */

#ifndef TL2_CACHE_LINE_SIZE
#  define TL2_CACHE_LINE_SIZE (64)
#endif
  enum {
    NEVER  = 0,
    ALWAYS = 1,
  };
  extern int                OverflowTally;
  extern volatile intptr_t  ThreadUniqID; /* Thread sequence number */
  extern volatile intptr_t  NumThread;
#ifdef TL2_STATS
  extern volatile int       global_stats[4096];
#endif

#define TL2_TIMER_T  unsigned long long
  /* CCM: low overhead timer; also works with simulator */
#define TL2_TIMER_READ() ({				\
      unsigned long long clock;				\
      unsigned long hi;					\
      unsigned long lo;					\
      __asm__ __volatile__ (				\
			    "rdtsc\n"			\
			    "movl %0, %%edx\n"		\
			    "movl %1, %%eax\n"		\
			    :  "=g" (hi), "=g" (lo)	\
			    );				\
      clock = (((unsigned long long)hi) << 32) |	\
	(((unsigned long long)(lo)) & 0xFFFF);		\
      clock;						\
    })
  /*
    Version with clock_gettime:

    #define TL2_TIMER_READ() ({ \
    struct timespec time; \
    clock_gettime(CLOCK_MONOTONIC, &time); \
    (long)time.tv_sec * 1000000000L + (long)time.tv_nsec; \
    })
  */
  /*
    Version with gettimeofday():

    #define TL2_TIMER_READ() ({ \
    struct timeval time; \
    gettimeofday(&time, NULL); \
    (long)time.tv_sec * 1000000L + (long)time.tv_usec; \
    })
  */


#define DIM(A)      (sizeof(A)/sizeof((A)[0]))
#define UNS(a)      ((uintptr_t)(a))
#define CTASSERT(x) {int tag[1-(2*!(x))]; printf("Tag @%lX\n", (unsigned long)tag);}
#define ASSERT(x)   /* nothing */
  /*#define ASSERT(x)   assert(x) */


  /* =============================================================================
   * Adjust
   * -- Atomic fetch-and-add
   * =============================================================================
   */
  __INLINE__ intptr_t
  Adjust (volatile intptr_t* addr, int dx)
  {
    intptr_t v;
    assert(0);
    for (v = *addr; CAS(addr, v, v+dx) != v; v = *addr) {}
    return v;
  }

#ifdef XBOOST_CHECKPOINTS
  /* =============================================================================
   * Reentrant lock with a hold count
   * -- TATAS acquire
   * =============================================================================
   */
  __INLINE__ int
  Lock (volatile intptr_t* addr, Thread* Self)
  {
    short mytid = (short)(100 + Self->UniqID);
    int expire = HOLDCOUNT_LOCK_EXPIRE;
    HoldCountLock hcCopy;
    HoldCountLock hcNew;
    
    while(expire--) {
      hcNew.pair.id = mytid;
      hcNew.pair.ct = 1;
      hcCopy.word = ((HoldCountLock *)addr)->word;   // read the lock

      //printf("%d READLOCK: lock=%d, id = %d, ct = %d [%d]\n",
      //Self->UniqID, (int)addr, (int)hcCopy.pair.id, (int)hcCopy.pair.ct, hcCopy.word);
      assert(hcCopy.pair.id > 99 || hcCopy.pair.id == 0);
      assert(hcCopy.pair.ct >= 0);
      if(hcCopy.pair.id == mytid) {                 // already holding. increment.
	hcNew.pair.ct = (short)(hcCopy.pair.ct + (short)1);
	if(CAS(addr,hcCopy.word,hcNew.word) == hcCopy.word) {
	  if(DEBUG) printf("%d re-acquire. lock=%d, id = %d, ct = %d [%d]\n",
		 Self->UniqID,(int)addr,hcNew.pair.id,hcNew.pair.ct, hcNew.word);
	  return (int)hcNew.pair.ct;
	}
      } else if (hcCopy.pair.id == (short)0) {      // no one's holding. take it!
	//printf("HCLock acquire %d / %d\n",hcCopy.pair.id,mytid);
	if(CAS(addr,hcCopy.word,hcNew.word) == hcCopy.word) {
	  if(DEBUG) printf("%d acquire. lock=%d, id = %d, ct = %d [%d]\n",
		 Self->UniqID,(int)addr,hcNew.pair.id,hcNew.pair.ct, hcNew.word);
	  return (int)hcNew.pair.ct;
	}
      } else { }                                    // someone else has it. wait!
    }
    if(DEBUG) printf("%d TIMEOUT! lock=%d, id = %d, ct = %d [%d]\n",
	   Self->UniqID,
	   (int)addr,
	   (int)hcCopy.pair.id,
	   (int)hcCopy.pair.ct,
	   hcCopy.word);
    return -1;                          // expired!
  }

  /* =============================================================================
   * Reentrant lock with a hold count
   * -- release
   * =============================================================================
   */
  __INLINE__ void
  Unlock  (volatile intptr_t* addr, Thread* Self)
  {
    short myid = (short)(100 + Self->UniqID);
    HoldCountLock *hcl = (HoldCountLock *)addr;
    HoldCountLock hcCopy = *hcl;       
    if(DEBUG) printf("%d release. lock=%d, id = %d, ct = %d\n",
	   Self->UniqID,(int)addr,hcCopy.pair.id, hcCopy.pair.ct);            // read the lock
    assert(hcCopy.pair.id == myid);
    if(hcCopy.pair.ct == 1) {
      hcCopy.pair.ct = 0;                    // release the lock entirely
      hcCopy.pair.id = 0;
      //printf("HCUnlock release\n");
    } else if(hcCopy.pair.ct > 1) {
      //printf("HCUnlock decr\n");
      hcCopy.pair.ct--;
    } else {
      assert(0);
    }
    hcl->word = hcCopy.word;

    assert(hcCopy.pair.ct >= 0);
    return;
  }

  /* =============================================================================
   * Reentrant lock with a hold count
   * -- complete release
   * =============================================================================
   */
  __INLINE__ void
  ReleaseLock  (volatile intptr_t* addr, Thread* Self)
  {
    if(DEBUG) printf("%d release. lock=%d, YIKES!\n",Self->UniqID,(int)addr);
    HoldCountLock hcNew;                    // release the lock entirely
    hcNew.pair.ct = 0;
    hcNew.pair.id = 0;
    *addr = *((int *)&hcNew);
  }

#else
  /* =============================================================================
   * Reentrant lock
   * -- TATAS acquire
   * =============================================================================
   */
  __INLINE__ int
  Lock (volatile intptr_t* addr, Thread* Self)
  {
    /*printf("Lock(%p, %p)\n", addr, Self);*/
    if (*addr == (int)Self) {
      return 0;
    }
    while (ALWAYS) {
      while ((void*)*addr != NULL) {}
      if (CAS(addr, NULL, Self) == (int)NULL) return 1;
    }
  }

  /* =============================================================================
   * Reentrant lock
   * -- release
   * =============================================================================
   */
  __INLINE__ void
  Unlock  (volatile intptr_t* addr, Thread* Self)
  {
    *addr = (int)NULL;
  }
#endif /* XBOOST_CHECKPOINTS */

  /* =============================================================================
   * AtomicUpdate
   * =============================================================================
   */
  __INLINE__ void
  AtomicUpdate (volatile intptr_t* addr, int value)
  {
    intptr_t v;
    assert(0);
    for (v = *addr; CAS(addr, v, value) != v; v = *addr) {}
  }


  /* =============================================================================
   * MarsagliaXORV
   * -- Simplistlic low-quality Marsaglia SHIFT-XOR RNG.
   * -- Bijective except for the trailing mask operation.
   * =============================================================================
   */
  __INLINE__ unsigned long long
  MarsagliaXORV (unsigned long long x)
  {
    if (x == 0) {
      x = 1;
    }
    x ^= x << 6;
    x ^= x >> 21;
    x ^= x << 7;
    return x;
  }


  /* =============================================================================
   * MarsagliaXOR
   * -- Simplistlic low-quality Marsaglia SHIFT-XOR RNG.
   * -- Bijective except for the trailing mask operation.
   * =============================================================================
   */
  __INLINE__ unsigned long long
  MarsagliaXOR (unsigned long long* seed)
  {
    unsigned long long x = MarsagliaXORV(*seed);
    *seed = x;
    return x;
  }


  /* =============================================================================
   * TSRandom
   * =============================================================================
   */
  __INLINE__ unsigned long long
  TSRandom (Thread* Self)
  {
    return MarsagliaXOR(&Self->rng);
  }


  /* =============================================================================
   * intptr2float
   * -- For floating-point TxStore/Load
   * =============================================================================
   */
  __INLINE__ float
  intptr2float (intptr_t val)
  {
    union {
      intptr_t i;
      float    f;
    } convert;
    convert.i = val;
    return convert.f;
  }


  /* =============================================================================
   * float2intptr
   * -- For floating-point TxStore/Load
   * =============================================================================
   */
  __INLINE__ intptr_t
  float2intptr (float val)
  {
    union {
      intptr_t i;
      float    f;
    } convert;
    convert.f = val;
    return convert.i;
  }


  /* =============================================================================
   * intptr2floatptr
   * -- For floating-point TxStore/Load
   * =============================================================================
   */
  __INLINE__ float*
  intptr2floatptr (intptr_t val)
  {
    union {
      intptr_t i;
      float*   f;
    } convert;
    convert.i = val;
    return convert.f;
  }


  /* =============================================================================
   * floatptr2intptr
   * -- For floating-point TxStore/Load
   * =============================================================================
   */
  __INLINE__ intptr_t
  floatptr2intptr (float* val)
  {
    union {
      intptr_t i;
      float*   f;
    } convert;
    convert.f = val;
    return convert.i;
  }


  /* =============================================================================
   * intptr2voidptr
   * =============================================================================
   */
  __INLINE__ void*
  intptr2voidptr (intptr_t val)
  {
    union {
      intptr_t i;
      void*    v;
    } convert;
    convert.i = val;
    return convert.v;
  }


  /* =============================================================================
   * voidptr2intptr
   * =============================================================================
   */
  __INLINE__ intptr_t
  voidptr2intptr (void* val)
  {
    union {
      intptr_t i;
      void*    v;
    } convert;
    convert.v = val;
    return convert.i;
  }


  /* =============================================================================
   * GLOBAL VERSION-CLOCK MANAGEMENT
   * =============================================================================
   */

  extern volatile vwLock GClock[64];
#define _GCLOCK  GClock[32]
#define _TABSZ (1<<20)
  extern volatile vwLock LockTab[]; /* PS : PS1M */


  /* =============================================================================
   * GVInit
   * =============================================================================
   */
  __INLINE__ void
  GVInit ()
  {
    _GCLOCK = 0;
  }


  /* =============================================================================
   * GVRead
   * =============================================================================
   */
  __INLINE__ vwLock
  GVRead (Thread* Self)
  {
#if 1
    return _GCLOCK;
#else
    /* Optional optimization: Avoid self-induced aborts (Use with GV5 or GV6) */
    vwLock gc = _GCLOCK;
    vwLock wv = Self->wv;
    if (wv > gc) {
      CAS(&_GCLOCK, gc, wv);
      return _GCLOCK;
    }
    return gc;
#endif
  }

  /*
   * GVGenerateWV():
   *
   * Conceptually, we'd like to fetch-and-add _GCLOCK.  In practice, however,
   * that naive approach, while safe and correct, results in CAS contention
   * and SMP cache coherency-related performance penalties.  As such, we
   * use either various schemes (GV4,GV5 or GV6) to reduce traffic on _GCLOCK.
   *
   * Global Version-Clock invariant:
   * I1: The generated WV must be > any previously observed (read) R
   */
#if !defined(_GVCONFIGURATION)
#  define _GVCONFIGURATION 4 /* previous default was 6 */
#endif

#if _GVCONFIGURATION == 4
#  define _GVFLAVOR "GV4"
#  define GVGenerateWV GVGenerateWV_GV4
#endif

#if _GVCONFIGURATION == 5
#  define _GVFLAVOR "GV5"
#  define GVGenerateWV GVGenerateWV_GV5
#endif

#if _GVCONFIGURATION == 6
#  define _GVFLAVOR "GV6"
#  define GVGenerateWV GVGenerateWV_GV6
#endif


  /* =============================================================================
   * GVGenerateWV_GV4
   * -- The GV4 form of GVGenerateWV() does not have a CAS retry loop.
   *    If the CAS fails then we have 2 writers that are racing, trying to bump
   *    the global clock.  One increment succeeded and one failed.  Because the
   *    2 writers hold locks at the time we bump, we know that their write-sets
   *    don't intersect. If the write-set of one thread intersects the read-set
   *    of the other then we know that one will subsequently fail validation
   *    (either because the lock associated with the read-set entry is held by
   *    the other thread, or because the other thread already made the update
   *    and dropped the lock, installing the new version #).  In this particular
   *    case it's safe if two threads call GVGenerateWV() concurrently and they
   *    both generate the same (duplicate) WV.  That is, if we have writers that
   *    concurrently try to increment the clock-version and then we allow them
   *    both to use the same wv.  The failing thread can "borrow" the wv of the
   *    successful thread
   * =============================================================================
   */
  __INLINE__ vwLock
  GVGenerateWV_GV4 (Thread* Self, vwLock maxv)
  {
    vwLock gv = _GCLOCK;
    vwLock wv = gv + 2;
    vwLock k = CAS(&_GCLOCK, gv, wv);
    if (k != gv) {
      wv = k;
    }
    ASSERT((wv & LOCKBIT) == 0);
    ASSERT(wv != 0); /* overflow */
    ASSERT(wv > Self->wv);
    ASSERT(wv > Self->rv);
    ASSERT(wv > maxv);
    Self->wv = wv;
    return wv;
  }


  /* =============================================================================
   * GVGenerateWV_GV5
   * -- Simply compute WV = GCLOCK + 2.
   * -- This increases the false+ abort-rate but reduces cache-coherence traffic.
   *    We only increment _GCLOCK at abort-time and perhaps TxStart()-time.
   *    The rate at which _GCLOCK advances controls performance and abort-rate.
   *    That is, the rate at which _GCLOCK advances is really a performance
   *    concern--related to false+ abort rates--rather than a correctness issue.
   * -- CONSIDER: use MAX(_GCLOCK, Self->rv, Self->wv, maxv, VERSION(Self->abv))+2
   * =============================================================================
   */
  __INLINE__ vwLock
  GVGenerateWV_GV5 (Thread* Self, vwLock maxv)
  {
    vwLock wv = _GCLOCK + 2;
    if (maxv > wv) {
      wv = maxv + 2;
    }
    ASSERT(wv != 0); /* overflow */
    ASSERT(wv > Self->rv);
    ASSERT(wv >= Self->wv);
    Self->wv = wv;
    return wv;
  }


  /* =============================================================================
   * GVGenerateWV_GV6
   * -- Composite of GV4 and GV5
   * -- Trade-off -- abort-rate vs SMP cache-coherence costs.
   * -- TODO: make the frequence mask adaptive at runtime.
   *    let the abort-rate or abort:success ratio drive the mask.
   * =============================================================================
   */
  __INLINE__ vwLock
  GVGenerateWV_GV6 (Thread* Self, vwLock maxv)
  {
    int rnd = (int)MarsagliaXOR(Self->xorrng);
    if ((rnd & 0x1F) == 0) {
      return GVGenerateWV_GV4(Self, maxv);
    } else {
      return GVGenerateWV_GV5(Self, maxv);
    }
  }


  /* =============================================================================
   * GVAbort
   *
   * GV5 and GV6 admit single-threaded false+ aborts.
   *
   * Consider the following scenario:
   * GCLOCK is initially 10.  TxStart() fetches GCLOCK, observing 10, and
   * sets RV accordingly.  The thread calls TXST().  At commit-time the thread
   * computes WV = 12 in GVComputeWV().  T1 stores WV (12) in various versioned
   * lock words covered by the write-set.  The transaction commits successfully.
   * The thread then runs a 2nd txn. TxStart() fetches _GCLOCK == 12 and sets RV
   * accordingly.  The thread then calls TXLD() to fetch a variable written in the
   * 1st txn and observes Version# == 12, which is > RV.  The thread aborts.
   * This is false+ abort as there is no actual interference.
   * We can recover by incrementing _GCLOCK at abort-time if we find
   * that RV == GCLOCK and Self->Abv > GCLOCK.
   * Alternately we can attempt to avoid the false+ abort by advancing
   * _GCLOCK at GVRead()-time if we find that the thread's previous WV is >
   * than the current _GCLOCK value
   * =============================================================================
   */
  __INLINE__ int
  GVAbort (Thread* Self)
  {
#if _GVCONFIGURATION != 4
    vwLock abv = Self->abv;
    if (abv & LOCKBIT) {
      return 0; /* normal interference */
    }
    vwLock gv = _GCLOCK;
    if (Self->rv == gv && abv > gv) {
      CAS(&_GCLOCK, gv, abv); /* advance to either (gv+2) or abv */
      /* If this was a GV5/GV6-specific false+ abort then do not delay */
      return 1; /* false+ abort */
    }
#endif
    return 0; /* normal interference */
  }


  /* =============================================================================
   * MakeList
   * -- Allocate the primary list as a large chunk so we can guarantee
   *    ascending & adjacent addresses through the list.
   *    This improves D$ and DTLB behaviour.
   * =============================================================================
   */
  __INLINE__ AVPair*
  MakeList (AVPair* list, int sz, Thread* Self)
  {
    AVPair* ap;
    if (list == NULL) {
      ap = (AVPair*) malloc((sizeof(*ap) * sz) + TL2_CACHE_LINE_SIZE);
    } else {
      ap = list;
    }
    assert(ap);
    memset(ap, 0, sizeof(*ap) * sz);
    AVPair* List = ap;
    AVPair* Tail = NULL;
    int i;
    for (i = 0; i < sz; i++) {
      AVPair* e = ap++;
      e->Next    = ap;
      e->Prev    = Tail;
      e->Owner   = Self;
      e->Ordinal = i;
      Tail = e;
    }
    Tail->Next = NULL;

    return List;
  }


#ifdef TL2_OPTIM_HASHLOG
  /* =============================================================================
   * MakeHashLog
   * =============================================================================
   */
  __INLINE__ void
  MakeHashLog (HashLog* hlPtr,
	       int numLog, int numEntryPerLog, Thread* Self)
  {
    hlPtr->numEntry = 0;
    hlPtr->numLog = numLog;
    hlPtr->logs = (Log*)calloc(numLog, sizeof(Log));
    int i;
    for (i = 0; i < numLog; i++) {
      hlPtr->logs[i].List = MakeList(NULL, numEntryPerLog, Self);
      hlPtr->logs[i].put = hlPtr->logs[i].List;
    }
  }
#endif /* TL2_OPTIM_HASHLOG */


  /* =============================================================================
   * ExtendList
   * -- Postpend at the tail.  We want the front of the list, which sees
   *    the most traffic, to remains contiguous.
   * =============================================================================
   */
  __INLINE__ AVPair*
  ExtendList (AVPair* tail)
  {
    AVPair* e = (AVPair*)malloc(sizeof(*e));
    memset(e, 0, sizeof(*e));
    tail->Next = e;
    e->Prev    = tail;
    e->Next    = NULL;
    e->Owner   = tail->Owner;
    e->Ordinal = tail->Ordinal + 1;
    return e;
  }


  /* =============================================================================
   * WriteBack
   * -- Transfer the data in the log its ultimate location and
   * =============================================================================
   */
  __INLINE__ void
  WriteBack (Log* k)
  {
    AVPair* e;
    AVPair* End = k->put;
    for (e = k->List; e != End; e = e->Next) {
      *(e->Addr) = e->Valu;
    }
  }


  /* =============================================================================
   * FindFirst
   * -- Search for first log entry that contains lock
   * =============================================================================
   */
  __INLINE__ AVPair*
  FindFirst (Log* k, volatile vwLock* Lock)
  {
    AVPair* e;
    AVPair* const End = k->put;
    for (e = k->List; e != End; e = e->Next) {
      if (e->LockFor == Lock) {
	return e;
      }
    }
    return NULL;
  }

  /* =============================================================================
   * RecordStore
   * =============================================================================
   */
  __INLINE__ void
  RecordStore (Log* k, volatile intptr_t* Addr, intptr_t Valu, volatile vwLock* Lock)
  {
    /*
     * As an optimization we could squash multiple stores to the same location.
     * Maintain FIFO order to avoid WAW hazards.
     * TODO-FIXME - CONSIDER
     * Keep Self->LockSet as a sorted linked list of unique LockFor addresses.
     * We'd scan the LockSet for Lock.  If not found we'd insert a new
     * LockRecord at the appropriate location in the list.
     * Call InsertIfAbsent (Self, LockFor
     */
    AVPair* e = k->put;
    if (e == NULL) {
      e = ExtendList(k->tail);
    }
    ASSERT(Addr != NULL);
    k->tail    = e;
    k->put     = e->Next;
    e->Addr    = Addr;
    e->Valu    = Valu;
    e->LockFor = Lock;
    e->Held    = 0;
    e->rdv     = LOCKBIT; /* use either 0 or LOCKBIT */
  }


  /* =============================================================================
   * SaveForRollBack
   * =============================================================================
   */
  __INLINE__ void
  SaveForRollBack (Log* k, volatile intptr_t* Addr, intptr_t Valu)
  {
    AVPair* e = k->put;
    if (e == NULL) {
      e = ExtendList(k->tail);
    }
    k->tail    = e;
    k->put     = e->Next;
    e->Addr    = Addr;
    e->Valu    = Valu;
    e->LockFor = NULL;
  }


  /* =============================================================================
   * TrackLoad
   * =============================================================================
   */
  __INLINE__ void
  TrackLoad (Thread* Self, volatile vwLock* LockFor)
  {
    Log* k = &Self->rdSet;

    /*
     * Consider collapsing back-to-back track loads ...
     * if the previous LockFor and rdv match the incoming arguments then
     * simply return
     */

    AVPair* e = k->put;
    if (e != NULL) {
      k->tail    = e;
      k->put     = e->Next;
      e->LockFor = LockFor;
      /* Note that Valu and Addr fields are undefined for tracked loads */
      return;
    }

    /*
     * Read log overflow suggests a rogue or incoherent transaction.
     * Consider calling SpeculativeReadSetCoherent() and, if needed, TxAbort().
     * This lets us distinguish between a doomed txn that's gone rogue
     * and a large transaction that legitimately overflows the buffer.
     * In the latter case we might  the buffer or chain an overflow
     * buffer onto "k".
     * Options: print, abort, panic, extend, ignore & discard
     * Beware of inlining effects - TrackLoad() is performance-critical.
     * Decreasing the sample period tunable in TxValid() will reduce the
     * rate of overflows caused by zombie transactions.
     */
    Self->Mode = TABORTED;
    Self->ovf++;
  }

  /* =============================================================================
   * Abstract Log Methods
   * Log of two-phase abstract locks, to be released on commot or abort.
   * =============================================================================
   */

  /*
   * Allocates and initialize space for log or extension
   */
  __INLINE__ AbstractLogEntry*
  AbstractLogAlloc (int capacity)
  {
    AbstractLogEntry* entry;
    entry = (AbstractLogEntry*) malloc((sizeof(*entry) * capacity) + TL2_CACHE_LINE_SIZE);
    assert(entry);
    memset(entry, 0, sizeof(*entry) * capacity);
    int i;
    for (i = 0; i < capacity-1; i++) {
      entry[i].Next = &entry[i + 1];
    }
    return entry;
  }

  /*
   * Initializes new abstract log
   */
  __INLINE__ void
  AbstractLogNew (AbstractLog* log, int capacity)
  {
    AbstractLogEntry* entry;
    entry = AbstractLogAlloc(capacity);
    log->First = entry;
    log->Free = entry;
    log->Last = &entry[capacity - 1];
    log->Capacity = capacity;
  }
  /*
   * Extends log.
   * Links existing last entry's Next field to first newly-allocated entry.
   */
  __INLINE__ void
  AbstractLogExtend (AbstractLog* log)
  {
    AbstractLogEntry* oldLast;
    AbstractLogEntry* entry;
    int capacity;
    capacity = log->Capacity;
    oldLast = log->Last;
    entry = AbstractLogAlloc(capacity);
    if (entry == NULL) {
      printf("unable to extend abstract log\n");
      exit(1);
    }
    log->Last->Next = entry;
    log->Last = &entry[capacity - 1];
    log->Capacity = 2 * capacity;
    log->Free = entry;
  }
  /*
   * On commit or abort, release 2-phase locks.
   */
  __INLINE__ void
  AbstractLogRelease (Thread* Self, AbstractLog* log)
  {
    AbstractLogEntry* e;
    for (e = log->First; e != log->Free; e = e->Next) {
      //printf("%d: releaselock %d\n", Self->UniqID, (int)e->mutex);
      Unlock(e->mutex, Self);
    }
    log->Free = log->First;
  }

  __INLINE__ void
  AbstractLogAdd (Thread* Self, intptr_t* mutex)
  {
    AbstractLog* log;
    AbstractLogEntry* e;
    log = &Self->ALog;
    if (!log->Free) {
      AbstractLogExtend(log);
    }
    e = log->Free;
    e->mutex = mutex;
    log->Free = e->Next;
  }

  /* =============================================================================
   * Inverse Log Methods
   * =============================================================================
   */

  /*
   * Allocates and initialize space for log or extension
   */
  __INLINE__ InverseLogEntry*
  InverseLogAlloc (int capacity)
  {
    InverseLogEntry* entry;
    entry = (InverseLogEntry*) malloc((sizeof(*entry) * capacity) + TL2_CACHE_LINE_SIZE);
    assert(entry);
    memset(entry, 0, sizeof(*entry) * capacity);
    int i;
    entry[0].Next = &entry[1];
    for (i = 1; i < capacity - 1; i++) {
      entry[i].Next = &entry[i+1];
      entry[i].Prev = &entry[i-1];
    }
    entry[capacity-1].Prev = &entry[capacity-2];
    return entry;
  }

  /*
   * Initializes new inverse log
   */
  __INLINE__ void
  InverseLogNew (InverseLog* log, int capacity)
  {
    InverseLogEntry* entry;
    entry = InverseLogAlloc(capacity);
    log->First = entry;
    log->Free = entry;
    log->Last = &entry[capacity - 1];
    log->Capacity = capacity;
  }

  /*
   * Extends log.
   * Links existing last entry's Next field to first newly-allocated entry,
   * and vice-versa.
   */
  __INLINE__ void
  InverseLogExtend (InverseLog* log)
  {
    InverseLogEntry* oldLast;
    InverseLogEntry* entry;
    int capacity;
    capacity = log->Capacity;
    oldLast = log->Last;
    entry = InverseLogAlloc(capacity);
    log->Last->Next = entry;
    entry->Prev = log->Last;
    log->Last = &entry[capacity - 1];
    log->Capacity = 2 * capacity;
  }

  __INLINE__ void
  InverseLogOnAbort (InverseLog* log)
  {
    InverseLogEntry* e;
    e = log->Free->Prev;
    while (e != NULL && e->Prev != NULL) {
      PlayInverse(e->numArgs,e->arg,e->Inverse);
      e = e->Prev;
    }
  }

  static void InverseLogAdd (Thread* Self, voidFunc inverse, int numArgs, ...)
  {
    int i;
    va_list argp;
    InverseLog* log;
    log = &Self->ILog;
    va_start(argp, numArgs);
    InverseLogEntry* e;
    if (!log->Free) {
      InverseLogExtend(log);
    }
    e = log->Free;
    e->Inverse = inverse;
    for (i = 0; i < numArgs; i++) {
      e->arg[i] = va_arg(argp, void*);
    }
    e->numArgs = numArgs;
    log->Free = log->Free->Next;
    va_end(argp);
  }

  __INLINE__ void
  InverseLogOnCommit (InverseLog* log)
  {
    log->Free = log->First;
  }

/* =============================================================================
 * Computation Log
 * =============================================================================
 */

  /*
   * Allocates and initialize space for log or extension
   */
  __INLINE__ ComputationLogEntry*
  ComputationLogAlloc (int capacity)
  {
    ComputationLogEntry* entry;
    entry = (ComputationLogEntry*) malloc((sizeof(*entry) * capacity) + TL2_CACHE_LINE_SIZE);
    assert(entry);
    memset(entry, 0, sizeof(*entry) * capacity);
    int i;
    entry[0].Next = &entry[1];
    for (i = 1; i < capacity - 1; i++) {
      entry[i].Next = &entry[i+1];
      entry[i].Prev = &entry[i-1];
    }
    entry[capacity-1].Prev = &entry[capacity-2];
    return entry;
  }

  /*
   * Initializes new inverse log
   */
  __INLINE__ void
  ComputationLogNew (ComputationLog* log, int capacity)
  {
    ComputationLogEntry* entry;
    entry = ComputationLogAlloc(capacity);
    log->First = entry;
    log->Free = entry;
    log->Last = &entry[capacity - 1];
    log->Capacity = capacity;
  }

  /*
   * Extends log.
   * Links existing last entry's Next field to first newly-allocated entry,
   * and vice-versa.
   */
  __INLINE__ void
  ComputationLogExtend (ComputationLog* log)
  {
    ComputationLogEntry* oldLast;
    ComputationLogEntry* entry;
    int capacity;
    capacity = log->Capacity;
    oldLast = log->Last;
    entry = ComputationLogAlloc(capacity);
    log->Last->Next = entry;
    entry->Prev = log->Last;
    log->Last = &entry[capacity - 1];
    log->Capacity = 2 * capacity;
  }

  /*
   * Adds an INVERSE to the log
   */
  static void ComputationLogInverse (Thread* Self, voidFunc inverse, int numArgs, ...)
  {
    int i;
    va_list argp;
    ComputationLog* log = &Self->CLog;
    va_start(argp, numArgs);
    ComputationLogEntry* e;
    if (!log->Free) {
      ComputationLogExtend(log);
    }
    e = log->Free;
    e->Type = LOG_TYPE_INVERSE;
    e->Inverse = inverse;
    for (i = 0; i < numArgs; i++) {
      e->arg[i] = va_arg(argp, void*);
    }
    e->numArgs = numArgs;
    log->Free = log->Free->Next;
    va_end(argp);
  }

  /*
   * Adds a MUTEX to the log
   */
  static void ComputationLogLock (Thread* Self, intptr_t* mutex)
  {
    if(DEBUG) printf("%d ComputationLogLock [%d]\n",Self->UniqID, (int)mutex);
    ComputationLog* log;
    log = &Self->CLog;
    ComputationLogEntry* e;
    if (!log->Free) {
      ComputationLogExtend(log);
    }
    e = log->Free;
    e->Type = LOG_TYPE_KEY;
    e->Mutex = mutex;
    log->Free = log->Free->Next;
  }

  /*
   * Adds a QLock to the log
   */
  static void ComputationLogQLock (Thread* Self, QLockHandle_t* handle)
  {
    if(DEBUG) printf("%d ComputationLogQLock [%d] key=%d ENTER\n",Self->UniqID, (int)handle,handle->key);
    ComputationLog* log;
    log = &Self->CLog;
    ComputationLogEntry* e;
    if (!log->Free) {
      ComputationLogExtend(log);
    }
    e = log->Free;
    e->Type = LOG_TYPE_QLOCK;
    e->QLHandle = handle;
    log->Free = log->Free->Next;
  }

  /*
   * Adds a Dreadlock to the log
   */
  static void ComputationLogDreadLock (Thread* Self, intptr_t* lock)
  {
    if(DEBUG) printf("%d ComputationLogDreadLock [%d] ENTER\n",Self->UniqID, (int)lock);
    ComputationLog* log;
    log = &Self->CLog;
    ComputationLogEntry* e;
    if (!log->Free) {
      ComputationLogExtend(log);
    }
    e = log->Free;
    e->Type = LOG_TYPE_DREADLOCK;
    e->Mutex = lock;
    log->Free = log->Free->Next;
  }

  /*
   * kills the most recently added thing
   */
  static void ComputationLogPop (Thread* Self)
  {
    if(DEBUG) printf("%d ComputationLogPop\n",Self->UniqID);
    ComputationLog* log;
    log = &Self->CLog;
    ComputationLogEntry* e = log->Free->Prev;
    // for now,
    assert(e->Type == LOG_TYPE_QLOCK); 
    log->Free = log->Free->Prev;
  }

  /* add a context to the log.
   * NOTE: the way this is implemented, you can never rollback past
   * the first checkpoint because the context is saved just before it 
   * is added to the list. if we rollback here, we'll add it to the list
   * again.
   */
  static void ComputationLogContext (Thread* Self, ucontext_t *ctx, int i)
  {
    if(DEBUG) print_log(Self->UniqID,&Self->CLog);
    if(DEBUG) printf("%d ComputationLogContext [%d]\n",Self->UniqID, ctx);
    ComputationLog* log;
    log = &Self->CLog;
    ComputationLogEntry* e;
    if (!log->Free) {
      ComputationLogExtend(log);
    }
    e = log->Free;
    e->Type = LOG_TYPE_CONTEXT;
    e->Context = ctx;
    e->Context_iter = i;
    log->Free = log->Free->Next;
  }

  // * * * * * * * * * * * * * * * * * * * * * * * * * * 
  // search backwords,
  //    playing inverses, unlocking etc.
  //
  //    if release_key==-1,
  //        setcontext() on the *second* context
  //    otherwise,
  //        setcontext() on the first context
  //        that preceeds unlock(release_key)
  //
  // * * * * * * * * * * * * * * * * * * * * * * * * * * 
  static void ComputationLogRollback (Thread *Self, int release_key)
  {
    int invoke_context = 0;
    ComputationLog* log = &Self->CLog;
    ComputationLogEntry* e;
    e = log->Free->Prev;
    if(DEBUG) printf("%d Rollback: for %d\n", Self->UniqID, release_key);
    while (e != NULL) { // && e->Prev != NULL) {
      if(DEBUG) print_log(Self->UniqID,&Self->CLog);
      if(DEBUG) fflush(stdout);
      if(e->Type == LOG_TYPE_INVERSE) {
	PlayInverse(e->numArgs,e->arg,e->Inverse);

      } else if(e->Type == LOG_TYPE_KEY) {
	Unlock(e->Mutex, Self);

      } else if(e->Type == LOG_TYPE_DREADLOCK) {
	DreadLock_Unlock(e->Mutex, Self);

      } else if(e->Type == LOG_TYPE_QLOCK) {
	if(DEBUG) printf("%d Rollback: queue unlock(%d)\n", Self->UniqID, e->QLHandle->key);
	QLockKey_Unlock(Self,e->QLHandle);
	if(e->QLHandle->key == release_key &&   // key needed
	   e->QLHandle->pred != NULL) {         // not a fake handle
	  if(DEBUG) printf("%d Rollback: found release_key. will invoke next ctx\n", Self->UniqID);	  
	  invoke_context = 1;
	}

      } else if(e->Type == LOG_TYPE_CONTEXT) {

	if(invoke_context) {
	  // pull out the context, truncate the log
	  ucontext_t *ctx = e->Context;
	  Self->iter = e->Context_iter;
	  e->Context = NULL;
	  log->Free = e;
	  if(DEBUG) printf("%d Rollback: truncated log. invoke context [%d]\n", Self->UniqID, ctx);
	  Self->PreemptKey = -1;
	  setcontext(ctx);
	} else {
	  if(DEBUG) printf("%d Rollback: ignoring context (%d)\n", Self->UniqID, e->Context);
	  // don't free because this is a context that was malloc()ed  in priority.c,
	  // and if we free it, when we return to this context, we'll be using a dangling pointer
	  //free(e->Context);
	}

	// invoke the next context
	if(release_key == -1) invoke_context = 1;

      } else { assert(0); }
      log->Free = e;
      e = e->Prev;
    }
    if(DEBUG) printf("%d Rollback: empty log!\n", Self->UniqID);
    TxAbort(Self);
  }

  __INLINE__ void
  ComputationLogOnCommit (Thread* Self, ComputationLog* log)
  {
    //fprintf(stderr,"%d DreadLock: commit\n",Self->UniqID);
    ComputationLogEntry* e;
    e = log->Free->Prev;
    while (e != NULL) { // && e->Prev != NULL) {
      if(e->Type == LOG_TYPE_KEY)
	Unlock(e->Mutex, Self);
      else if(e->Type == LOG_TYPE_DREADLOCK)
	DreadLock_Unlock(e->Mutex, Self);
      else if(e->Type == LOG_TYPE_QLOCK)
	QLockKey_Unlock(Self,e->QLHandle);
      else if(e->Type == LOG_TYPE_CONTEXT) {
	free(e->Context);
	e->Context = NULL;
      } else { }
      e = e->Prev;
    }
    if(Self->DreadlockTimeout < 0) {
      SET_CLEAR(Self->digest);
    }
    log->Free = log->First;
  }

  /*
   * on abort, play inverses, release locks
   */

  __INLINE__ 
  void ComputationLogOnAbort (Thread *Self, ComputationLog* log)
  {
    if(DEBUG) printf("%d aborted!\n",Self->UniqID);
    if(DEBUG) print_log(Self->UniqID,&Self->CLog);
    ComputationLogEntry* e;
    e = log->Free->Prev;
    while (e != NULL) {
      if(e->Type == LOG_TYPE_INVERSE) {
	PlayInverse(e->numArgs,e->arg,e->Inverse);

      } else if(e->Type == LOG_TYPE_KEY) {
	Unlock(e->Mutex, Self);

      } else if(e->Type == LOG_TYPE_DREADLOCK) {
	DreadLock_Unlock(e->Mutex, Self);

      } else if(e->Type == LOG_TYPE_QLOCK) {
	QLockKey_Unlock(Self,e->QLHandle);

      } else if(e->Type == LOG_TYPE_CONTEXT) {
	if(DEBUG) printf("free context: %d\n",e->Context);
	//free(e->Context);
	e->Context = NULL;

      } else { assert(0); }
      e = e->Prev;
    }
    if(Self->DreadlockTimeout < 0) {
      SET_CLEAR(Self->digest);
    }
    if(DEBUG) printf("%d abort roll back complete.\n", Self->UniqID);
    log->Free = log->First;
  }

  static void ThreadIterSet(Thread *Self,int i) { Self->iter = i; }
  static int  ThreadIterGet(Thread *Self) { return Self->iter; }

  static void ComputationLogShow (Thread *Self)
  {
    print_log(Self->UniqID,&Self->CLog);
  }

/* =============================================================================
 * TxInitThread
 * =============================================================================
 */

//#define DREADLOCKS_BLOOM 1

__INLINE__ void
TxInitThread (Thread* t, int id)
{
  /* default value for most members */
  memset(t,0,sizeof(*t));

  t->UniqID = id;
  t->NumThread = 0;
  t->iter = 0;
  t->isHigh = 0;
  t->Timeouts = 0;
  t->Deadlocks = 0;
  t->TxnNo = -1;
  t->PreemptKey = -1;
  t->rng = (TL2_TIMER_READ() ^ id) | 1;
  t->xorrng[0] = t->rng;


  t->DreadlockTimeout = -1;

  t->TL2Timeouts = 0;
  t->TL2Deadlocks = 0;

#ifdef TL2_OPTIM_HASHLOG
  MakeHashLog(&t->wrSet, HASHLOG_INIT_NUM_LOG, HASHLOG_INIT_NUM_ENTRY_PER_LOG, t);
#else /* !TL2_OPTIM_HASHLOG */
  t->wrSet.List = MakeList(0, TL2_INIT_WRSET_NUM_ENTRY, t);
  t->wrSet.put = t->wrSet.List;
#endif /* !TL2_OPTIM_HASHLOG */

  t->rdSet.List = MakeList(0, TL2_INIT_RDSET_NUM_ENTRY, t);
  t->rdSet.put = t->rdSet.List;

  t->LocalUndo.List = MakeList(0, TL2_INIT_LOCAL_NUM_ENTRY, t);
  t->LocalUndo.put = t->LocalUndo.List;
#ifdef XBOOST_PARTIAL
  ComputationLogNew(&(t->CLog), COMPUTATION_LOG_NUM_ENTRY);
  memset(t->QL_KeyList,0,(QUEUELOCK_TABLE_SIZE * sizeof(int)));
#else
  AbstractLogNew(&(t->ALog), ABSTRACT_LOG_NUM_ENTRY);
  InverseLogNew(&(t->ILog), INVERSE_LOG_NUM_ENTRY);
#endif

  global_tid2ptr[id] = t;
}

__INLINE__ void TxHighThread (Thread* t) { t->isHigh = 1; }
__INLINE__ void TxDreadlockTimeout (Thread* t, int to) {
  t->DreadlockTimeout = to;
  if(to < 0) {
#ifdef DREADLOCKS_BIGBLOOM
    t->digest = malloc(sizeof(BLOOMFILTER));
    t->digestID = malloc(sizeof(BLOOMFILTER));
    SET_HASH(t->digestID,t->UniqID + 1);
    //t->digestID = SET_GETHASH(t->UniqID + 1);
    //t->digest = bloom_create(13);
    //t->digestID = bloom_getID(t->digest,t->UniqID + 1);
#elif defined(DREADLOCKS_SMALLBLOOM)
    t->digest = malloc(sizeof(unsigned int));
    t->digestID = bloom32_gethash(t->UniqID + 1);
#elif defined(DREADLOCKS_MIDVEC)
    t->digest = malloc(sizeof(BITMAP128));
    t->digestID = t->UniqID;
#elif defined(DREADLOCKS_BIGVEC)
    t->digest = malloc(sizeof(BITMAP256));
    t->digestID = t->UniqID;
#else
    t->digest = malloc(sizeof(int));
    t->digestID = t->UniqID; 
    assert(t->digestID < 32);
#endif
    assert(t->digest != NULL);
    SET_CLEAR(t->digest);
  } else {
    // initialize digest to point to something that's not null
    t->digest = 0x1;
  }
}


/* =============================================================================
 * TxNewThread
 * =============================================================================
 */
__INLINE__ void
TxNewThread (Thread* t)
{
  PROF_STM_NEWTHREAD_BEGIN();

  /* CCM: so we can access TL2's thread metadata in signal handlers */
  pthread_setspecific(global_key_self, (void*)t);

  PROF_STM_NEWTHREAD_END();
}


/* =============================================================================
 * TxReset
 * =============================================================================
 */
__INLINE__ void
TxReset (Thread* Self)
{
  ASSERT(Self->LockList == NULL);
  Self->Mode = 0;

#ifdef TL2_OPTIM_HASHLOG
  if (Self->wrSet.numEntry > 0) {
    int numLog = Self->wrSet.numLog;
    Log* logs = Self->wrSet.logs;
    Log* end = logs + numLog;
    Log* log;
    for (log = logs; log != end; log++) {
      log->put = log->List;
      log->tail = NULL;
    }
  }
  Self->wrSet.numEntry = 0;
#else /* !TL2_OPTIM_HASHLOG */
  Self->wrSet.put = Self->wrSet.List;
  Self->wrSet.tail = NULL;
#endif /* !TL2_OPTIM_HASHLOG */

  Self->wrSet.BloomFilter = 0;
  Self->rdSet.put = Self->rdSet.List;
  Self->rdSet.tail = NULL;

  Self->LocalUndo.put = Self->LocalUndo.List;
  Self->LocalUndo.tail = NULL;
  Self->HoldsLocks = 0;
}


/*
 * Remarks on deadlock:
 * Indefinite spinning in the lock acquisition phase admits deadlock.
 * We can avoid deadlock by any of the following means:
 *
 * 1. Bounded spin with back-off and retry.
 *    If the we fail to acquire the lock within the interval we drop all
 *    held locks, delay (back-off - either random or exponential), and retry
 *    the entire txn.
 *
 * 2. Deadlock detection - detect and recover.
 *    Use a simple waits-for graph to detect deadlock.  We can recovery
 *    either by aborting *all* the participant threads, or we can arbitrate.
 *    One thread becomes the winner and is allowed to proceed or continue
 *    spinning.  All other threads are losers and must abort and retry.
 *
 * 3. Prevent or avoid deadlock by acquiring locks in some order.
 *    Address order using LockFor as the key is the most natural.
 *    Unfortunately this requires sorting -- See the LockRecord structure.
 */


/* =============================================================================
 * OwnerOf
 * =============================================================================
 */
__INLINE__ Thread*
OwnerOf (vwLock v)
{
  return (v & LOCKBIT) ? ((AVPair*) (v ^ LOCKBIT))->Owner : NULL;
}


/*
 * With PS the versioned lock words (the LockTab array) are table stable and
 * references will never fault.  Under PO, however, fetches by a doomed
 * zombie txn can fault if the referent is free()ed and unmapped
 */
#if 0
#  define LDLOCK(a)    LDNF(a)  /* for PO */
#else
#  define LDLOCK(a)    *(a)     /* for PS */
#endif


/* =============================================================================
 * ReadSetCoherent
 * -- Is the read-set mutually consistent?
 * -- Can be called at any time--before the caller acquires locks or after
 * =============================================================================
 */
__INLINE__ int
ReadSetCoherent (Thread* Self)
{
  intptr_t dx = 0;
  vwLock rv = Self->rv;
  Log* const rd = &Self->rdSet;
  AVPair* const EndOfList = rd->put;
  AVPair* e;

  ASSERT((rv & LOCKBIT) == 0);

  for (e = rd->List; e != EndOfList; e = e->Next) {
    ASSERT(e->LockFor != NULL);
    vwLock v = LDLOCK(e->LockFor);
    if (v & LOCKBIT) {
      dx |= UNS(((AVPair*)(UNS(v) & ~LOCKBIT))->Owner) ^ UNS(Self);
    } else {
      dx |= (v > rv);
    }
  }

  return (dx == 0);
}


/* =============================================================================
 * RestoreLocks
 * =============================================================================
 */
__INLINE__ void
RestoreLocks (Thread* Self)
{
#ifdef TL2_OPTIM_HASHLOG
  int numLog = Self->wrSet.numLog;
  Log* logs = Self->wrSet.logs;
  Log* end = logs + numLog;
  Log* wr;
  for (wr = logs; wr != end; wr++)
#else /* !TL2_OPTIM_HASHLOG*/
    Log* wr = &Self->wrSet;
#endif /* !TL2_OPTIM_HASHLOG*/
  {
    AVPair* p;
    AVPair* const End = wr->put;
    for (p = wr->List; p != End; p = p->Next) {
      ASSERT(p->Addr != NULL);
      ASSERT(p->LockFor != NULL);
      if (p->Held == 0) {
	continue;
      }
      ASSERT(OwnerOf(*(p->LockFor)) == Self);
      ASSERT(*(p->LockFor) == (UNS(p)|LOCKBIT));
      ASSERT((p->rdv & LOCKBIT) == 0);
      p->Held = 0;
      *(p->LockFor) = p->rdv;
    }
  }
  Self->HoldsLocks = 0;
}


/* =============================================================================
 * DropLocks
 * =============================================================================
 */
__INLINE__ void
DropLocks (Thread* Self, vwLock wv)
{
#ifdef TL2_OPTIM_HASHLOG
  int numLog = Self->wrSet.numLog;
  Log* logs = Self->wrSet.logs;
  Log* end = logs + numLog;
  Log* wr;
  for (wr = logs; wr != end; wr++)
#else /* !TL2_OPTIM_HASHLOG*/
    Log* wr = &Self->wrSet;
#endif /* !TL2_OPTIM_HASHLOG*/
  {
    AVPair* p;
    AVPair* const End = wr->put;
    ASSERT((wv & LOCKBIT) == 0);
    for (p = wr->List; p != End; p = p->Next) {
      ASSERT(p->Addr != NULL);
      ASSERT(p->LockFor != NULL);
      if (p->Held == 0) {
	continue;
      }
      p->Held = 0;
#if _GVCONFIGURATION == 4
      ASSERT(wv > p->rdv);
#else
      /* GV5 and GV6 admit wv == p->rdv */
      ASSERT(wv >= p->rdv);
#endif
      ASSERT(OwnerOf(*(p->LockFor)) == Self);
      ASSERT(*(p->LockFor) == (UNS(p)|LOCKBIT));
      *(p->LockFor) = wv;
    }
  }
  Self->HoldsLocks = 0;
}


/* =============================================================================
 * backoff
 * =============================================================================
 */
__INLINE__ void
backoff (Thread* Self, int attempt)
{
#ifdef TL2_BACKOFF_EXPONENTIAL
  unsigned long long n = 1 << ((attempt < 63) ? (attempt) : (63));
  unsigned long long stall = TSRandom(Self) % n;
#else
  unsigned long long stall = TSRandom(Self) & 0xF;
  stall += attempt >> 2;
  stall *= 10;
#endif
#if 0
  TL2_TIMER_T expiry = TL2_TIMER_READ() + stall;
  while (TL2_TIMER_READ() < expiry) {
    PAUSE();
  }
#else
  {
    /* CCM: timer function may misbehave */
    volatile typeof(stall) i = 0;
    while (i++ < stall) {
      PAUSE();
    }
  }
#endif
}


/* =============================================================================
 * TryFastUpdate
 * =============================================================================
 */
__INLINE__ int
TryFastUpdate (Thread* Self)
{
#ifdef TL2_OPTIM_HASHLOG
  int numLog = Self->wrSet.numLog;
  Log* logs = Self->wrSet.logs;
  Log* end = logs + numLog;
  Log* wr;
#else /* !TL2_OPTIM_HASHLOG*/
  Log* const wr = &Self->wrSet;
#endif /* !TL2_OPTIM_HASHLOG*/
  Log* const rd = &Self->rdSet;
  int ctr;
  vwLock wv;

  ASSERT(Self->Mode == TTXN);

  /*
   * Optional optimization -- pre-validate the read-set.
   *
   * Consider: Call ReadSetCoherent() before grabbing write-locks.
   * Validate that the set of values we've fetched from pure READ objects
   * remain coherent.  This avoids the situation where a doomed transaction
   * grabs write locks and impedes or causes other potentially successful
   * transactions to spin or abort.
   *
   * A smarter tactic might be to only call ReadSetCoherent() when
   * Self->Retries > NN
   */

#if 0
  if (!ReadSetCoherent(Self)) {
    return 0;
  }
#endif

  /*
   * Consider: if the write-set is long or Self->Retries is high we
   * could run a pre-pass and sort the write-locks by LockFor address.
   * We could either use a separate LockRecord list (sorted) or
   * link the write-set entries via SortedNext
   */

  /*
   * Lock-acquisition phase ...
   *
   * CONSIDER: While iterating over the locks that cover the write-set
   * track the maximum observed version# in maxv.
   * In GV4:   wv = GVComputeWV(); ASSERT wv > maxv
   * In GV5|6: wv = GVComputeWV(); if (maxv >= wv) wv = maxv + 2
   * This is strictly an optimization.
   * maxv isn't required for algorithmic correctness
   */
  Self->HoldsLocks = 1;
  ctr = 1000; /* Spin budget - TUNABLE */
  vwLock maxv = 0;
#ifdef TL2_OPTIM_HASHLOG
  for (wr = logs; wr != end; wr++)
#endif /* TL2_OPTIM_HASHLOG*/
    {
      AVPair* p;
      AVPair* const End = wr->put;
      for (p = wr->List; p != End; p = p->Next) {
	volatile vwLock* const LockFor = p->LockFor;
	vwLock cv;
	ASSERT(p->Addr != NULL);
	ASSERT(p->LockFor != NULL);
	ASSERT(p->Held == 0);
	ASSERT(p->Owner == Self);
	/* Consider prefetching only when Self->Retries == 0 */
	prefetchw(LockFor);
	cv = LDLOCK(LockFor);
	if ((cv & LOCKBIT) && ((AVPair*)(cv ^ LOCKBIT))->Owner == Self) {
	  /* Already locked by an earlier iteration. */
	  continue;
	}

	if (FindFirst(rd, LockFor) != NULL) {
	  /*
	   * READ-WRITE stripe
	   */
	  if ((cv & LOCKBIT) == 0 &&
	      cv <= Self->rv &&
	      UNS(CAS(LockFor, cv, (UNS(p)|UNS(LOCKBIT)))) == UNS(cv))
	    {
	      if (cv > maxv) {
		maxv = cv;
	      }
	      p->rdv  = cv;
	      p->Held = 1;
	      continue;
	    }
	  /*
	   * The stripe is either locked or the previously observed read-
	   * version changed.  We must abort. Spinning makes little sense.
	   * In theory we could spin if the read-version is the same but
	   * the lock is held in the faint hope that the owner might
	   * abort and revert the lock
	   */
	  Self->abv = cv;
	  RestoreLocks(Self);
	  Self->TL2Timeouts++;
	  return 0;
	} else {
	  /*
	   * WRITE-ONLY stripe
	   * Note that we already have a fresh copy of *LockFor in cv.
	   * If we find a write-set element locked then we can either
	   * spin or try to find something useful to do, such as :
	   * A. Validate the read-set by calling ReadSetCoherent()
	   *    We can abort earlier if the transaction is doomed.
	   * B. optimistically proceed to the next element in the write-set.
	   *    Skip the current locked element and advance to the
	   *    next write-set element, later retrying the skipped elements
	   */
	  int c = ctr;
	  for (;;) {
	    cv = LDLOCK(LockFor);
	    if ((cv & LOCKBIT) == 0 &&
		UNS(CAS(LockFor, cv, (UNS(p)|UNS(LOCKBIT)))) == UNS(cv))
	      {
		if (cv > maxv) {
		  maxv = cv;
		}
		p->rdv  = cv; /* save so we can restore or increment */
		p->Held = 1;
		break;
	      }
	    if (--c < 0) {
	      /* Will fall through to TxAbort */
	      Self->TL2Timeouts++;
	      return 0;
	    }
	    /*
	     * Consider: while spinning we might validate
	     * the read-set by calling ReadSetCoherent()
	     */
	    PAUSE();
	  }
	} /* write-only stripe */
      } /* foreach (entry in write-set) */
    }

  wv = GVGenerateWV(Self, maxv);

  /*
   * We now hold all the locks for RW and W objects.
   * Next we validate that the values we've fetched from pure READ objects
   * remain coherent.
   *
   * If GVGenerateWV() is implemented as a simplistic atomic fetch-and-add
   * then we can optimize by skipping read-set validation in the common-case.
   * Namely,
   *   if (Self->rv != (wv-2) && !ReadSetCoherent(Self)) { ... abort ... }
   * That is, we could elide read-set validation for pure READ objects if
   * there were no intervening write txns between the fetch of _GCLOCK into
   * Self->rv in TxStart() and the increment of _GCLOCK in GVGenerateWV()
   */

  if (!ReadSetCoherent(Self)) {
    /*
     * The read-set is inconsistent.
     * The transaction is spoiled as the read-set is stale.
     * The candidate results produced by the txn and held in
     * the write-set are a function of the read-set, and thus invalid
     */
    RestoreLocks(Self);
    return 0;
  }

  /*
   * We are now committed - this txn is successful.
   */

#ifdef TL2_OPTIM_HASHLOG
  for (wr = logs; wr != end; wr++)
#endif /* TL2_OPTIM_HASHLOG*/
    {
      WriteBack(wr); /* write-back the deferred stores */
    }
  MEMBARSTST(); /* Ensure the above stores are visible  */
  DropLocks(Self, wv); /* Release locks and increment the version */

  /*
   * Ensure that all the prior STs have drained before starting the next
   * txn.  We want to avoid the scenario where STs from "this" txn
   * languish in the write-buffer and inadvertently satisfy LDs in
   * a subsequent txn via look-aside into the write-buffer
   */
  MEMBARSTLD();

  return 1; /* success */
}


/*
 * We use a degenerate Bloom filter with only one hash function generating
 * a single bit.  A traditional Bloom filter use multiple hash functions and
 * multiple bits.  Relatedly, the size our filter is small, so it can saturate
 * and become useless with a rather small write-set.
 * A better solution might be small per-thread hash tables keyed by address that
 * point into the write-set.
 * Beware that 0x1F == (MIN(sizeof(int),sizeof(intptr_t))*8)-
 *
 * CCM: enable per-thread hash tables with -DTL2_OPTIM_HASHLOG
 */

#define FILTERHASH(a)   ((UNS(a) >> 2) ^ (UNS(a) >> 5))
#define FILTERBITS(a)   (1 << (FILTERHASH(a) & 0x1F))

/*
 * PSLOCK: maps variable address to lock address.
 * For PW the mapping is simply (UNS(addr)+sizeof(int))
 * COLOR attempts to place the lock(metadata) and the data on
 * different D$ indexes.
 */

#define TABMSK        (_TABSZ-1)

/*
  #define COLOR         0
  #define COLOR         (256-16)
*/
#define COLOR         (128)

/*
 * Alternate experimental mapping functions ....
 * #define PSLOCK(a)     (LockTab + 0)                                   // PS1
 * #define PSLOCK(a)     (LockTab + ((UNS(a) >> 2) & 0x1FF))             // S512
 * #define PSLOCK(a)     (LockTab + (((UNS(a) >> 2) & (TABMSK & ~0x7)))) // PS1M
 * #define PSLOCK(a)     (LockTab + (((UNS(a) >> 6) & (TABMSK & ~0x7)))) // PS1M
 * #define PSLOCK(a)     (LockTab + ((((UNS(a) >> 2)|0xF) & TABMSK)))    // PS1M
 * #define PSLOCK(a)     (LockTab + (-(UNS(a) >> 2) & TABMSK))
 * #define PSLOCK(a)     (LockTab + ((UNS(a) >> 6) & TABMSK))
 * #define PSLOCK(a)     (LockTab + ((UNS(a) >> 2) & TABMSK))            // PS1
 */

/*
 * ILP32 vs LP64.  PSSHIFT == Log2(sizeof(intptr_t)).
 */
#define PSSHIFT        ((sizeof(void*) == 4) ? 2 : 3)

#define PSLOCK(a)    (LockTab + (((UNS(a)+COLOR) >> PSSHIFT) & TABMSK)) /* PS1M */


#if defined(TL2_OPTIM_HASHLOG) && defined(TL2_RESIZE_HASHLOG)
/* =============================================================================
 * resizeHashLog
 * =============================================================================
 */
__INLINE__ void
resizeHashLog (HashLog* hlPtr, Thread* Self)
{

  int oldNumLog = hlPtr->numLog;
  int newNumLog = oldNumLog* HASHLOG_GROWTH_FACTOR;
  Log* oldLogs = hlPtr->logs;
  Log* newLogs;
  Log* end;
  Log* log;

  /* Create new logs */
  newLogs = (Log*)calloc(newNumLog, sizeof(Log));
  end = newLogs + newNumLog;
  for (log = newLogs; log != end; log++) {
    log->List = MakeList(NULL, HASHLOG_INIT_NUM_ENTRY_PER_LOG, Self);
    log->put = log->List;
  }

  /* Move from old logs to new ones */
  end = oldLogs + oldNumLog;
  for (log = oldLogs; log != end; log++) {
    AVPair* oldEntry;
    AVPair* const End = log->put;
    for (oldEntry = log->List; oldEntry != End; oldEntry = oldEntry->Next) {
      volatile intptr_t* addr = oldEntry->Addr;
      int hash = HASHLOG_HASH(addr) % newNumLog;
      Log* newLog = &newLogs[hash];
      AVPair* newEntry = newLog->put;
      if (newEntry == NULL) {
	newEntry = ExtendList(newLog->tail);
      }
      newLog->tail      = newEntry;
      newLog->put       = newEntry->Next;
      newEntry->Addr    = addr;
      newEntry->Valu    = oldEntry->Valu;
      newEntry->LockFor = oldEntry->LockFor;
      newEntry->Held    = oldEntry->Held;
      newEntry->rdv     = oldEntry->rdv;
    }
  }

  /* Point HashLog to new logs */
  hlPtr->numLog = newNumLog;
  hlPtr->logs = newLogs;
}
#endif /* TL2_OPTIM_HASHLOG && TL2_RESIZE_HASHLOG */


/* =============================================================================
 * TxAbort
 *
 * Our mechanism admits mutual abort with no progress - livelock.
 * Consider the following scenario where T1 and T2 execute concurrently:
 * Thread T1:  WriteLock A; Read B LockWord; detect locked, abort, retry
 * Thread T2:  WriteLock B; Read A LockWord; detect locked, abort, retry
 *
 * Possible solutions:
 *
 * - Try backoff (random and/or exponential), with some mixture
 *   of yield or spinning.
 *
 * - Use a form of deadlock detection and arbitration.
 *
 * In practice it's likely that a semi-random semi-exponential back-off
 * would be best.
 * =============================================================================
 */
__INLINE__ void
TxAbort (Thread* Self)
{

  PROF_STM_ABORT_BEGIN();

  //fprintf(stderr,"%d TxAbort -- about to longjump\n",Self->UniqID);

  //if(Self->Retries > 20000) {
  //fprintf(stderr,"%d still can't get it. clog:\n",Self->UniqID);
  //ComputationLogShow(Self);    
  //}

#if defined(XBOOST_PARTIAL)
  ComputationLogOnAbort(Self,&Self->CLog);
#else
  InverseLogOnAbort(&Self->ILog);
  AbstractLogRelease(Self,&Self->ALog);
#endif

  if (Self->HoldsLocks) {
    RestoreLocks(Self);
  }

  /* Clean up after an abort. Restore any modified locals */
  if (Self->LocalUndo.put != Self->LocalUndo.List) {
    WriteBack(&Self->LocalUndo);
  }

  Self->Retries++;
  Self->Aborts++;
  Self->Mode = TABORTED;
  Self->PreemptKey = -1;

  if (GVAbort(Self)) {
    /* possibly advance _GCLOCK for GV5 or GV6 */
    goto __rollback;
  }

  /*
   * Beware: back-off is useful for highly contended environments
   * where N threads shows negative scalability over 1 thread.
   * Extreme back-off restricts parallelism and, in the extreme,
   * is tantamount to allowing the N parallel threads to run serially
   * 1 at-a-time in succession.
   *
   * Consider: make the back-off duration a function of:
   * + a random #
   * + the # of previous retries
   * + the size of the previous read-set
   * + the size of the previous write-set
   *
   * Consider using true CSMA-CD MAC style random exponential backoff
   */

  if (Self->Retries > 3) { /* TUNABLE */
    //backoff(Self, Self->Retries);
  }

 __rollback:
  PROF_STM_ABORT_END();
  SIGLONGJMP(*Self->envPtr, 1);
  ASSERT(0);
}



/* =============================================================================
 * TxStore
 * =============================================================================
 */
__INLINE__ void
TxStore (Thread* Self, volatile intptr_t* addr, intptr_t valu)
{
  PROF_STM_WRITE_BEGIN();
  volatile vwLock* LockFor;
  vwLock rdv;

  ASSERT(m == TTXN);

  /*
   * In TL2 we're always coherent, so we should never see NULL stores.
   * In TL it was possible to see NULL stores in zombie txns.
   */

  if (Self->IsRO) {
    *(Self->ROFlag) = 0;
    PROF_STM_WRITE_END();
    TxAbort(Self);
    return;
  }
#ifdef TL2_STATS
  Self->TxST++;
#endif

  LockFor = PSLOCK(addr);

#if 0
  /* CONSIDER: prefetch both the lock and the data */
  if (Self->Retries == 0) {
    prefetchw(addr);
    prefetchw(LockFor);
  }
#endif

  /*
   * CONSIDER: spin briefly (bounded) while the object is locked,
   * periodically calling ReadSetCoherent(Self)
   */

#if 1
  rdv = LDLOCK(LockFor);
#else
  {
    int ctr = 100; /* TUNABLE */
    for (;;) {
      rdv = LDLOCK(LockFor);
      if ((rdv & LOCKBIT) == 0) {
	break;
      } else if ((ctr & 0x1F) == 0 && !ReadSetCoherent(Self)) {
	PROF_STM_WRITE_END();
	TxAbort(Self);
	return;
      } else if (--ctr < 0) {
	PROF_STM_WRITE_END();
	TxAbort(Self);
	return;
      }
    }
  }
#endif

#ifdef TL2_OPTIM_HASHLOG
  HashLog* wrSet = &Self->wrSet;
  int numLog = wrSet->numLog;
  int hash = HASHLOG_HASH(addr) % numLog;
  Log* wr = &wrSet->logs[hash];
#else /* !TL2_OPTIM_HASHLOG*/
  Log* wr = &Self->wrSet;
#endif /* !TL2_OPTIM_HASHLOG*/
  /*
   * Convert a redundant "idempotent" store to a tracked load.
   * This helps minimize the wrSet size and reduces false+ aborts.
   * Conceptually, "a = x" is equivalent to "if (a != x) a = x"
   * This is entirely optiona
   */
  MEMBARLDLD();

  if (ALWAYS && LDNF(addr) == valu) {
    AVPair* e;
    for (e = wr->tail; e != NULL; e = e->Prev) {
      ASSERT(e->Addr != NULL);
      if (e->Addr == addr) {
	ASSERT(LockFor == e->LockFor);
	e->Valu = valu; /* CCM: update associated value in write-set */
	PROF_STM_WRITE_END();
	return;
      }
    }
    /* CCM: Not writing new value; convert to load */
    if ((rdv & LOCKBIT) == 0 && rdv <= Self->rv && LDLOCK(LockFor) == rdv) {
      TrackLoad(Self, LockFor);
      PROF_STM_WRITE_END();
      return;
    }
  }

#ifdef TL2_OPTIM_HASHLOG
  wrSet->BloomFilter |= FILTERBITS(addr) ;
#else /* !TL2_OPTIM_HASHLOG*/
  wr->BloomFilter |= FILTERBITS(addr) ;
#endif /* !TL2_OPTIM_HASHLOG*/

#ifdef TL2_OPTIM_HASHLOG
  int numEntry = wrSet->numEntry++;
#endif /* TL2_OPTIM_HASHLOG*/

  RecordStore(wr, addr, valu, LockFor);

#if defined(TL2_OPTIM_HASHLOG) && defined(TL2_RESIZE_HASHLOG)
  if (numEntry > (numLog * HASHLOG_RESIZE_RATIO)) {
    resizeHashLog(wrSet, Self);
  }
#endif /* TL2_OPTIM_HASHLOG && TL2_RESIZE_HASHLOG */

  PROF_STM_WRITE_END();
}


/* =============================================================================
 * TxLoad
 * =============================================================================
 */
__INLINE__ intptr_t
TxLoad (Thread* Self, volatile intptr_t* Addr)
{
  PROF_STM_READ_BEGIN();

  intptr_t Valu;

  ASSERT(m == TTXN);
#ifdef TL2_STATS
  Self->TxLD++;
#endif

  /*
   * Preserve the illusion of processor consistency in run-ahead mode.
   * Look-aside: check the wrSet for RAW hazards.
   * This is optional, but it improves the quality and fidelity
   * of the wrset and rdset compiled during speculative mode.
   * Consider using a Bloom filter on the addresses in wrSet to give us
   * a statistically fast out if the address doesn't appear in the set
   */

  intptr_t msk = FILTERBITS(Addr);
  if ((Self->wrSet.BloomFilter & msk) == msk) {
#ifdef TL2_OPTIM_HASHLOG
    Log* wr = &Self->wrSet.logs[HASHLOG_HASH(Addr) % Self->wrSet.numLog];
#else /* !TL2_OPTIM_HASHLOG*/
    Log* wr = &Self->wrSet;
#endif /* !TL2_OPTIM_HASHLOG*/
    AVPair* e;
    for (e = wr->tail; e != NULL; e = e->Prev) {
      ASSERT(e->Addr != NULL);
      if (e->Addr == Addr) {
	ASSERT(LockFor == e->LockFor);
	PROF_STM_READ_END();
	return e->Valu;
      }
    }
  }

  /*
   * TODO-FIXME:
   * Currently we set Self->rv in TxStart().
   * We might be better served to defer reading Self->rv
   * until the 1st transactional load.
   * if (Self->rv == 0) Self->rv = _GCLOCK
   */

  /*
   * Fetch tentative value
   * Use either SPARC non-fault loads or complicit signal handlers.
   * If the LD fails we'd like to call TxAbort()
   * TL2 does not permit zombie/doomed txns to run
   */
  volatile vwLock* LockFor = PSLOCK(Addr);
  vwLock rdv = LDLOCK(LockFor) & ~LOCKBIT;
  MEMBARLDLD();
  Valu = LDNF(Addr);
  MEMBARLDLD();
  if (rdv <= Self->rv && LDLOCK(LockFor) == rdv) {
    if (!Self->IsRO) {
      TrackLoad(Self, LockFor);
    }
    PROF_STM_READ_END();
    return Valu;
  }

  /*
   * The location is either currently locked or has been
   * updated since this txn started.  In the later case if
   * the read-set is otherwise empty we could simply re-load
   * Self->rv = _GCLOCK and try again.  If the location is
   * locked it's fairly likely that the owner will release
   * the lock by writing a versioned write-lock value that
   * is > Self->rv, so spinning provides little profit.
   */

  Self->abv = LDLOCK(LockFor);
  PROF_STM_READ_END();
  TxAbort(Self);
  ASSERT(0);

  return 0;
}


/* =============================================================================
 * TxSterilize
 *
 * Use TxSterilize() any time an object passes out of the transactional domain
 * and will be accessed solely with normal non-transactional load and store
 * operations. TxSterilize() allows latent pending txn STs to drain before allowing
 * the object to escape.  This avoids use-after-free errors, for instance.
 * Note that we need to know or track the length of the malloc()ed objects.
 * In practice, however, most malloc() subsystems can compute the object length
 * in a very efficient manner, so a simple extension to the malloc()-free()
 * interface would suffice.
 * =============================================================================
 */
__INLINE__ void
TxSterilize (Thread* Self, void* Base, size_t Length)
{
  PROF_STM_STERILIZE_BEGIN();

  intptr_t* Addr = (intptr_t*) Base;
  intptr_t* End = Addr + Length;
  ASSERT(Addr <= End);
  while (Addr < End) {
    volatile vwLock* Lock = PSLOCK(Addr);
    Addr++;
    while (*Lock & LOCKBIT) {
      /* wait */
    }
  }

  PROF_STM_STERILIZE_END();
}


/* =============================================================================
 * TxStoreLocal
 * -- Update in-place, saving the original contents in the undo log
 * =============================================================================
 */
__INLINE__ void
TxStoreLocal (Thread* Self, volatile intptr_t* Addr, intptr_t Valu)
{
  PROF_STM_WRITELOCAL_BEGIN();

  SaveForRollBack(&Self->LocalUndo, Addr, *Addr);
  *Addr = Valu;

  PROF_STM_WRITELOCAL_END();
}


/* =============================================================================
 * TxStart
 * =============================================================================
 */
__INLINE__ void
TxStart (Thread* Self, jmp_buf* envPtr, int* ROFlag)
{
  PROF_STM_START_BEGIN();

  if (Self->Mode == TABORTED) {
    Self->Mode = TIDLE;
    if (Self->ovf != 0) {
      /*
       * Most aborts are handled immediately at the time we detect the
       * problem. An abort because of an overflow, however, is deferred
       * until the the next call to TxStart().  This is intentional, as
       * it means the code in TrackLoad() does not need to call TxAbort(),
       * thus making it either a leaf or eligible for more aggressive
       * inlining by the C compiler. That is, instead of having TrackLoad()
       * call SpeculativeReadSetCoherent() and TxAbort(), TrackLoad()
       * simply sets Self->ovf and we defer the triage until the next call
       * to TxStart(). If we aborted because the read-set list capacity
       * was too small _and_ the operation was coherent the we grow the list.
       * WARNING: we assume that overflows are from rdSet ...
       */
      Self->ovf = 0;
      OverflowTally++;
      /*
       * CCM: Read set is not maintained. Aborts automatically generated
       * if read filter detects violation.
       */
      int coh = ReadSetCoherent(Self);
      if (coh) {
	ExtendList(Self->rdSet.tail);
      }
      Self->rdSet.put  = Self->rdSet.List;
    }
  }

  do {
    int prev = global_txn_count;
    int res  = CAS(&global_txn_count,prev,prev+1);
    if(res == prev) {
      Self->TxnNo = prev;

      //ifdef DREADLOCKS_SMALLBLOOM
      // EJK
      //Self->digestID = bloom32_gethash(Self->TxnNo);
      //printf("swap to txn id %d\n",Self->TxnNo);
      //endif

      break;
    }
  } while(1);

  ASSERT(Self->Mode == TIDLE);
  TxReset(Self);
  Self->Mode = TTXN;
  Self->rv = GVRead(Self);
  ASSERT((Self->rv & LOCKBIT) == 0);
  MEMBARLDLD();
  Self->ROFlag = ROFlag;
  Self->IsRO = ROFlag ? *ROFlag : 0;
  Self->envPtr= envPtr;

  ASSERT(Self->LocalUndo.put == Self->LocalUndo.List);
  ASSERT(Self->wrSet.put == Self->wrSet.List);

  PROF_STM_START_END();
}


/* =============================================================================
 * TxCommit
 * =============================================================================
 */
__INLINE__ int
TxCommit (Thread* Self)
{
  PROF_STM_COMMIT_BEGIN();

  ASSERT(Self->Mode == TTXN);


  /* Fast-path: Optional optimization for pure-readers */
#ifdef TL2_OPTIM_HASHLOG
  if (Self->wrSet.numEntry == 0)
#else /* !TL2_OPTIM_HASHLOG*/
    if (Self->wrSet.put == Self->wrSet.List)
#endif /* !TL2_OPTIM_HASHLOG*/
      {
	/* Given TL2 the read-set is already known to be coherent. */
	TxReset(Self);
	Self->Retries = 0;
	Self->ovf = 0;
#if defined(TL2_OPTIM_HASHLOG) && defined(TL2_RESIZE_HASHLOG)
	if (Self->wrSet.numLog > HASHLOG_INIT_NUM_LOG) {
	  /*
	   * If we are read-only, reduce the number of logs so less time
	   * iterating over logs only to find that they are empty.
	   */
	  Self->wrSet.numLog--;
	}
#endif
#if defined(XBOOST_PARTIAL)
	ComputationLogOnCommit(Self,&Self->CLog);
#else
	InverseLogOnCommit(&Self->ILog); /* discard inverse log */
	AbstractLogRelease(Self,&Self->ALog);
#endif
	PROF_STM_COMMIT_END();
	PROF_STM_SUCCESS();
	return 1;
      }

  if (TryFastUpdate(Self)) {
    TxReset(Self);
    Self->Retries = 0;
    Self->ovf = 0;
#if defined(TL2_OPTIM_HASHLOG) && defined(TL2_RESIZE_HASHLOG)
    if (Self->wrSet.numLog > HASHLOG_INIT_NUM_LOG &&
	Self->wrSet.numEntry < HASHLOG_INIT_NUM_Log * HASHLOG_RESIZE_RATIO) {
      /*
       * Current hash log is too large. Reduce the number of logs so less
       * time is spent iterating over logs only to find that they are empty.
       */
      Self->wrSet.numLog--;
    }
#endif
#if defined(XBOOST_PARTIAL)
    ComputationLogOnCommit(Self,&Self->CLog);
#else
    InverseLogOnCommit(&Self->ILog); /* discard inverse log */
    AbstractLogRelease(Self,&Self->ALog);
#endif
    PROF_STM_COMMIT_END();
    PROF_STM_SUCCESS();
    return 1;
  }
  TxAbort(Self);
  PROF_STM_COMMIT_END();
  ASSERT(0);
  return 0;
}



#ifdef __cplusplus
}
#endif


#endif /* TL2_H */


/* =============================================================================
 *
 * End of tl2.h
 *
 * =============================================================================
 */

