/* =============================================================================
 *
 * tl2.c
 * -- Transactional Locking 2 software transactional memory
 *
 * =============================================================================
 *
 * Copyright (C) Sun Microsystems Inc., 2006.  All Rights Reserved.
 * Authors: Dave Dice, Nir Shavit, Ori Shalev.
 *
 * TL2: Transactional Locking for Disjoint Access Parallelism
 *
 * Transactional Locking II,
 * Dave Dice, Ori Shalev, Nir Shavit
 * DISC 2006, Sept 2006, Stockholm, Sweden.
 *
 * =============================================================================
 *
 * Modified by Chi Cao Minh (caominh@stanford.edu):
 *   Aug 2006 -- x86/Linux port
 *   Oct 2006 -- Hashlog for write-set
 *   Dec 2006 -- setjmp/longjmp
 *
 * =============================================================================
 */


#include "tl2.h"
#include "bloom32.h"
#include <pthread.h>
#include <signal.h>


#define mb() __asm__ __volatile__ ("lock; addl $0,0(%%esp)": : :"memory")
// define mb() alternative("lock; addl $0,0(%%esp)", "mfence", X86_FEATURE_XMM2)

pthread_key_t global_key_self;

int global_txn_count = 0;

Thread* global_tid2ptr[256]; // TODO : fix this

/* =============================================================================
 * GENERIC INFRASTRUCTURE
 * =============================================================================
 */

#ifdef TL2_STATS
volatile int       global_stats[4096];
#endif

/* =============================================================================
 * emulateNonFaultingLoad
 * =============================================================================
 */
void
emulateNonFaultingLoad (int signum)
{
    Thread* Self = (Thread*)pthread_getspecific(global_key_self);
    /* Re-register to catch the next one */
    if ((signal(SIGSEGV, &emulateNonFaultingLoad) == SIG_ERR) ||
        (signal(SIGBUS,  &emulateNonFaultingLoad) == SIG_ERR) ||
        (signal(SIGILL,  &emulateNonFaultingLoad) == SIG_ERR))
    {
        perror("error signal (non-faulting load emulation)");
        exit(1);
    }
    TxAbort(Self);
}


/* =============================================================================
 * GLOBAL VERSION-CLOCK MANAGEMENT
 * =============================================================================
 */

/*
 * Consider 4M alignment for LockTab so we can use large-page support.
 * Alternately, we could mmap() the region with anonymous DZF pages.
 */
volatile vwLock LockTab[_TABSZ];

/*
 * We use GClock[32] as the global counter.  It must be the sole occupant
 * of its cache line to avoid false sharing.  Even so, accesses to
 * GCLock will cause significant cache coherence & communication costs
 * as it is multi-read multi-write.
 */
volatile vwLock GClock[TL2_CACHE_LINE_SIZE];


/* =============================================================================
 * TL2 INFRASTRUCTURE
 * =============================================================================
 */

int OverflowTally = 0;


/* =============================================================================
 * TxOnce
 * =============================================================================
 */
void
TxOnce ()
{
    printf("TX system ready: ");
    GVInit();
    struct utsname un;
    uname(&un);
    printf("%s; ", un.nodename);
    printf("%s\n", _GVFLAVOR);

    pthread_key_create(&global_key_self, NULL);
    /* CCM: Set up handlers to emulate non-faulting load */
    if ((signal(SIGSEGV, &emulateNonFaultingLoad) == SIG_ERR) ||
        (signal(SIGBUS,  &emulateNonFaultingLoad) == SIG_ERR) ||
        (signal(SIGILL,  &emulateNonFaultingLoad) == SIG_ERR))
    {
        perror("error signal (non-faulting load emulation)");
        exit(1);
    }
}


/* =============================================================================
 * TxShutdown
 * =============================================================================
 */
void
TxShutdown ()
{
    printf("Shutdown: Overflows=%d GCLOCK=%lX\n",
           OverflowTally, (unsigned long)_GCLOCK);

#ifdef TL2_STATS
    unsigned i;
    for (i = 0; i < DIM(global_stats); i++) {
        if (global_stats[i] != 0) {
            printf("  %d: %d\n", i, global_stats[i]);
        }
    }
#endif

    pthread_key_delete(global_key_self);
    if ((signal(SIGSEGV, SIG_DFL) == SIG_ERR) ||
        (signal(SIGBUS,  SIG_DFL) == SIG_ERR) ||
        (signal(SIGILL,  SIG_DFL) == SIG_ERR))
    {
      perror("error signal (restoring default sig handlers)");
      exit(1);
    }
    MEMBARSTLD();
}


/* =============================================================================
 * MISC
 * =============================================================================
 */



/* =============================================================================
 * CTAsserts
 * -- Compile-time assertions -- establish critical invariants
 * -- It is always better to fail at compile-time than at run-time.
 * =============================================================================
 */
void
CTAsserts ()
{
#ifdef __LP32
    CTASSERT(sizeof(intptr_t) == sizeof(long));
    CTASSERT(sizeof(long) == 4);
#else /* !__LP32, i.e. LP64 */
    CTASSERT(sizeof(intptr_t) == sizeof(long));
    CTASSERT(sizeof(long) == 8);
#endif /* !__LP32 */
    CTASSERT((_TABSZ & (_TABSZ-1)) == 0); /* must be power of 2 */
    /* For type conversions */
    CTASSERT(sizeof(float)  == sizeof(intptr_t));
    CTASSERT(sizeof(float*) == sizeof(intptr_t));
    CTASSERT(sizeof(void*)  == sizeof(intptr_t));
}

/* =============================================================================
 *
 * Computation Log
 *
 * =============================================================================
 */

#define QLOCK_DEBUG 0

void myFree(int id, void *thing, char *what) {
  if(QLOCK_DEBUG) printf("%d free(%d) [%s]\n",id, thing, what);
  free(thing);
}


/* =============================================================================
 *
 * QLock stuff
 *
 * =============================================================================
 */

QLockKey_t* QLockKey_alloc(int patience) {
#if !defined(XBOOST_PARTIAL)
  fprintf(stderr,"can't use QLock without XBOOST_PARTIAL (need computation log)\n");
  exit(1);
#endif
  int i;
  QLockKey_t* self = (QLockKey_t*)malloc(sizeof(QLockKey_t));
  printf("fixme. this is really inefficient!!!!!!!!!!!!!!!!!!\n");
  for(i = 0; i < QUEUELOCK_TABLE_SIZE; i++) {
    QLockKeyElement_t* el = &(self->table[i & QUEUELOCK_TABLE_MASK]);
    el->Lhead = el->Ltail = (QLockNode_t*)malloc(sizeof(QLockNode_t));
    el->Lhead->locked = 0;
    el->Lhead->abandoned = 0;
    el->Lhead->pred = NULL;
    el->Hhead = el->Htail = (QLockNode_t*)malloc(sizeof(QLockNode_t));    
    el->Hhead->locked = 1;
    el->Hhead->abandoned = 0;
    el->Hhead->pred = NULL;
  }
  self->patience = patience;
  return self;
}

void QLockKey_free(QLockKey_t* self) {
  int i;
  for(i = 0; i < QUEUELOCK_TABLE_SIZE; i++) {
    QLockKeyElement_t* el = &(self->table[i & QUEUELOCK_TABLE_MASK]);
    free(el->Ltail);
    free(el->Htail);
  }
  myFree(0, self, "whole QLockKey");
}

// the lazy approach is a pain because you have to also set all of the 
// HEAD and TAIL pointers... or maybe i'm just lazy.
static void QLockKey_init_nodes(QLockKeyElement_t* el) {
}

static void print_el(QLockKeyElement_t *el, char* msg) {
  if(QLOCK_DEBUG) return;
  printf("HIGH: ");
  QLockNode_t* c;
  for(c = el->Htail; c != NULL; c = c->pred) {
    printf("(%d:l=%d,u=%d,a=%d) ",c,c->locked,c->unseen,c->abandoned);
    if(c == el->Hhead) break;
    printf("[tid=%d] ",c->owner->UniqID);
  }
  printf(" [%s]\n", msg);
  printf("LOW: ");
  for(c = el->Ltail; c != NULL; c = c->pred) {
    printf("(%d:l=%d,u=%d,a=%d) ",c,c->locked,c->unseen,c->abandoned);
    if(c == el->Lhead) break;
    printf("[tid=%d] ",c->owner->UniqID);
  }
  printf("\n");
  Thread *own = el->owner;
  if(own)
    printf("OWN: %d\n",own->UniqID);

}

/* =============================================================================
 *
 * QLock: QueueIsEmpty()
 *
 * =============================================================================
 */
static int QLockKey_isEmpty(QLockKeyElement_t* el, int high) {
  QLockNode_t* curr;
  if(QLOCK_DEBUG) printf("%d ie\n", -1);
  if(high) {
    curr = el->Htail;
    while(curr) {
      if(curr == el->Hhead) return 1;
      if(!curr->abandoned) return 0;
      curr = curr->pred;
    }
  }

  else {
    curr = el->Ltail;
    while(curr) {
      if(curr == el->Lhead) return 1;
      if(!curr->abandoned) return 0;
      curr = curr->pred;
    }
  }

  // shouldn't get here because we probably found the head first
  if(QLOCK_DEBUG) printf("%d yikes!!!!!!\n", -1);
  fflush(stdout);
  assert(0); 
}


/* =============================================================================
 *
 * QLockKey_Enqueue(): add ourselves to the high or low queue
 *
 * =============================================================================
 */
static QLockHandle_t* QLockKey_Enqueue(Thread *self, QLockKeyElement_t* el, QLockKey_t *ql, int key, int high) {
  QLockHandle_t* h = (QLockHandle_t*)malloc(sizeof(QLockHandle_t));
  assert(h != NULL);
  h->isHigh = high;
  h->lock = ql;
  h->key = key;
  h->el = el;
  h->curr = h->pred = NULL; // don't know yet

  if(self->QL_KeyList[key]) {
    self->QL_KeyList[key]++;
    return h;
  }

  // create a new qnode and a new handle
  QLockNode_t*  qnode = (QLockNode_t*)malloc(sizeof(QLockNode_t));
  assert(qnode != NULL);
  qnode->locked = 1;
  qnode->abandoned = 0;
  qnode->unseen = 0;
  qnode->owner = self;
  qnode->pred = NULL;      // don't know yet
  h->curr = qnode;

  // loop until we were able to swing the tail pointer, adding
  // our new qnode to the end of the queue
  while(1) {
    QLockNode_t* last = (high ? el->Htail : el->Ltail);
    h->pred     = last;
    qnode->pred = last;
    QLockNode_t* res;
    if(high) res = CAS(&(el->Htail),last,qnode);
    else     res = CAS(&(el->Ltail),last,qnode);
    if(res == last)
      break;
  }
  if(QLOCK_DEBUG) print_element("cas complete",el);

  return h;
}

/* =============================================================================
 *
 * QLockKey_Dequeue()
 *
 * =============================================================================
 */
static void QLockKey_Dequeue(Thread *self, QLockHandle_t *handle) {
  QLockKeyElement_t* el = handle->el;

  // HIGH PRIORITY THREAD
  if(handle->isHigh) {
    // maybe need to just yield to another high priority thread
    // TODO: with more than 1 hp thread, this is broken because
    //   another HP thread could have added itself, but then abandoned
    if(handle->curr != el->Htail) {
      if(QLOCK_DEBUG) printf("QLock: unlock HP: yeild to other HP.\n");
      el->Hhead = handle->curr;       // shrink queue (no need to cas)
      handle->curr->pred = NULL;
      handle->curr->locked = 0;       // kick off next HP
      assert(handle->pred != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle->pred,"1 handle->pred"); handle->pred = NULL;
      assert(handle != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle, "1 handle"); handle = NULL;
    }

    // otherwise, time to kick off low priority
    // (handle->curr is now the only HP qnode)
    else {
      if(QLOCK_DEBUG) printf("QLock: unlock HP: start LP queue\n");
      el->Hhead = handle->curr;     // shrink queue (no need to cas)
      el->Lhead->locked = 0;        // kick off first LP thread
      assert(handle->pred != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle->pred,"2 handle->pred"); handle->pred = NULL;
      assert(handle != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle,"2 handle"); handle = NULL;
    }

    // with only one thread, this must be true
    assert(QLockKey_isEmpty(el, 1));
  } 

  // LOW PRIORITY THREAD
  else {
    
    // maybe there are no HP threads to worry about
    if(QLockKey_isEmpty(el, 1)) {
      if(QLOCK_DEBUG) printf("QLock: unlock LP: don't worry about HP threads\n");
      el->Lhead = handle->curr;     // shrink queue
      assert(handle->curr != NULL);
      handle->curr->locked = 0;     // kick off others in LP queue
      assert(handle->pred != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle->pred,"3 handle->pred"); handle->pred = NULL;
      assert(handle != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle,"3 handle"); handle = NULL;
    }

    // otherwise, gotta yield to HP threads
    else {
      if(QLOCK_DEBUG) printf("QLock: unlock LP: send the lock to the HP queue %d\n",handle);
      if(QLOCK_DEBUG) print_element("BAD:",handle->el);
      el->Lhead = handle->curr;     // shrink queue
      el->Hhead->locked = 0;        // kick off HP queue
      if(QLOCK_DEBUG) print_element("BAD:",handle->el);
      if(QLOCK_DEBUG) print_element("BAD:",handle->el);
      assert(handle->pred != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle->pred,"4 handle->pred"); handle->pred = NULL;
      assert(handle != NULL); // CHECK: looking for somethign that was freed before rollback
      myFree(self->UniqID,handle,"4 handle"); handle = NULL;
    }
 
  }
}







/* =============================================================================
 *
 * QLockKey_Lock()
 *
 * =============================================================================
 */

QLockHandle_t*
QLockKey_Lock(Thread *self, QLockKey_t* ql, int key, int high, int fullAbort) {

  // find the queue that corresponds to this key
  QLockKeyElement_t* el = &(ql->table[key & QUEUELOCK_TABLE_MASK]);

  if(QLOCK_DEBUG) print_el(el,"prelock");
  if(QLOCK_DEBUG) printf("%d lock(%d) pre\n", self->UniqID, key);

  // before we begin, detect if we're in a special case:
  //    if no LP threads, first HP thread arrives
  //    then need to propagate the lock from LP queue
  // REVISED 2007-10-30: HP thread *always* adds self to the LP queue,
  //    just in case the LP queue is full of abandoned nodes

  QLockHandle_t* handle = NULL;
  QLockNode_t*   spin   = NULL;

  // ******** fast-path: usually, the LP queue is empty ********
  if(QLockKey_isEmpty(el, 0)) {
    QLockHandle_t* LPh = QLockKey_Enqueue(self,el,ql,key,0);
    // fake handle case (already have the key)
    if (LPh->pred == NULL) {
      if(QLOCK_DEBUG) print_el(el,"fakelockedL");
      ComputationLogQLock(self, LPh);
      if(QLOCK_DEBUG) printf("%d lock(%d) fake low\n", self->UniqID, key);
      return LPh;
    }
    handle = LPh;
    spin = LPh->pred;

  // ******** HIGH PRIORITY ********
  } else if(high) {
    QLockHandle_t* HPh = QLockKey_Enqueue(self,el,ql,key,high);
    // fake handle case (already have the key)
    if (HPh->pred == NULL) {
      if(QLOCK_DEBUG) print_el(el,"fakelocked");
      ComputationLogQLock(self, HPh);
      if(QLOCK_DEBUG) printf("%d lock(%d) fake high\n", self->UniqID, key);
      return HPh;
    }
    // add to LP queue to propagate lock
    QLockHandle_t* LPh = QLockKey_Enqueue(self,el,ql,key,   0);
    // check to see if there's a live body that jumped ahead of us in the LP queue
    int liveLP = 0;
    QLockNode_t* c = LPh->pred;
    while(1) {
      if(c == el->Lhead) break;
      if(!c->abandoned) { liveLP = 1; break; }
      c = c->pred;
      assert(c!=NULL);
    }
    if(liveLP) {
      //assert(LPh->curr->pred == LPh->pred);
      LPh->curr->abandoned = 1;
      myFree(self->UniqID,LPh,"special temp handle");
    }
    // otherwise, we got the lock so dequeue will pass to high
    else QLockKey_Dequeue(self,LPh);

    // spin on the HP handle
    handle = HPh;
    spin = HPh->pred;

  // ******** LOW  PRIORITY ********
  } else {
    QLockHandle_t* LPh = QLockKey_Enqueue(self,el,ql,key,0);
    // fake handle case (already have the key)
    if (LPh->pred == NULL) {
      if(QLOCK_DEBUG) print_el(el,"fakelockedL");
      ComputationLogQLock(self, LPh);
      if(QLOCK_DEBUG) printf("%d lock(%d) fake low\n", self->UniqID, key);
      return LPh;
    }
    handle = LPh;
    spin = LPh->pred;
  }


  assert(spin);

  // ******** SPIN *****************
  int patience = QUEUELOCK_PATIENCE;
  int need_whisper = 1;
  if(QLOCK_DEBUG) printf("%d QLock: spinning on %d (tail is %d)\n", self->UniqID,spin,(high ? el->Htail : el->Ltail));
  while(patience--) { // high

    assert(!(spin->abandoned && spin == (high?el->Hhead:el->Lhead)));

    //printf("%d QLock: spin=%d\n", self->UniqID,spin);
    // [HP,LP] CASE 1: abandoned
    if (spin->abandoned) {
      QLockNode_t* doomed = spin;
      assert(doomed->unseen == 0 && doomed > 0);
      doomed->unseen = self->UniqID + 100;
      // everything points to the next node
      spin = handle->curr->pred = handle->pred = doomed->pred;
      assert(spin != NULL);
      myFree(self->UniqID,doomed,"abandoned node");
      if(QLOCK_DEBUG) printf("%d QLock: spin: abandoned advance to %d\n", self->UniqID,spin);
    }
      
    // [HP,LP] CASE 2: success
    else if (! spin->locked) {
      // success. our pred has released it's lock
      // announce (should never fail)
      assert(el->owner == NULL);
      Thread *res = CAS(&(el->owner),NULL,self);
      assert(res == NULL);
      // log this lock, so it can be unlocked at commit/abort
      self->QL_KeyList[key] = 1;
      ComputationLogQLock(self, handle);
      if(QLOCK_DEBUG) printf("%d QLock: got lock!\n", self->UniqID);
      assert(handle->pred);
      if(QLOCK_DEBUG) print_el(el,"locked");
      if(QLOCK_DEBUG) printf("%d lock(%d)\n", self->UniqID, key);
      return handle;
    }
    
    // [LP] CASE 4: listen to whisper
    else if(!high && self->PreemptKey != -1) {
      if (QLOCK_DEBUG) printf("%d got whisper! for k=%d\n",self->UniqID, self->PreemptKey);
      // abandon this node
      handle->curr->abandoned = 1;
      assert(handle);
      myFree(self->UniqID,handle,"whispered abandon"); handle = NULL;

      //CAS(&(self->PreemptKey),self->PreemptKey,-1);
      if(fullAbort)
	TxAbort(self);
      else
	ComputationLogRollback(self, self->PreemptKey);
      assert(0) ; // should never get here
    }

    // [HP] CASE 3: whisper
    else if(need_whisper) {
      Thread *own = el->owner;
      if(own != NULL) {
	if(high || (!el->owner->isHigh && self->TxnNo < el->owner->TxnNo)) {
	  if (QLOCK_DEBUG && !high)
	    printf("%d LP whispered to %d LP for %d. own PEK=%d\n",
		   self->UniqID, own->UniqID, key,own->PreemptKey);
	  //assert(own->PreemptKey == -1);
	  int res = CAS(&(own->PreemptKey),-1,key);
	  if(res == -1 || own->PreemptKey == key) {
	    need_whisper = 0;
	    if (QLOCK_DEBUG) printf("%d whispered for %d\n",self->UniqID, key);
	  }
	  else 
	    if (QLOCK_DEBUG) printf("%d FAILED whispered for %d\n",self->UniqID, key);
	}
      }
    }

    // [HP,LP] CASE 5: keep spining
    else { }
      
  }

  // [HP,LP] CASE 6: timeout
  handle->curr->abandoned = 1;
  assert(handle);

  myFree(self->UniqID,handle,"timedout handle"); handle = NULL;
  if(QLOCK_DEBUG) printf("%d (high=%d) timed out for key %d. ha!\n",self->UniqID,high,key);
  if(QLOCK_DEBUG) print_el(el,"timeout");

  if(high) {
    if(QLOCK_DEBUG) print_el(el,"HPtimeout");
  }

  self->Timeouts++;
  return NULL;
}

/* =============================================================================
 *
 * QLockKey_Unlock()
 *
 * =============================================================================
 */

void QLockKey_Unlock(Thread* self,QLockHandle_t* handle) {
  QLockKeyElement_t* el = handle->el;

  //printf("%d unlock(%d)\n", self->UniqID, handle->key);

  if(QLOCK_DEBUG) printf("%d unlock(%d)\n",self->UniqID,handle->key);

  if(QLOCK_DEBUG) print_el(el,"preunlock");

  assert(handle->el);
  if(QLOCK_DEBUG) print_element("Unlock begin",el);
  if(QLOCK_DEBUG) print_handle(handle);

  if((--(self->QL_KeyList[handle->key])) > 0) {
    assert(handle != NULL); // CHECK: looking for somethign that was freed before rollback
    // make sure there's another one in the log
    

    myFree(self->UniqID,handle,"fake handle"); handle = NULL; // free the dummy handle
    if(QLOCK_DEBUG) printf("%d unlock( ) fake!\n", self->UniqID);
    if(QLOCK_DEBUG) print_el(el,"unlocked-fh");
    return;
  }

  // unannounce when you unlock the key for the last time (should never fail)
  assert(el->owner == self);
  Thread *res = CAS(&(el->owner),self,NULL);
  if(QLOCK_DEBUG) printf("%d unanounce on lock %d\n",self->UniqID,handle->key);
  assert(res == self);

  QLockKey_Dequeue(self,handle);

  if(QLOCK_DEBUG) print_el(el,"unlocked");
}

/* =============================================================================
 *
 * Dreadlock
 *
 * =============================================================================
 */

#define DLDEBUG 0
#define DLASSERT(p) ( (!DLDEBUG) || (p) )

typedef struct dreadLock {
  intptr_t addr;
  short    count;
} dreadLock_t;

// volatile?

int DreadLock_Lock(dreadLock_t *dl, Thread* self) {

  //if(DLDEBUG) fprintf(stderr,"lock value= %d\n",((HoldCountLock *)addr)->word);
  // your filter should always be empty (except for your own ID)
  //if(DLDEBUG) fprintf(stderr,"%d lock %d\n",self->UniqID,addr);
  //DLASSERT(set_is_empty(self));
  //printf("lock %d [digest=%d]\n", dl->addr, self->digest);

  // TIMEOUT ==========================================
  if(self->DreadlockTimeout >= 0) {
    int prev = 0;
    int to = self->DreadlockTimeout;

    // we may already own the lock
    if((void *)dl->addr == (void *)self) {
      assert(dl->count > 0);
      dl->count++;
      return;
    }

    while (1) {
      while (1) {
	if(to-- < 0) {
	  self->Timeouts++;
	  TxAbort(self);
	  assert(0);
	}
	if((void *)(dl->addr) == NULL) break;
      }
      // lock looks free, try to acquire
      if(CAS(&(dl->addr),NULL,(void *)self) == NULL) {
	dl->count = 1;
	//fprintf(stderr,"%d lock %d\n",self->UniqID,dl);
	return 1;
      }
    }
  }

  // DREADLOCKS ==========================================
  else {
    //int prev = -1;

    // we may already own the lock
    if((void *)dl->addr == (void *)self->digest &&
       (void *)dl->addr != NULL) { // digest=0 is valid for bv32
      assert(dl->count > 0);
      dl->count++;
      return;
    }

    while (1) {
      while (1) {

	if((digest_t *)dl->addr == NULL) break;

	// detect deadlock
	mb();
	if (SET_CONTAINS((digest_t *)dl->addr,self->digestID)) {
	  self->Deadlocks++;
	    //int r = dval & (1 << (self->digestID));
	    //printf("tmp=%d / %d, self=%d, r=%d, odigest=%d\n", dval, *((digest_t *)dl->addr), self->digestID, r,*odigest);
	  SET_CLEAR(self->digest);
	  TxAbort(self);
	} else {
	  // update with the latest list of who i'm waiting for
	  //if(*odigest != *(self->digest) | (1 << self->digestID))
	  SET_UNION(self->digest,self->digestID,(digest_t *)dl->addr);
	}
      }
      // lock looks free, try to acquire
      //if(!SET_EMPTY(self->digest))
      if(CAS(&(dl->addr),NULL,self->digest) == NULL) {
	dl->count = 1;
	SET_SINGLE(self->digest,self->digestID);
	return 1;
      }
      //SET_CLEAR(self->digest);
    }
  }


  assert(0);

}

void DreadLock_Unlock(dreadLock_t *dl, Thread* self) {

  //DLASSERT(set_is_empty(self));
  //fprintf(stderr,"%d unlock %d\n",self->UniqID,dl);

  if(self->DreadlockTimeout >= 0) {
    // we may just need to decrement
    if(dl->count > 1) {
      (dl->count)--;
      return;
    }

    // or just release entirely
    dl->count = 0;
    if(CAS(&(dl->addr),(void *)self,NULL) == (void *)self) {
      return;
    }




  } else {
    // we may just need to decrement
    //DLASSERT(dl->addr == self->digest);
    //DLASSERT(dl->count > 0);
    //fprintf(stderr,"%d addr: %d, count: %d\n",self->UniqID,dl->addr, dl->count);
    if(dl->count > 1) {
      (dl->count)--;
      if(DLDEBUG) fprintf(stderr,"%d DreadLock: decr %d\n",self->UniqID, dl->addr);
      return;
    }

    // or just release entirely
    dl->count = 0;
    if(CAS(&(dl->addr),self->digest,NULL) == self->digest) {
      if(DLDEBUG) fprintf(stderr,"%d DreadLock: release %d\n",self->UniqID, dl->addr);
      return;
    }
  }
  fprintf(stderr,"%d yikes: release %d.\n",self->UniqID, dl->addr);
  assert(0);
}

/* =============================================================================
 *
 * End of tl2.c
 *
 * =============================================================================
 */

int TxDeadlockRpt(Thread* Self) {
  //printf("%d detected deadlocks: %d\n",Self->UniqID, Self->Deadlocks);
  return Self->Deadlocks;
}

int TxTimeoutRpt(Thread* Self) {
  //printf("%d detected timeouts: %d\n",Self->UniqID, Self->Timeouts);
  return Self->Timeouts;
}

int TxStats(Thread* Self) {
  printf("%d TL2 timeout count: %d\n",Self->UniqID, Self->TL2Timeouts);
  printf("%d TL2 deadlock count: %d\n",Self->UniqID, Self->TL2Deadlocks);
  return Self->TL2Timeouts;
}

/* =============================================================================
 *
 * End of tl2.c
 *
 * =============================================================================
 */
