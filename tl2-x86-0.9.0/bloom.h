#ifndef __BLOOM_H__
#define __BLOOM_H__

#include <stdlib.h>

//#define DEBUG_BAD 1

typedef unsigned int (*hashfunc_t)(const int);
typedef struct {
  size_t asize;
  unsigned char *data;
  size_t nfuncs;
  hashfunc_t *funcs;
#ifdef DEBUG_BAD
  char str[1024];
#endif
} BLOOM;

typedef unsigned int BLOOMFILTER[4];

void SET_HASH(BLOOMFILTER *filter, unsigned int threadID);

unsigned int sax_hash(const int val);
unsigned int sdbm_hash(const int val);
unsigned int jenkins(const int val);

BLOOM *bloom_create(size_t size);
unsigned char * bloom_getID(BLOOM *bloom, const int val);
int bloom_destroy(BLOOM *bloom);
int bloom_add(BLOOM *bloom, const unsigned char *hashID);
int bloom_check(BLOOM *bloom, const unsigned char *hashID);
int bloom_clear(BLOOM *bloom);
int bloom_union(BLOOM *bloom, BLOOM *other);

void bloom_setone(BLOOM *bloom, const unsigned char *hashID);
void bloom_setunion(BLOOM *bloom, const unsigned char *hashID, BLOOM *other);

#endif
