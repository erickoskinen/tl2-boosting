# ==============================================================================
#
# Defines.common.mk
#
# ==============================================================================


# ==============================================================================
# Variables
# ==============================================================================

CC       := gcc
CFLAGS   += -g -Wall
CFLAGS   += -O3
CFLAGS   += -m32
CFLAGS   += -I$(LIB)
CPP      := g++
CPPFLAGS += $(CFLAGS)
LD       := g++
LDFLAGS  += -m32
LIBS     +=

# Remove these files when doing clean
OUTPUT +=

STM := ../../tl2-x86-0.9.0


# ==============================================================================
#
# End of Defines.common.mk
#
# ==============================================================================
