# ==============================================================================
#
# Defines.common.mk
#
# ==============================================================================


LIB := ../lib

CFLAGS += -I$(LIB)
CFLAGS += -DOUTPUT_TO_STDOUT
LIBS   += -lpthread

# We need to adjust gcc's defaults to fully inline the STM; defaults:
#   -finline-limit=600
#   --param inline-unit-growth=150 --param large-function-growth=200
CFLAGS += -Winline
CFLAGS += -finline-limit=6000
CFLAGS += --param inline-unit-growth=1500 --param large-function-growth=2000

PROG := priority

SRCS := \
	priority.c \
	$(LIB)/memory.c \
	$(LIB)/mt19937ar.c \
	$(LIB)/random.c \
	$(LIB)/tm.c \
	$(LIB)/thread.c \
	$(LIB)/lockKey.c \
	$(LIB)/queueLock.c \
#
OBJS := ${SRCS:.c=.o}


# ==============================================================================
#
# End of Defines.common.mk
#
# ==============================================================================
