#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include "tm.h"
#include <ucontext.h>
#include <unistd.h>
#include "lockKey.h"
#include <sys/time.h>
#include <time.h>

#define DEBUG 0

// NO   Contention: ./priority -t 10 -b 1000 -o 5 -h 0
// LOW  Contention: ./priority -t  6 -b 100  -o 5 -h 0
// HIGH Contention: ./priority -t 10 -b 10   -o 5 -h 0

int NUM_THREADS = 10;   // -t 10
int NUM_TXNS    = 10;   // -x 10 (transactions per thread)
int NUM_OPS     =  5;   // -o 30
int NUM_BINS    = 30;   // -b 10
int BIN_PER_T   =  0;   // -p 0
int DREADLOCK   =  1;   // -D 1
int TIMEOUT     =  8 * 1024 * 1024; // -T
int NO_DEADLOCK =  0;   // -S 1

int* ds;

int version;

dreadLockKey_t* dlk;
QLockKey_t* qlk;

float statMSec;
int statTxns, statOps, statDL, statTO;

void mult_matrices(int a[][3], int b[][3], int result[][3])
{
   int i, j, k;
   for(i=0; i<3; i++)
   {
	 for(j=0; j<3; j++)
	 {
	    for(k=0; k<3; k++)
	    {
		  result[i][j] = a[i][k] + b[k][j];
	    }
	 }
   }
}
static void tie_up_cpu(int howmuch) {
  int dumbstuff = howmuch; // 24 * 1024;
   int p[3][3] = {  {1, 3, -4}, {1, 1, -2}, {-1, -2, 5}  };
   int q[3][3] = {  {8, 3, 0}, {3, 10, 2}, {0, 2, 6}  };
   int r[3][3];
   while(dumbstuff--) {
     mult_matrices(p,q,r);
   }
   //sched_yield();
}

static void decr(int key) {
  assert(ds[key] > 0);
  tie_up_cpu(100); // above 100 causes huge delays
  ds[key] -= 4;
}

static void work(TM_THREAD_ARGDECL) {
  int id, i, j, txn;
  int keys[NUM_OPS];
  struct timeval start, end;
  long thrMSec = 0;
  srand((int)time(NULL));
  // start up thread stuff
  TM_THREAD_ENTER();
  id = TM_GET_THREAD_ID();
  for (i = 0; i < NUM_OPS; i++) {
      keys[i] = rand() % NUM_BINS;
  }
  if(NO_DEADLOCK) {
    int swapped;
    do {
      swapped = 0;
      for(i=0; i< NUM_OPS-1; i++)
	if(keys[i] > keys[i+1]) {
	  int t = keys[i];
	  keys[i] = keys[i+1];
	  keys[i+1] = t;
	  swapped = 1;
	}
    } while (swapped);
    //for(i=0; i< NUM_OPS; i++) printf("%d ", keys[i]); printf("\n");
  }
  if(DREADLOCK) {
    STM_USE_DREADLOCKS();
  } else {
    STM_SET_TIMEOUT(TIMEOUT);
  }

  // bookkeeping
  gettimeofday(&start,NULL);
  for(txn = 0; txn < NUM_TXNS; txn++) {

  // begin transactoin
  TM_BEGIN();

/* =============================================================================
 *
 * operations performed within a transaction
 *
 * =============================================================================
 */

#if !defined(XBOOST_PARTIAL)
# error "must use XBOOST_PARTIAL"
#elif defined(XBOOST)

    /* ========COMPLEX========== */
  void dreadlock_txn(int i) {
    if(i == NUM_OPS) return;
    int key = keys[i];
    if(DEBUG) fprintf(stdout,"%d key=%d i=%d\n",id,key,i);
    dreadLockKey_lock(TM_ARG dlk,key);

    if(DEBUG) fprintf(stdout,"%d operation start\n", id);
    ds[key] = ds[key] + 4;
    STM_LOG_INVERSE(decr, 1, key);
    if(DEBUG) fprintf(stdout,"%d operation done\n", id);
    dreadlock_txn(i+1);
  }

  dreadlock_txn(0);

#else
  fprintf(stderr,"error:partial w/o xbpc\n");
  exit(1);
#endif


  // do something CPU bound
  tie_up_cpu(10);

  TM_END();
  //fprintf(stdout,"%d txn %d done\n", id, txn);

  statTxns++;
  statOps += NUM_OPS;


  }

  gettimeofday(&end,NULL);
  float b = (start.tv_sec * 1000) + ((float)start.tv_usec / 1000);
  float e = (end.tv_sec * 1000) + ((float)end.tv_usec / 1000);
  assert(e > b);
  statMSec += (e - b);

  //fprintf(stderr,"%d all %d txns done\n", id, NUM_TXNS);
  statDL += STM_REPORT_DEADLOCK();
  statTO += STM_REPORT_TIMEOUTS();

  TM_THREAD_EXIT();
}

/* =============================================================================
 *
 * microbench
 *
 * =============================================================================
 */

static void microbench(TM_THREAD_ARGDECL) {
  int i;
  struct timeval start, end;
  int diffms;
  // start up thread stuff
  TM_THREAD_ENTER();

  gettimeofday(&start,NULL);
  for(i=0;i<10000;i++) {
    TM_BEGIN();
    QLockHandle_t* handle = STM_QLOCK_LOCK(qlk,4,0,0);
    assert(handle != NULL);
    TM_END();
  }
  gettimeofday(&end,NULL);

  diffms += (end.tv_sec - start.tv_sec) * 1000;
  diffms += (end.tv_usec - start.tv_usec) / 1000;
  printf("QLOCK: %d ms\n",diffms);


  gettimeofday(&start,NULL);
  for(i=0;i<10000;i++) {
    TM_BEGIN();
    dreadLockKey_lock(TM_ARG dlk,4);
    TM_END();
  }
  gettimeofday(&end,NULL);

  diffms += (end.tv_sec - start.tv_sec) * 1000;
  diffms += (end.tv_usec - start.tv_usec) / 1000;
  printf("DLOCK: %d ms\n",diffms);

  TM_THREAD_EXIT();
}

/* =============================================================================
 *
 * main
 *
 * =============================================================================
 */

MAIN(argc,argv)
{
  GOTO_REAL();
  int c,i, MICROBENCH = 0;

  opterr = 0;
  while ((c = getopt (argc, argv, "t:o:b:x:p:D:M:T:S:")) != -1) {
    switch (c) {
    case 't':
      NUM_THREADS = atoi(optarg);
      break;
    case 'o':
      NUM_OPS = atoi(optarg);
      break;
    case 'b':
      NUM_BINS = atoi(optarg);
      break;
    case 'x':
      NUM_TXNS = atoi(optarg);
      break;
    case 'p':
      BIN_PER_T = atoi(optarg);
      break;
    case 'D':
      DREADLOCK = atoi(optarg);
      break;
    case 'T':
      TIMEOUT = atoi(optarg);
      break;
    case 'M':
      MICROBENCH = atoi(optarg);
      break;
    case 'S':
      NO_DEADLOCK = atoi(optarg);
      break;
    case '?':
      fprintf (stderr, "Option -%c requires an argument.\n", optopt);
      exit(1);
    }
  }

  statMSec = statTxns = statOps = statDL = statTO = 0;

  if(MICROBENCH) {
    dlk = dreadLockKey_alloc();
    qlk = STM_QLOCK_ALLOC(TIMEOUT);
    TM_STARTUP();
    TM_PARALLEL(microbench, NULL, 1);
    TM_SHUTDOWN();
    dreadLockKey_free(dlk);
    STM_QLOCK_FREE(qlk);
    return 0;
  }


  dlk = dreadLockKey_alloc();

  printf("begin [threads=%d,bins=%d,ops=%d,txns=%d,dreadlock=%d]\n",
	 NUM_THREADS, NUM_BINS, NUM_OPS, NUM_TXNS, DREADLOCK);
  ds = malloc(sizeof(int)*NUM_BINS);
  for(i=0;i<NUM_BINS;i++)
    ds[i] = 0;

  TM_STARTUP();
  TM_PARALLEL(work, NULL, NUM_THREADS);
  TM_SHUTDOWN();

  printf("time      : %f\n", statMSec); //  ms (average time to run a transaction)
  printf("txns      : %d\n", statTxns);
  printf("ops       : %d\n", statOps);
  printf("deadlocks : %d\n", statDL);
  printf("timeouts  : %d\n", statTO);

  printf("end [threads=%d,bins=%d,ops=%d,txns=%d,dreadlock=%d]\n",
	 NUM_THREADS, NUM_BINS, NUM_OPS, NUM_TXNS, DREADLOCK);
  printf("./priority -t %d -b %d -o %d -x %d -D %d\n",
	 NUM_THREADS, NUM_BINS, NUM_OPS, NUM_TXNS, DREADLOCK);

  dreadLockKey_free(dlk);

  return 0;
}
