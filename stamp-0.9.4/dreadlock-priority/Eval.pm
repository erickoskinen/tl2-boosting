
package Eval;

sub parse_dread {
  my ($output) = @_;
  my $out;
  $out->{all} = $output;
  my @L = (grep / : /, split /\n/, $output);
  for my $l (@L) {
    if($l =~ m/^([a-z]+) * : ([\d\.]+)$/) {
      #print "$1 = $2\n";
      $out->{$1} = $2;
    }
    else { warn qq/unable to parse $l\n/; }
  }
  $out->{time} /= 1000 if defined $out->{time};
  print "time: $output\n" unless $output =~ /ops *:/m;
  return $out;
}

#time      : 1 ms (average time to run a transaction)
#txns      : 700
#ops       : 7000
#deadlocks : 20927
#timeouts  : 0

sub parse_line {
  my ($l) = @_;
  #printf("x|priority|bins|threads|txns|t_us|t_s|ops\n");
  my (undef,$p,$bins,$threads,$txns,$us,$s,$os) = split /\|/, $l;

  my $ms = ($s * 1000) + ($us / 1000);

  my $rate = ($txns == 0 ? 0 : $txns / $ms);

  return {
	  priority => $p,
	  bins     => $bins,
	  threads  => $threads,
	  txns     => $txns,
	  ms       => $ms,
	  ops      => $os,
	  rate     => $rate };
}

1
