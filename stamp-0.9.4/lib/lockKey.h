/* =============================================================================
 *
 * lockKey.h
 * -- associate 2-phase lock with each key
 *
 * =============================================================================
 */

#include <pthread.h>
#include <string.h>
#include "hashtable.h"

#ifndef LOCKKEY_H
#define LOCKKEY_H 1

#define LOCKKEY_TABLE_SIZE (8 * 1024)
#define LOCKKEY_TABLE_MASK (LOCKKEY_TABLE_SIZE - 1)

 typedef struct lockKey {
   intptr_t table[LOCKKEY_TABLE_SIZE];
 } lockKey_t;

lockKey_t*
lockKey_alloc();

void
lockKey_free(lockKey_t*);

void
lockKey_lock(TM_ARGDECL lockKey_t*, int);

intptr_t*
lockKey_get(TM_ARGDECL lockKey_t*, int);

/* =============================================================================
 *
 * hold count locks
 *
 * =============================================================================
 */

#ifdef XBOOST_CHECKPOINTS

#define HCLOCK_TIMEOUT(x)   (x == -1)  // timeout while trying to acquire
#define HCLOCK_ACQUIRED(x)  (x == 1)   // we acquired the lock for the first time
#define HCLOCK_REACQUIRED(x) (x > 1)    // we already had the lock

lockKey_t*
hclockKey_alloc();

void
hclockKey_free(lockKey_t*);

int
hclockKey_lockIncr(TM_ARGDECL lockKey_t*, int);

void
hclockKey_lockDecr(TM_ARGDECL lockKey_t*, int);

#endif /* XBOOST_CHECKPOINTS */

/* =============================================================================
 *
 * deadlock-detection locks
 *
 * =============================================================================
 */

#ifdef XBOOST_CHECKPOINTS

typedef struct dreadLock {
  intptr_t addr;
  short    count;
} dreadLock_t;


typedef struct dreadLockKey {
  dreadLock_t table[LOCKKEY_TABLE_SIZE];
} dreadLockKey_t;

dreadLockKey_t*
dreadLockKey_alloc();

void
dreadLockKey_free(dreadLockKey_t*);

void
dreadLockKey_lock(TM_ARGDECL dreadLockKey_t*, int);

#endif

/* ============================================================================= */

#endif /* LOCKKEY_H */
