/* =============================================================================
 *
 * tm.h
 * -- Utility defines for transactional memory
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Authors: Chi Cao Minh and Martin Trautmann
 *
 * =============================================================================
 *
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#ifndef TM_H
#define TM_H 1


#include <stdlib.h> /* malloc */
#include "memory.h"


/* =============================================================================
 * MACRO interface
 * =============================================================================
 *
 * MAIN(argc, argv) -- resolves to function declaration without semicolon
 *     used to declare the main function with argc being the identifier
 *     for the argument count and argv being the name for argument
 *     string list (usually declared by MAIN: int argc, char **argv)
 *
 * MAIN_RETURN(int_val); -- resolves to statement list
 *     returns from MAIN function
 *
 * GOTO_SIM() -- resolves to statement list
 *     switch simulator in simulation mode (contributes to statistics);
 *     attention: simulators should enter MAIN in simulation mode and
 *     allow returning in simulation mode
 *
 * GOTO_REAL() -- resolves to statement list
 *     switch simulator in real mode (faster)
 *     attention: use in sequential region only, some TM-calls are not
 *     allowed between GOTO_REAL() and the next GOTO_SIM() like
 *     TM_GET_NUM_CPU and TM_PRINT* (use printf/cout instead)
 *
 * IS_IN_SIM() -- resolves to int-expression
 *     returns whether simulation mode is turned on (always 0 if no
 *     simulator used)
 *
 * SIM_GET_NUM_CPU() -- resolves to int-expression
 *     returns number of cpus in the simulated system
 *
 * TM_PRINTF(format_string, ...) -- resolves to statement
 *     printf that can be redirected according to simulator purposes
 *     (e.g. low contribution to simulated application statistics)
 *     attention: call in simulation mode only, if runs on simulator
 *
 * TM_PRINT0(string) -- resolves to statement
 *     printf with fixed length arguments
 *     attention: call in simulation mode only, if runs on simulator
 *
 * TM_PRINT1(format_string, int_var) -- resolves to statement
 *     printf with fixed length arguments (single integer argument %d/%x)
 *     attention: call in simulation mode only, if runs on simulator
 *
 * TM_PRINT2(format_string, int_var, int_var) -- resolves to statement
 *     printf with fixed length arguments (two integer arguments %d/%x)
 *     attention: call in simulation mode only, if runs on simulator
 *
 * TM_PRINT3(format_string,int_var,int_var,int_var) -- resolves to statement
 *     printf with fixed length arguments (three integer arguments %d/%x)
 *     attention: call in simulation mode only, if runs on simulator
 *
 * TM_STARTUP() -- resolves to statement list
 *     has to be called before any other TM-call in sequential region
 *
 * TM_SHUTDOWN() -- resolves to statement list
 *     has to be called as last TM-call in sequential region
 *     (TM_PRINT-calls are still allowed afterwards)
 *
 * TM_PARALLEL(function,arg_ptr,numThread) -- resolves to statement list
 *     start a parallel region with <numThread> threads each
 *     executing a given function; <function> is a function pointer
 *     with the type void(*function)(TM_THREAD_ARGDECL); <arg_ptr>
 *     is a void* pointer that will be accessible by all threads;
 *
 * TM_THREAD_ENTER() -- resolves to declaration statement list
 *     has to be declared in the first declaration region of the
 *     parallel region of every thread
 *     attention: don't call any other TM function in parallel region
 *     before this
 *
 * TM_THREAD_EXIT() -- resolves to statement list
 *     has to be called as last TM-function in parallel region of
 *     every thread
 *
 *
 * ----------------------------------------------------------
 * The following macros are only allowed in parallel regions:
 * ----------------------------------------------------------
 *
 * TM_BEGIN() -- resolves to statement list
 *     begin atomic block / transaction
 *     attention: every begin has to be mached by a TM_END() in the
 *     same statement block
 *
 * TM_BEGIN_RO() -- resolves to statement list
 *     begin atomic block / transaction that only reads shared data
 *
 * TM_END() -- resolves to statement list
 *     end atomic block / transaction
 *
 * TM_GET_THREAD_ID() -- resolves to int-expression
 *     returns thread id
 *     attention: call from parallel region only
 *
 * TM_GET_NUM_THREAD() -- resolves to int-expression
 *     returns number of threads
 *     attention: call from parallel region only
 *
 * TM_BARRIER() -- resolves to statement list
 *     synchronize all threads in a barrier manner
 *
 * TM_HINT_MEMORY_PER_THREAD(size) -- resolves to statement list
 *     gives a hint about how much memory might be needed per thread;
 *     a thread local allocator might reserve that amount of memory per
 *     thread before entering the parallel region
 *
 * TM_MALLOC(size) -- resolves to (void*)-expression
 *     allocate memory like with malloc; implementations can easily
 *     use a thread local allocator since for example
 *     TM_GET_THREAD_ID() provides a way to distinguish the threads
 *     (in sequential region no TM_MALLOC is allowed)
 *
 * TM_FREE(ptr) -- resolves to statement-list
 *     frees memory at ptr if ptr was returned by a previous TM_MALLOC
 *     call; This function does not require TM_ARG-macros and thus
 *     isn't allowed to call other TM-functions; All information required
 *     to perform TM_FREE needs to be stored by TM_MALLOC at a place
 *     where TM_FREE can find it with the pointer value;
 *     (user has to keep track which memory was allocated by TM_MALLOC
 *     and which by another allocator)
 */

/* ----------------------------------------------------------------
 * Mixed memory allocation between sequential and parallel section:
 *   since this way of allocation requires additional overhead anyway
 *   it should be preferred over TM_MALLOC/TM_FREE if it is important
 *   that the free call actually frees memory for mixed allocation
 *   between different threads
 * ----------------------------------------------------------------
 * TM_MIXED_MALLOC_SEQ(size) -- resolves to (void*)-expression
 *     allocate memory in sequential section
 *
 * TM_MIXED_MALLOC_PAR(size) -- resolves to (void*)-expression
 *     allocate memory in parallel section
 *
 * TM_MIXED_FREE(ptr)
 *     frees memory at ptr appropriately no matter whether it is seq
 *     or par
 *
 */

/* ---------------------------------------------------------------
 * The following macros are explained together with some examples:
 * ---------------------------------------------------------------
 *
 * TM_ARGDECL            can be prepended to parameter declarations
 * TM_ARGDECL_ALONE     resolves to a parameter declaration list
 * TM_ARGDECL_END       can be appended to parameter declarations (use only of necessary)
 * TM_ARG               can be prepended when passing parmeters
 * TM_ARG_ALONE         resolves to a complete parameter list
 * TM_ARG_END           can be appended to parameter list (use only of necessary)
 * TM_NO_ARG            can be prepended when passing parmeters
 * TM_NO_ARG_ALONE      resolves to a complete parameter list
 * TM_NO_ARG_END        can be appended to parameter list (use only of necessary)
 * TM_CONSTRUCTOR_ARG   resolves to parameter list including brackets or nothing
 * TM_THREAD_ARGDECL    resolves to a parameter declaration list
 * TM_MEMBERDECL        resolves to a class/struct member declaration (C++)
 * TM_MEMBER_INIT()     resolves to a statement list (C++)
 * TM_MEMBER_DISABLE()  resolves to a statement list (C++)
 *
 * These macros are used to support the TM-function calls above with
 * thread context information; They can be implemented to create zero
 * overhead to applications; But they ease portability;
 * Their usage is explained by an example of their correct usage:
 *
 *     // some function declarations each using a ARGDECL macro
 *     void subfunction1(TM_ARGDECL_ALONE);
 *     int subfunction2(TM_ARGDECL  int a, int b, int c);
 *     void parallel_region(TM_THREAD_ARGDECL);
 *
 *     // lets create a small parallel application:
 *     MAIN(argc,argv)
 *     {
 *       void* custom_arg = ... any data structure or just NULL ...;
 *       TM_STARTUP();
 *       TM_PARALLEL(parallel_region,custom_arg,8); // creates 8 threads
 *       TM_SHUTDOWN();
 *     }
 *     void parallel_region(TM_THREAD_ARGDECL)
 *     {
 *       // this is a declation that needs to be before any TM-call:
 *       TM_THREAD_ENTER();
 *       // now we can use TM-calls in follow-up declarations
 *       void* custom_arg = TM_GET_ARG();
 *       // here the approriate arguments are used to match the
 *       // ARGDECL declarations:
 *       subfunction1(TM_ARG_ALONE);
 *       subfunction2(TM_ARG 1,2,3);
 *       TM_THREAD_EXIT();
 *     }
 *     void subfunction1(TM_ARGDECL_ALONE)
 *     {
 *       void* custom_arg = TM_GET_ARG();
 *       int id = TM_GET_THREAD_ID();
 *       int n = TM_GET_NUM_THREAD();
 *       // thread local allocation:
 *       int *array = (int*) TM_MALLOC(sizeof(int)*10);
 *       ... do something ...
 *       TM_BARRIER();
 *       ... do something else ...
 *     }
 *     int subfunction2(TM_ARGDECL  int a, int b, int c)
 *     {
 *       void* custom_arg = TM_GET_ARG();
 *       int id = TM_GET_THREAD_ID();
 *       int n = TM_GET_NUM_THREAD();
 *       // thread local allocation:
 *       int *array = (int*) TM_MALLOC(sizeof(int)*10);
 *       ... do something ...
 *       TM_BARRIER();
 *       ... do something else ...
 *     }
 *
 * Here some C++ example code:
 *     class A : public TM_Allocatable {
 *       A();
 *       A( int i );
 *       ... some declarations ...
 *     };
 *
 *     class C {
 *     public:
 *       // constructors
 *       C() { TM_MEMBER_DISABLE(); }
 *       C(TM_ARGDECL_ALONE)  { TM_MEMBER_INIT(); }
 *       C(TM_ARGDECL  int _a) { TM_MEMBER_INIT(); a=_a; }
 *       // initialize if not done before
 *       void init(TM_ARGDECL_ALONE)   { TM_MEMBER_INIT(); }
 *       void init(TM_ARGDECL  int _a)  { TM_MEMBER_INIT(); a=_a; }
 *       void fcn() { int id = TM_GET_THREAD_ID(); }
 *     private:
 *       TM_MEMBERDECL;
 *       int a;
 *     };
 *
 *     void parallel_region( TM_THREAD_ARGDECL )
 *     {
 *       TM_THREAD_ENTER();
 *       A *parallel_a = new(TM_ARG true) A;
 *       C c1 TM_CONSTRUCTOR_ARG; // arg may be nothing or (TM_ARG_ALONE)
 *       C c2(TM_ARG 1);        // easier if constructor has additional argument
 *       TM_THREAD_EXIT();
 *     }
 *
 *     MAIN(argc,argv)
 *     {
 *       A *sequential_a = new(TM_NO_ARG false) A(5);
 *       TM_STARTUP();
 *       TM_PARALLEL(parallel_region,0,8); // creates 8 threads
 *       TM_SHUTDOWN();
 *     }
 *
 * some additional macros are defined in tm_annotate.h which can be
 * used to explicitely mark read- or write-accesses to shared
 * locations which need protection by a transactional memory
 * implementation
 */

/* In some cases inline functions are used instead of macros */
#define __TM_INLINE__  static __inline__
extern int _tm_initial_memory_per_thread;

#ifndef TM_CACHE_LINE_SIZE
#  define TM_CACHE_LINE_SIZE  (64)
#endif


/* =============================================================================
 * Simulator specific
 * =============================================================================
 */
#ifdef SIMULATOR

#  include <simapi.h>

#  define MAIN(argc, argv)        void mainX (int argc, const char** argv, const char** envp)
#  define MAIN_RETURN(val)        return /* value is ignored */
#  define GOTO_SIM()              goto_sim()
#  define GOTO_REAL()             goto_real()
#  define IS_IN_SIM()             (inSimulation)
#  define SIM_GET_NUM_CPU(var) ({ \
    if (!IS_IN_SIM()) { \
        GOTO_SIM(); \
        var = Sim_GetNumCpus(); \
        GOTO_REAL(); \
    } else { \
        var = Sim_GetNumCpus(); \
    } \
    var; \
})

#  define TM_PRINTF  Sim_Print
#  define TM_PRINT0  Sim_Print0
#  define TM_PRINT1  Sim_Print1
#  define TM_PRINT2  Sim_Print2
#  define TM_PRINT3  Sim_Print3

#else

#  include <stdio.h>

#  define MAIN(argc,argv)         int main (int argc, char** argv)
#  define MAIN_RETURN(val)        return val
#  define GOTO_SIM()              /* nothing */
#  define GOTO_REAL()             /* nothing */
#  define IS_IN_SIM()             (0)
#  define SIM_GET_NUM_CPU(var) /* nothing */

#  define TM_PRINTF   printf
#  define TM_PRINT0   printf
#  define TM_PRINT1   printf
#  define TM_PRINT2   printf
#  define TM_PRINT3   printf

#endif /* SIMULATOR */


/* =============================================================================
 * HTM - Hardware Transactional Memory
 * =============================================================================
 */

#ifdef HTM

#  ifndef SIMULATOR
#    error HTM requries SIMULATOR
#   endif

#  include <tmapi.h>
#  include "thread.h"
#  include "memory.h"
extern THREAD_BARRIER_T _tm_global_barrier;
extern int _tm_initial_memory_per_thread;

#  define TM_ARGDECL           /* nothing */
#  define TM_ARGDECL_ALONE     /* nothing */
#  define TM_ARGDECL_END       /* nothing */
#  define TM_ARG               /* nothing */
#  define TM_ARG_ALONE         /* nothing */
#  define TM_ARG_END           /* nothing */
#  define TM_NO_ARG            /* nothing */
#  define TM_NO_ARG_ALONE      /* nothing */
#  define TM_NO_ARG_END        /* nothing */
#  define TM_CONSTRUCTOR_ARG   /* nothing */
#  define TM_THREAD_ARGDECL    void* _tm_thread_arg
#  define TM_MEMBERDECL        /* nothing */
#  define TM_MEMBER_INIT()     /* nothing */
#  define TM_MEMBER_DISABLE()  /* nothing */

#  define TM_STARTUP()         /* nothing */
#  define TM_SHUTDOWN()        /* nothing */

/* store size in variable within memory.c */
#  define TM_HINT_MEMORY_PER_THREAD(size)  _tm_initial_memory_per_thread=size

#  define TM_PARALLEL(function,arg_ptr, numThread) \
     _tm_parallel(function, (void*)arg_ptr, numThread)

#  define TM_THREAD_ENTER()    /* nothing */
#  define TM_THREAD_EXIT()     /* nothing */

#  define TM_BEGIN()           TM_BeginClosed()
#  define TM_BEGIN_RO()        TM_BeginClosed()
#  define TM_END()             TM_EndClosed()

#  define TM_GET_ARG()         _tm_thread_arg
#  define TM_GET_THREAD_ID()   Sim_GetMyId()
#  define TM_GET_NUM_THREAD()  Sim_GetNumCpus()

#  define TM_BARRIER() \
     THREAD_BARRIER(&_tm_global_barrier, TM_GET_THREAD_ID());

#  define TM_MALLOC(size)            memory_get(TM_GET_THREAD_ID(), size)
#  define TM_FREE(ptr)               /* TODO: thread local free is non-trivial */
#  define TM_MIXED_MALLOC_SEQ(size)  _tm_mixed_malloc_seq(size)
#  define TM_MIXED_MALLOC_PAR(size)  _tm_mixed_malloc_par(TM_ARG size)
#  define TM_MIXED_FREE_SEQ(ptr)     _tm_mixed_free_seq(ptr)
#  define TM_MIXED_FREE_PAR(ptr)     _tm_mixed_free_par(TM_ARG ptr)

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL), void* arg, int numThread);

__TM_INLINE__ void* _tm_mixed_malloc_seq (unsigned int size);
__TM_INLINE__ void* _tm_mixed_malloc_par (TM_ARGDECL  unsigned int size);
__TM_INLINE__ void _tm_mixed_free_seq (void* ptr);
__TM_INLINE__ void _tm_mixed_free_par (TM_ARGDECL  void* ptr);

/* =============================================================================
 * STM - Software Transactional Memory
 * =============================================================================
 */

#elif defined(STM)

#  include "thread.h"
#  include "memory.h"
#  include "stm.h"     /* macro-based STM interface */

typedef struct {
  STM_THREAD_T stm;
  int threadId;
  int numThread;
  void* arg;
  THREAD_BARRIER_T *barrier;
  char dummy[TM_CACHE_LINE_SIZE]; /* avoid false sharing */
} _tm_thread_context_t;

#  define TM_ARGDECL           _tm_thread_context_t *_tm_thread_context,
#  define TM_ARGDECL_ALONE     _tm_thread_context_t *_tm_thread_context
#  define TM_ARGDECL_END       ,_tm_thread_context_t *_tm_thread_context
#  define TM_ARG               _tm_thread_context,
#  define TM_ARG_ALONE         _tm_thread_context
#  define TM_ARG_END           ,_tm_thread_context
#  define TM_NO_ARG            (0),
#  define TM_NO_ARG_ALONE      (0)
#  define TM_NO_ARG_END        ,(0)
#  define TM_CONSTRUCTOR_ARG   (TM_ARG_ALONE)
#  define TM_THREAD_ARGDECL    void* _tm_thread_arg
#  define TM_MEMBERDECL        _tm_thread_context_t *_tm_thread_context
#  define TM_MEMBER_INIT()     this->_tm_thread_context = _tm_thread_context
#  define TM_MEMBER_DISABLE()  this->_tm_thread_context = 0

#  define TM_STARTUP()         STM_STARTUP()
#  define TM_SHUTDOWN()        STM_SHUTDOWN()

/* Store size in variable within memory.c */
#  define TM_HINT_MEMORY_PER_THREAD(size) _tm_initial_memory_per_thread=size

#  define TM_PARALLEL(function,arg_ptr, numThread) \
     _tm_parallel(function, (void*)arg_ptr, numThread)

#  define TM_THREAD_ENTER() \
     _tm_thread_context_t *_tm_thread_context =  _tm_init_thread(_tm_thread_arg)
#  define TM_THREAD_EXIT()     /* nothing: consider deallocation */

#  define TM_BEGIN()           STM_BEGIN_WR()
#  define TM_BEGIN_RO()        STM_BEGIN_RD()
#  define TM_END()             STM_END()

#  define TM_GET_ARG()         (((_tm_thread_context_t*)_tm_thread_arg)->arg)
#  define TM_GET_THREAD_ID()   (_tm_thread_context->threadId)
#  define TM_GET_NUM_THREAD()  (_tm_thread_context->numThread)

#  define TM_BARRIER() \
     THREAD_BARRIER(_tm_thread_context->barrier, TM_GET_THREAD_ID())

#  define TM_MALLOC(size)           memory_get(TM_GET_THREAD_ID(), size)
#  define TM_FREE(ptr)              /* TODO: thread local free is non-trivial */
#  define TM_MIXED_MALLOC_SEQ(size) _tm_mixed_malloc_seq(size)
#  define TM_MIXED_MALLOC_PAR(size) _tm_mixed_malloc_par(TM_ARG size)
#  define TM_MIXED_FREE_SEQ(ptr)    _tm_mixed_free_seq(ptr)
#  define TM_MIXED_FREE_PAR(ptr)    _tm_mixed_free_par(TM_ARG ptr)

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL), void* arg, int numThread);

__TM_INLINE__ _tm_thread_context_t*
_tm_init_thread (void* _tm_thread_arg);

__TM_INLINE__ void* _tm_mixed_malloc_seq (unsigned int size);
__TM_INLINE__ void* _tm_mixed_malloc_par (TM_ARGDECL   unsigned int size);
__TM_INLINE__ void _tm_mixed_free_seq (void* ptr);
__TM_INLINE__ void _tm_mixed_free_par (TM_ARGDECL   void* ptr);

/* =============================================================================
 * Sequential execution
 * =============================================================================
 */

#else /* SEQUENTIAL */

#  include "memory.h"

#  define TM_ARGDECL           /* nothing */
#  define TM_ARGDECL_ALONE     /* nothing */
#  define TM_ARGDECL_END       /* nothing */
#  define TM_ARG               /* nothing */
#  define TM_ARG_ALONE         /* nothing */
#  define TM_ARG_END           /* nothing */
#  define TM_NO_ARG            /* nothing */
#  define TM_NO_ARG_ALONE      /* nothing */
#  define TM_NO_ARG_END        /* nothing */
#  define TM_CONSTRUCTOR_ARG   /* nothing */
#  define TM_THREAD_ARGDECL    void* _tm_thread_arg
#  define TM_MEMBERDECL        /* nothing */
#  define TM_MEMBER_INIT()     /* nothing */
#  define TM_MEMBER_DISABLE()  /* nothing */

#  define TM_STARTUP()         /* nothing */
#  define TM_SHUTDOWN()        /* nothing */

/* store size in variable within memory.c */
#  define TM_HINT_MEMORY_PER_THREAD(size) _tm_initial_memory_per_thread=size

#  define TM_PARALLEL(function, arg_ptr, numThread) \
     _tm_parallel(function, (void*)arg_ptr, numThread)

#  define TM_THREAD_ENTER()    /* nothing */
#  define TM_THREAD_EXIT()     /* nothing */

#  define TM_BEGIN()           /* nothing */
#  define TM_BEGIN_RO()        /* nothing */
#  define TM_END()             /* nothing */

#  define TM_GET_ARG()         (_tm_thread_arg)
#  define TM_GET_THREAD_ID()   (0)
#  define TM_GET_NUM_THREAD()  (1)

#  define TM_BARRIER()         /* nothing */

#  define TM_MALLOC(size)            memory_get(TM_GET_THREAD_ID(), size)
#  define TM_FREE(ptr)               /* thread local free is non-trivial */
#  define TM_MIXED_MALLOC_SEQ(size)  _tm_mixed_malloc_seq(size)
#  define TM_MIXED_MALLOC_PAR(size)  _tm_mixed_malloc_par(TM_ARG size)
#  define TM_MIXED_FREE_SEQ(ptr)     _tm_mixed_free_seq(ptr)
#  define TM_MIXED_FREE_PAR(ptr)     _tm_mixed_free_par(TM_ARG ptr)

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL), void* arg, int numThread);

__TM_INLINE__ void* _tm_mixed_malloc_seq(unsigned int size);
__TM_INLINE__ void* _tm_mixed_malloc_par(TM_ARGDECL  unsigned int size);
__TM_INLINE__ void _tm_mixed_free_seq(void* ptr);
__TM_INLINE__ void _tm_mixed_free_par(TM_ARGDECL  void* ptr);

#endif /* SEQUENTIAL */


/* =============================================================================
 * C++ addon
 * =============================================================================
 */

#ifdef __cplusplus

class TM_Allocatable
{
public:
  inline void* operator new (unsigned int size) throw() {
    int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
    bool parallel_region = false;
#  ifdef TM_ALLOCATABLE_NO_PADDING
    mem = (int*)malloc(size + sizeof(int));
#  else
    mem = (int*)malloc(size + sizeof(int) + TM_CACHE_LINE_SIZE);
#  endif
    if (!mem) {
      return 0;
    }
    mem[0] = (parallel_region ? 1:0);
    return (void*)(mem+1);
  }
  inline void* operator new (unsigned int size, TM_ARGDECL  bool parallel_region) throw() {
    int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
#  ifdef TM_ALLOCATABLE_NO_PADDING
    if (parallel_region) {
      mem = (int*)TM_MALLOC(size + sizeof(int)); /* alignment guarantees??? */
    } else {
      mem = (int*)malloc(size + sizeof(int));
    }
#  else
    if (parallel_region) {
      mem = (int*)TM_MALLOC(size + sizeof(int) + TM_CACHE_LINE_SIZE); /* alignment guarantees??? */
    } else {
      mem = (int*)malloc(size + sizeof(int) + TM_CACHE_LINE_SIZE);
    }
#  endif
    if (!mem) {
      return 0;
    }
    mem[0] = (parallel_region ? 1:0);
    return (void*)(mem+1);
  }
  inline void operator delete (void* ptr) {
    int *mem = ((int*)ptr) - 1;
    if (mem[0]) {
      TM_FREE(mem);
    } else {
      free(mem);
    }
  }
};

#endif /* __cplusplus */


/* =============================================================================
 * Inline function implementations (primarily for STM)
 * =============================================================================
 */

#ifdef HTM

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL),
              void* arg_ptr,
              int numThread)
{
  if (IS_IN_SIM()) {
    GOTO_REAL(); /* exclude initialization from statistics */
  }
  memory_init(numThread, _tm_initial_memory_per_thread, 2);
  THREAD_BARRIER_INIT(&_tm_global_barrier,numThread);
  GOTO_SIM();
  Sim_ParallelFork((void* (*)(void*))(parallel_region), (void*)(arg_ptr));
  THREAD_BARRIER_DESTROY(&_tm_global_barrier);
}

__TM_INLINE__ void*
_tm_mixed_malloc_seq (unsigned int size)
{
  int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (int*)malloc(size + sizeof(int));
  if (!mem) {
    return 0;
  }
  mem[0] = -1; /* threadId==-1 indicates sequential malloc */
  return (void*)(mem+1);
}

__TM_INLINE__ void*
_tm_mixed_malloc_par (TM_ARGDECL  unsigned int size)
{
  int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (int*)memory_get(TM_GET_THREAD_ID(), size + sizeof(int));
  if (!mem) {
    return 0;
  }
  mem[0] = TM_GET_THREAD_ID(); /* store threadId */
  return (void*)(mem+1);
}

__TM_INLINE__ void
_tm_mixed_free_seq (void* ptr)
{
  int *mem = ((int*)ptr) - 1;
  int threadId = mem[0];
  if (threadId == -1) {
    free(mem);
  } else {
    /*
     * TODO: do correct free call allocater's thread id is threadId
     */
  }
}

__TM_INLINE__ void
_tm_mixed_free_par (TM_ARGDECL  void* ptr)
{
  int *mem = ((int*)ptr) - 1;
  int threadId = mem[0];
  if (threadId == -1) {
#ifndef SIMULATOR
    free(mem);
#endif
  } else {
    /*
     * TODO: do correct free call
     * allocater's thread id is threadId
     * deallocater's thread id is TM_GET_THREAD_ID()
     */
  }
}

#elif defined(STM)

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL),
              void* arg_ptr,
              int numThread)
{
  THREAD_T* threads;
  THREAD_ATTR_T threadAttr;
  THREAD_BARRIER_T* barrier_ptr;
  int i;
  if (IS_IN_SIM()) {
    GOTO_REAL(); /* exclude initialization from statistics */
  }
  barrier_ptr = THREAD_BARRIER_ALLOC(numThread);
  THREAD_BARRIER_INIT(barrier_ptr,numThread);
  assert(barrier_ptr);
  threads = (THREAD_T*)malloc(numThread * sizeof(THREAD_T));
  assert(threads);
  _tm_thread_context_t *thread_args =
    (_tm_thread_context_t*)malloc(numThread * sizeof(_tm_thread_context_t));
  for (i = 0; i < numThread; i++) {
    thread_args[i].arg = arg_ptr;
    thread_args[i].threadId = i;
    thread_args[i].numThread = numThread;
    thread_args[i].barrier = barrier_ptr;
    STM_INIT_THREAD(&thread_args[i].stm, i);
  }
  memory_init(numThread, _tm_initial_memory_per_thread, 2);
  THREAD_ATTR_INIT(threadAttr);
  GOTO_SIM(); /* back to simulation mode */
  for (i = 1; i < numThread; i++) {
    THREAD_CREATE(threads[i], threadAttr, parallel_region, (void*)&thread_args[i]);
  }
  parallel_region(&thread_args[0]);
  for (i = 1; i < numThread; i++) {
    THREAD_JOIN(threads[i]);
  }
  THREAD_BARRIER_DESTROY(barrier_ptr);
  THREAD_BARRIER_FREE(barrier_ptr);
  free(thread_args);
  free(threads);
}

__TM_INLINE__ _tm_thread_context_t*
_tm_init_thread (void* _tm_thread_arg)
{
  _tm_thread_context_t *context = (_tm_thread_context_t *)_tm_thread_arg;
  STM_NEW_THREAD(&context->stm);
  return context;
}

__TM_INLINE__ void*
_tm_mixed_malloc_seq (unsigned int size)
{
  _tm_thread_context_t** mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (_tm_thread_context_t**)malloc(size + sizeof(_tm_thread_context_t*));
  if (!mem) {
    return 0;
  }
  mem[0] = 0; /* 0 indicates sequential malloc */
  return (void*)(mem+1);
}

__TM_INLINE__ void*
_tm_mixed_malloc_par (TM_ARGDECL  unsigned int size)
{
  _tm_thread_context_t** mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (_tm_thread_context_t**)memory_get(TM_GET_THREAD_ID(),
                                           size + sizeof(_tm_thread_context_t*));
  if (!mem) {
    return 0;
  }
  mem[0] = _tm_thread_context; /* store thread_context */
  return (void*)(mem+1);
}

__TM_INLINE__ void
_tm_mixed_free_seq (void* ptr)
{
  _tm_thread_context_t** mem = ((_tm_thread_context_t**)ptr) - 1;
  _tm_thread_context_t* allocator_thread_context = mem[0];
  if (allocator_thread_context == 0) {
    free(mem);
  } else {
    /*
     * TODO: do correct free call
     * allocater's thread context is allocator_thread_context
     */
  }
}

__TM_INLINE__ void
_tm_mixed_free_par (TM_ARGDECL  void* ptr)
{
  _tm_thread_context_t** mem = ((_tm_thread_context_t**)ptr) - 1;
  _tm_thread_context_t* allocator_thread_context = mem[0];
  if (allocator_thread_context == 0) {
#ifndef SIMULATOR
    free(mem); /* disable (simulator bug) */
#endif
  } else {
    /*
     * TODO: do correct free call
     * allocater's thread context is allocator_thread_context
     * deallocater's thread context is _tm_thread_context
     */
  }
}

#else /* SEQUENTIAL */

__TM_INLINE__ void
_tm_parallel (void(*parallel_region)(TM_THREAD_ARGDECL),
              void* arg_ptr,
              int numThread)
{
  if (IS_IN_SIM()) {
    GOTO_REAL(); /* exclude initialization from statistics */
  }
  memory_init(1, _tm_initial_memory_per_thread, 2);
  GOTO_SIM();
  parallel_region(arg_ptr);
}

__TM_INLINE__ void*
_tm_mixed_malloc_seq (unsigned int size)
{
  int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (int*)malloc(size + sizeof(int));
  if (!mem) {
    return 0;
  }
  mem[0] = -1; /* threadId==-1 indicates sequential malloc */
  return (void*)(mem+1);
}

__TM_INLINE__ void*
_tm_mixed_malloc_par (TM_ARGDECL  unsigned int size)
{
  int *mem; /* sizeof(mem) % 4 == 0 keeps alignment guarantees of malloc*/
  mem = (int*)memory_get(TM_GET_THREAD_ID(), size + sizeof(int));
  if (!mem) {
    return 0;
  }
  mem[0] = TM_GET_THREAD_ID(); /* store threadId */
  return (void*)(mem+1);
}

__TM_INLINE__ void
_tm_mixed_free_seq (void* ptr)
{
  int *mem = ((int*)ptr) - 1;
  int threadId = mem[0];
  if (threadId == -1) {
    free(mem);
  } else {
    /*
     * TODO: do correct free call
     * there is only one thread that could have allocated ptr with
     * memory_get => threadId == 0
     */
  }
}

__TM_INLINE__ void
_tm_mixed_free_par (TM_ARGDECL  void* ptr)
{
  int *mem = ((int*)ptr) - 1;
  int threadId = mem[0];
  if (threadId == -1) {
    /* free(mem); commented for fairness */
  } else {
    /*
     * TODO: do correct free call
     * there is only one thread that could have allocated ptr with
     * memory_get => threadId == 0
     */
  }
}

#endif /* SEQUENTIAL */


#include "tm_annotate.h" /* shared/local annoation of memory accesses */


#endif /* TM_H */


/* =============================================================================
 *
 * End of tm.h
 *
 * =============================================================================
 */
