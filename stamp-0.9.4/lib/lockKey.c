/* =============================================================================
 *
 * lockKey.c
 * -- associate 2-phase lock with each key
 *
 * =============================================================================
 */

#include "lockKey.h"
#include "stm.h"

lockKey_t*
lockKey_alloc()
{
  lockKey_t* self = (lockKey_t*)malloc(sizeof(lockKey_t));
  memset(&self->table, 0, LOCKKEY_TABLE_SIZE * sizeof(intptr_t));
  return self;
}

void
lockKey_free(lockKey_t* self)
{
  free(self);
}

void
lockKey_lock(TM_ARGDECL lockKey_t* self, int key)
{
  int ok;
  intptr_t* twoPhase = &(self->table[key & LOCKKEY_TABLE_MASK]);
  ok = STM_LOCK(twoPhase);
  if (ok) {
    STM_LOG_ABSTRACTLOCK(twoPhase);
  }
}

intptr_t* 
lockKey_get(TM_ARGDECL lockKey_t* self, int key)
{
  return &(self->table[key & LOCKKEY_TABLE_MASK]);
}

/* =============================================================================
 *
 * hold count locks
 *
 * =============================================================================
 */

#ifdef XBOOST_CHECKPOINTS

lockKey_t*
hclockKey_alloc()
{
  return lockKey_alloc();
}

void
hclockKey_free(lockKey_t* self)
{
  free(self);
}

int
hclockKey_lockIncr(TM_ARGDECL lockKey_t* self, int key)
{
  intptr_t* twoPhase = &(self->table[key & LOCKKEY_TABLE_MASK]);
  int r = STM_LOCK(twoPhase);
  if (HCLOCK_ACQUIRED(r) || HCLOCK_REACQUIRED(r)) {
    STM_LOG_ABSTRACTLOCK(twoPhase);
  }
  return r;
}

void
hclockKey_lockDecr(TM_ARGDECL lockKey_t* self, int key)
{
  intptr_t* twoPhase = &(self->table[key & LOCKKEY_TABLE_MASK]);
  STM_UNLOCK(twoPhase);
}

void
hclockKey_lockRelease(TM_ARGDECL lockKey_t* self, int key)
{
  intptr_t* twoPhase = &(self->table[key & LOCKKEY_TABLE_MASK]);
  STM_RELEASE_LOCK(twoPhase);
}

#endif


/* =============================================================================
 *
 * dreadlocks
 *
 * =============================================================================
 */

#ifdef XBOOST_CHECKPOINTS

dreadLockKey_t* dreadLockKey_alloc() {
  dreadLockKey_t* self = (dreadLockKey_t*)malloc(sizeof(dreadLockKey_t));
  memset(&self->table, 0, LOCKKEY_TABLE_SIZE * sizeof(dreadLock_t));
  return self;

 return lockKey_alloc(); }

void dreadLockKey_free(dreadLockKey_t* self) { free(self); }

void dreadLockKey_lock(TM_ARGDECL dreadLockKey_t* self, int key) {
  dreadLock_t* lock = &(self->table[key & LOCKKEY_TABLE_MASK]);
  int ok = STM_DL_LOCK(lock);
  if(ok)
    STM_LOG_DREADLOCK(lock);
}

#endif
