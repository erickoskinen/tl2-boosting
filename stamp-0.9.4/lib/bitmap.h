/* =============================================================================
 *
 * bitmap.h
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Author: Chi Cao Minh
 *
 * =============================================================================
 *
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#ifndef BITMAP_H
#define BITMAP_H 1


#include "types.h"


#ifdef __cplusplus
extern "C" {
#endif


typedef struct bitmap {
    int numBit;
    int numWord;
    uint_t* bits;
} bitmap_t;


/* =============================================================================
 * bitmap_alloc
 * -- Returns NULL on failure
 * =============================================================================
 */
bitmap_t*
bitmap_alloc (int numBit);


/* =============================================================================
 * bitmap_free
 * =============================================================================
 */
void
bitmap_free (bitmap_t* bitmapPtr);


/* =============================================================================
 * bitmap_set
 * -- Sets ith bit to 1
 * -- Returns TRUE on success, else FALSE
 * =============================================================================
 */
bool_t
bitmap_set (bitmap_t* bitmapPtr, int i);


/* =============================================================================
 * bitmap_clear
 * -- Clears ith bit to 0
 * -- Returns TRUE on success, else FALSE
 * =============================================================================
 */
bool_t
bitmap_clear (bitmap_t* bitmapPtr, int i);


/* =============================================================================
 * bitmap_isClear
 * -- Returns TRUE if ith bit is clear, else FALSE
 * =============================================================================
 */
bool_t
bitmap_isClear (bitmap_t* bitmapPtr, int i);


/* =============================================================================
 * bitmap_isSet
 * -- Returns TRUE if ith bit is set, else FALSE
 * =============================================================================
 */
bool_t
bitmap_isSet (bitmap_t* bitmapPtr, int i);


/* =============================================================================
 * bitmap_findClear
 * -- Returns index of first clear bit
 * -- If all bits are set, returns -1
 * =============================================================================
 */
int
bitmap_findClear (bitmap_t* bitmapPtr);


/* =============================================================================
 * bitmap_findSet
 * -- Returns index of first set bit
 * -- If all bits are clear, returns -1
 * =============================================================================
 */
int
bitmap_findSet (bitmap_t* bitmapPtr);


/* =============================================================================
 * bitmap_getNumClear
 * =============================================================================
 */
int
bitmap_getNumClear (bitmap_t* bitmapPtr);


/* =============================================================================
 * bitmap_getNumSet
 * =============================================================================
 */
int
bitmap_getNumSet (bitmap_t* bitmapPtr);


#ifdef __cplusplus
}
#endif


#endif /* BITMAP_H */


/* =============================================================================
 *
 * End of bitmap.h
 *
 * =============================================================================
 */
