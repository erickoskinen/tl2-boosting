/* =============================================================================
 *
 * vector.c
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Author: Chi Cao Minh
 *
 * =============================================================================
 *
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#include <stdlib.h>
#include "types.h"
#include "utility.h"
#include "vector.h"


/* =============================================================================
 * vector_alloc
 * -- Returns NULL if failed
 * =============================================================================
 */
vector_t*
vector_alloc (int initCapacity)
{
    vector_t* vectorPtr;
    int capacity = MAX(initCapacity, 1);

    vectorPtr = (vector_t*)malloc(sizeof(vector_t));

    if (vectorPtr != NULL) {
        vectorPtr->size = 0;
        vectorPtr->capacity = capacity;
        vectorPtr->elements = (void**)malloc(capacity * sizeof(void*));
        if (vectorPtr->elements == NULL) {
            return NULL;
        }
    }

    return vectorPtr;
}


/* =============================================================================
 * vector_free
 * =============================================================================
 */
void
vector_free (vector_t* vectorPtr)
{
    free(vectorPtr->elements);
    free(vectorPtr);
    vectorPtr = NULL;
}


/* =============================================================================
 * vector_at
 * -- Returns NULL if failed
 * =============================================================================
 */
void *
vector_at (vector_t* vectorPtr, int i)
{
    if ((i < 0) || (i >= vectorPtr->size)) {
        return NULL;
    }

    return (vectorPtr->elements[i]);
}


/* =============================================================================
 * vector_pushBack
 * -- Returns FALSE if fail, else TRUE
 * =============================================================================
 */
bool_t
vector_pushBack (vector_t* vectorPtr, void* dataPtr)
{
    if (vectorPtr->size == vectorPtr->capacity) {
        int i;
        int newCapacity = vectorPtr->capacity * 2;;
        void** newElements = (void**)malloc(newCapacity * sizeof(void*));
        if (newElements == NULL) {
            return FALSE;
        }
        vectorPtr->capacity = newCapacity;
        for (i = 0; i < vectorPtr->size; i++) {
            newElements[i] = vectorPtr->elements[i];
        }
        free(vectorPtr->elements);
        vectorPtr->elements = newElements;
    }

    vectorPtr->elements[vectorPtr->size++] = dataPtr;

    return TRUE;
}


/* =============================================================================
 * vector_popBack
 * Returns NULL if fail, else returns last element
 * =============================================================================
 */
void*
vector_popBack (vector_t* vectorPtr)
{
    if (vectorPtr->size < 1) {
        return NULL;
    }

    return (vectorPtr->elements[--(vectorPtr->size)]);
}


/* =============================================================================
 * vector_getSize
 * =============================================================================
 */
int
vector_getSize (vector_t* vectorPtr)
{
    return (vectorPtr->size);
}


/* =============================================================================
 * TEST_VECTOR
 * =============================================================================
 */
#ifdef TEST_VECTOR


#include <stdio.h>

static void
printVector (vector_t* vectorPtr)
{
    int i;
    int size = vector_getSize(vectorPtr);

    printf("[");
    for (i = 0; i < size; i++) {
        printf("%i ", *((int*)vector_at(vectorPtr, i)));
    }
    puts("]");
}


static void
insertInt (vector_t* vectorPtr, int* data)
{
    printf("Inserting: %i\n", *data);
    vector_pushBack(vectorPtr, (void*)data);
    printVector(vectorPtr);
}


static void
removeInt (vector_t* vectorPtr)
{
    printf("Removing: %i\n", *((int*)vector_popBack(vectorPtr)));
    printVector(vectorPtr);

}


int
main ()
{
    vector_t* vectorPtr;
    int data[] = {3, 1, 4, 1, 5, -1};
    int i;

    puts("Starting...");

    vectorPtr = vector_alloc(1);

    for (i = 0; data[i] >= 0; i++) {
        insertInt(vectorPtr, &data[i]);
    }

    while (i-- > 0) {
        removeInt(vectorPtr);
    }

    vector_free(vectorPtr);

    puts("Done.");

    return 0;
}


#endif /* TEST_VECTOR */


/* =============================================================================
 *
 * End of vector.c
 *
 * =============================================================================
 */
