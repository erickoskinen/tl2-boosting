/* =============================================================================
 *
 * thread.h
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Author: Chi Cao Minh
 *
 * =============================================================================
 *
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#ifndef THREAD_H
#define THREAD_H 1


#ifdef __cplusplus
extern "C" {
#endif


#include <pthread.h>
#include <stdlib.h>


#define THREAD_T       pthread_t
#define THREAD_ATTR_T  pthread_attr_t

#define THREAD_ATTR_INIT(attr) pthread_attr_init(&attr)
#define THREAD_CREATE(tid, attr, funcPtr, argPtr) \
    pthread_create(&(tid), &(attr), (void* (*)(void*))(funcPtr), (void*)(argPtr))
#define THREAD_JOIN(tid)  pthread_join(tid, (void**)NULL)

#define THREAD_MUTEX_T             pthread_mutex_t
#define THREAD_MUTEX_INIT(lock)    pthread_mutex_init(&(lock), NULL)
#define THREAD_MUTEX_LOCK(lock)    pthread_mutex_lock(&(lock))
#define THREAD_MUTEX_UNLOCK(lock)  pthread_mutex_unlock(&(lock))

#define THREAD_COND_T                 pthread_cond_t
#define THREAD_COND_INIT(cond)        pthread_cond_init(&(cond), NULL)
#define THREAD_COND_SIGNAL(cond)      pthread_cond_signal(&(cond))
#define THREAD_COND_BROADCAST(cond)   pthread_cond_broadcast(&(cond))
#define THREAD_COND_WAIT(cond, lock)  pthread_cond_wait(&(cond), &(lock))

#ifdef SIMULATOR
#  define THREAD_BARRIER_T             pthread_barrier_t
#  define THREAD_BARRIER_ALLOC(N)      ((pthread_barrier_t*)malloc(sizeof(pthread_barrier_t)))
#  define THREAD_BARRIER_INIT(bar, N)  pthread_barrier_init(bar, 0, N)
#  define THREAD_BARRIER(bar, tid)     pthread_barrier_wait(bar)
#  define THREAD_BARRIER_DESTROY(bar)  pthread_barrier_destroy(bar)
#  define THREAD_BARRIER_FREE(bar)     free(bar)
#else /* !SIMULATOR */
#  define THREAD_BARRIER_T             thread_barrier_t
#  define THREAD_BARRIER_ALLOC(N)      thread_barrier_alloc(N)
#  define THREAD_BARRIER_INIT(bar, N)  thread_barrier_init(bar)
#  define THREAD_BARRIER(bar, tid)     thread_barrier(bar, tid)
#  define THREAD_BARRIER_DESTROY(bar)  /* nothing */
#  define THREAD_BARRIER_FREE(bar)     thread_barrier_free(bar)
#endif /* !SIMULATOR */

typedef struct thread_barrier {
    THREAD_MUTEX_T countLock;
    THREAD_COND_T proceedCond;
    THREAD_COND_T proceedAllCond;
    int count;
    int numThread;
} thread_barrier_t;


/* =============================================================================
 * thread_barrier_alloc
 * =============================================================================
 */
thread_barrier_t*
thread_barrier_alloc (int numThreads);


/* =============================================================================
 * thread_barrier_free
 * =============================================================================
 */
void
thread_barrier_free (thread_barrier_t* barrierPtr);


/* =============================================================================
 * thread_barrier_init
 * =============================================================================
 */
void
thread_barrier_init (thread_barrier_t* barrierPtr);


/* =============================================================================
 * thread_barrier
 * -- Simple logarithmic barrier
 * =============================================================================
 */
void
thread_barrier (thread_barrier_t* barrierPtr, int threadId);


#ifdef __cplusplus
}
#endif


#endif /* THREAD_H */


/* =============================================================================
 *
 * End of thread.h
 *
 * =============================================================================
 */
