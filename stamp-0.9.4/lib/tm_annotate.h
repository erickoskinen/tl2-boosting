/* =============================================================================
 *
 * tm_annotate.h
 * -- Utility defines for annotating shared/managed memory accesses
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Authors: Chi Cao Minh and Martin Trautmann
 *
 * =============================================================================
 *
 * For the license of kmeans, please see kmeans/LICENSE.kmeans
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/mt19937ar.c and lib/mt19937ar.h, please see the
 * header of the files.
 * 
 * ------------------------------------------------------------------------
 * 
 * For the license of lib/rbtree.h and lib/rbtree.c, please see
 * lib/LEGALNOTICE.rbtree and lib/LICENSE.rbtree
 * 
 * ------------------------------------------------------------------------
 * 
 * Unless otherwise noted, the following license applies to STAMP files:
 * 
 * Copyright (c) 2007, Stanford University
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 * 
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 * 
 *     * Neither the name of Stanford University nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY STANFORD UNIVERSITY ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL STANFORD UNIVERSITY BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * =============================================================================
 */


#ifndef TM_ANNOTATE_H
#define TM_ANNOTATE_H 1


#ifndef TM_H
#  error This should only be included from tm.h
#endif

#include <assert.h>


/* =============================================================================
 * C interface
 * =============================================================================
 */

#ifndef __cplusplus /* C */

#  if defined(STM)

#    define TM_SHARED_READ(var)    STM_READ(var)
#    define TM_SHARED_READ_P(var)  STM_READ_PTR(var)
#    define TM_SHARED_READ_F(var)  STM_READ_FLOAT(var)

#    define TM_SHARED_WRITE(var, val)    STM_WRITE(var, val)
#    define TM_SHARED_WRITE_P(var, val)  STM_WRITE_PTR(var, val)
#    define TM_SHARED_WRITE_F(var, val)  STM_WRITE_FLOAT(var, val)

#    define TM_LOCAL_PERSISTENT_READ(var)    (var)
#    define TM_LOCAL_PERSISTENT_READ_P(var)  (var)
#    define TM_LOCAL_PERSISTENT_READ_F(var)  (var)

#    define TM_LOCAL_PERSISTENT_WRITE(var, val)    STM_LOCAL_WRITE(var, val)
#    define TM_LOCAL_PERSISTENT_WRITE_P(var, val)  STM_LOCAL_WRITE_PTR(var, val)
#    define TM_LOCAL_PERSISTENT_WRITE_F(var, val)  STM_LOCAL_WRITE_FLOAT(var, val)

/*
 * For local non-persistent and outside of transactions no annotation
 * is required, but these macros can be used for convenience
 */
#    define TM_LOCAL_READ(var)    (var)
#    define TM_LOCAL_READ_P(var)  (var)
#    define TM_LOCAL_READ_F(var)  (var)

#    define TM_LOCAL_WRITE(var, val)    var=val
#    define TM_LOCAL_WRITE_P(var, val)  var=val
#    define TM_LOCAL_WRITE_F(var, val)  var=val

#  else /* !STM */

#    define TM_SHARED_READ(var)    (var)
#    define TM_SHARED_READ_P(var)  (var)
#    define TM_SHARED_READ_F(var)  (var)

#    define TM_SHARED_WRITE(var, val)    ({var = val; var;})
#    define TM_SHARED_WRITE_P(var, val)  ({var = val; var;})
#    define TM_SHARED_WRITE_F(var, val)  ({var = val; var;})

#    define TM_LOCAL_PERSISTENT_READ(var)    (var)
#    define TM_LOCAL_PERSISTENT_READ_P(var)  (var)
#    define TM_LOCAL_PERSISTENT_READ_F(var)  (var)

#    define TM_LOCAL_PERSISTENT_WRITE(var, val)    ({var = val; var;})
#    define TM_LOCAL_PERSISTENT_WRITE_P(var, val)  ({var = val; var;})
#    define TM_LOCAL_PERSISTENT_WRITE_F(var, val)  ({var = val; var;})

/*
 * For local non-persistent and outside of transactions no annotation
 * is required, but these macros can be used for convenience
 */
#    define TM_LOCAL_READ(var)    (var)
#    define TM_LOCAL_READ_P(var)  (var)
#    define TM_LOCAL_READ_F(var)  (var)

#    define TM_LOCAL_WRITE(var, val)    ({var = val; var;})
#    define TM_LOCAL_WRITE_P(var, val)  ({var = val; var;})
#    define TM_LOCAL_WRITE_F(var, val)  ({var = val; var;})

#  endif /* !STM */


/* =============================================================================
 * C++ interface
 * =============================================================================
 */

#else  /* C++ */

#  define TM_SHARED_READ(var)      _tm_shared_read(TM_ARG var)
#  define TM_SHARED_READ_P(var)    _tm_shared_read_p(TM_ARG var)
#  define TM_SHARED_READ_F(var)    _tm_shared_read_f(TM_ARG var)
#  define TM_SHARED_READ_CPP(var)  _tm_shared_read_cpp(TM_ARG var)

#  define TM_SHARED_WRITE(var, val)      _tm_shared_write(TM_ARG var,val)
#  define TM_SHARED_WRITE_P(var, val)    _tm_shared_write_p(TM_ARG var,val)
#  define TM_SHARED_WRITE_F(var, val)    _tm_shared_write_f(TM_ARG var,val)
#  define TM_SHARED_WRITE_CPP(var, val)  _tm_shared_write_cpp(TM_ARG var,val)

#  define TM_LOCAL_PERSISTENT_READ(var)      _tm_local_persistent_read(TM_ARG var)
#  define TM_LOCAL_PERSISTENT_READ_P(var)    _tm_local_persistent_read_p(TM_ARG var)
#  define TM_LOCAL_PERSISTENT_READ_F(var)    _tm_local_persistent_read_f(TM_ARG var)
#  define TM_LOCAL_PERSISTENT_READ_CPP(var)  _tm_local_persistent_read_cpp(TM_ARG var)

#  define TM_LOCAL_PERSISTENT_WRITE(var, val) \
     _tm_local_persistent_write(TM_ARG var,val)
#  define TM_LOCAL_PERSISTENT_WRITE_P(var, val) \
     _tm_local_persistent_write_p(TM_ARG var,val)
#  define TM_LOCAL_PERSISTENT_WRITE_F(var, val) \
     _tm_local_persistent_write_f(TM_ARG var,val)
#  define TM_LOCAL_PERSISTENT_WRITE_CPP(var, val) \
     _tm_local_persistent_write_cpp(TM_ARG var,val)

/*
 * For local non-persistent and outside of transactions no annotation
 * is required, but these macros can be used for convenience
 */
#    define TM_LOCAL_READ(var)       (var)
#    define TM_LOCAL_READ_P(var)    (var)
#    define TM_LOCAL_READ_F(var)    (var)
#    define TM_LOCAL_READ_CPP(var)  (var)

#    define TM_LOCAL_WRITE(var, val)      ({var = val; var;})
#    define TM_LOCAL_WRITE_P(var, val)    ({var = val; var;})
#    define TM_LOCAL_WRITE_F(var, val)    ({var = val; var;})
#    define TM_LOCAL_WRITE_CPP(var, val)  ({var = val; var;})

inline intptr_t _tm_shared_read (TM_ARGDECL const intptr_t &var);
template<typename T>
inline T*       _tm_shared_read_p (TM_ARGDECL T* const &var);
inline float    _tm_shared_read_f (TM_ARGDECL const float &var);
template<typename T>
inline T        _tm_shared_read_cpp (TM_ARGDECL T const &var);

inline void     _tm_shared_write (TM_ARGDECL intptr_t &var, intptr_t val);
template<typename T>
inline void     _tm_shared_write_p (TM_ARGDECL T* &var, T* val);
inline void     _tm_shared_write_f (TM_ARGDECL float &var, float val);
template<typename T>
inline void     _tm_shared_write_cpp (TM_ARGDECL T &var, T val);

inline intptr_t _tm_local_persistent_read (TM_ARGDECL const intptr_t &var);
template<typename T>
inline T*       _tm_local_persistent_read_p (TM_ARGDECL T* const &var);
inline float    _tm_local_persistent_read_f (TM_ARGDECL const float &var);
template<typename T>
inline T        _tm_local_persistent_read_cpp (TM_ARGDECL T const &var);

inline void     _tm_local_persistent_write (TM_ARGDECL intptr_t &var, intptr_t val);
template<typename T>
inline void     _tm_local_persistent_write_p (TM_ARGDECL T* &var, T* val);
inline void     _tm_local_persistent_write_f (TM_ARGDECL float &var, float val);
template<typename T>
inline void     _tm_local_persistent_write_cpp (TM_ARGDECL T &var, T val);


/* =============================================================================
 * Reference classes that protect their referenced variable
 * =============================================================================
 */

template<typename _VT>
class TM_Shared_Ref {
public:
  TM_Shared_Ref (TM_ARGDECL _VT& ref) : ref(ref) { TM_MEMBER_INIT(); }
  operator _VT () {
    return TM_SHARED_READ_CPP(ref);
  }
  TM_Shared_Ref<_VT>& operator= (_VT val) {
    TM_SHARED_WRITE_CPP(ref, val);
    return *this;
  }
  TM_Shared_Ref<_VT>& operator= (const TM_Shared_Ref<_VT>& val) {
    TM_SHARED_WRITE_CPP(ref, val);
    return *this;
  }
private:
  _VT &ref;
  TM_MEMBERDECL;
};

template<typename _VT>
class TM_Local_Ref {
public:
  TM_Local_Ref (TM_ARGDECL _VT& ref) : ref(ref) {}
  operator _VT () {
    return ref;
  }
  TM_Local_Ref<_VT>& operator= (_VT val) {
    ref = val;
    return *this;
  }
  TM_Local_Ref<_VT>& operator= (const TM_Local_Ref<_VT>& val) {
    ref = val.ref;
    return *this;
  }
private:
  _VT &ref;
};

template<typename _VT>
class TM_Local_Persistent_Ref {
public:
  TM_Local_Persistent_Ref (TM_ARGDECL _VT& ref) : ref(ref) { TM_MEMBER_INIT(); }
  operator _VT() {
    return ref;
  }
  TM_Local_Persistent_Ref<_VT>& operator= (_VT val) {
    TM_LOCAL_PERSISTENT_WRITE_CPP(ref, val);
    return *this;
  }
  TM_Local_Persistent_Ref<_VT>& operator= (const TM_Local_Persistent_Ref<_VT>& val) {
    TM_LOCAL_PERSISTENT_WRITE_CPP(ref, val);
    return *this;
  }
private:
  _VT &ref;
  TM_MEMBERDECL;
};


/* =============================================================================
 * Implementing inline functions (STM)
 * =============================================================================
 */

#  if defined(STM)

/*
 * TM_SHARED_READ
 */

inline intptr_t
_tm_shared_read (TM_ARGDECL const intptr_t &var)
{
  return STM_READ(var);
}

template<typename T>
inline T*
_tm_shared_read_p (TM_ARGDECL T* const &var)
{
  return static_cast<T*>((void*)STM_READ((void* const&)(var)));
}

inline float
_tm_shared_read_f (TM_ARGDECL const float &var)
{
  /*
   * this is usually the case for 32-bit machines
   * in case of 64-bit code move to double
   */
  assert(sizeof(float)==sizeof(intptr_t));
  return STM_READ_FLOAT(var);
}


/*
 * TM_SHARED_WRITE
 */

inline void
_tm_shared_write (TM_ARGDECL intptr_t &var, intptr_t val)
{
  STM_WRITE(var,val);
}

template<typename T>
inline void
_tm_shared_write_p (TM_ARGDECL T* &var, T* val)
{
  STM_WRITE(var,val);
}

inline void
_tm_shared_write_f (TM_ARGDECL float &var, float val)
{
  /*
   * this us usually the case for 32-bit machines
   * in case of 64-bit code move to double
   */
  assert(sizeof(float)==sizeof(intptr_t));
  STM_WRITE_FLOAT(var,val);
}


/*
 * TM_LOCAL_PERSISTENT_READ
 */

inline intptr_t
_tm_local_persistent_read (TM_ARGDECL const intptr_t &var)
{
  return (var);
}

template<typename T>
inline T*
_tm_local_persistent_read_p (TM_ARGDECL T* const &var)
{
  return (var);
}

inline float
_tm_local_persistent_read_f (TM_ARGDECL const float &var)
{
  return (var);
}


/*
 * TM_LOCAL_PERSISTENT_WRITE
 */

inline void
_tm_local_persistent_write (TM_ARGDECL intptr_t &var, intptr_t val)
{
  STM_LOCAL_WRITE(var,val);
}

template<typename T>
inline void
_tm_local_persistent_write_p (TM_ARGDECL T* &var, T* val)
{
  STM_LOCAL_WRITE(var,val);
}

inline void
_tm_local_persistent_write_f (TM_ARGDECL float &var, float val)
{
  /*
   * this us usually the case for 32-bit machines
   * in case of 64-bit code move to double
   */
  assert(sizeof(float)==sizeof(intptr_t));
  STM_LOCAL_WRITE_FLOAT(var,val);
}


#  else /* !STM */


/* =============================================================================
 * Implementing inline functions (!STM)
 * =============================================================================
 */

/*
 * TM_SHARED_READ
 */

inline intptr_t
_tm_shared_read (TM_ARGDECL const intptr_t &var)
{
  return (var);
}

template<typename T>
inline T*
_tm_shared_read_p (TM_ARGDECL T* const &var)
{
  return (var);
}
inline float
_tm_shared_read_f (TM_ARGDECL const float &var)
{
  return (var);
}


/*
 * TM_SHARED_WRITE
 */

inline void
_tm_shared_write (TM_ARGDECL intptr_t &var, intptr_t val)
{
  var=val;
}

template<typename T>
inline void
_tm_shared_write_p (TM_ARGDECL T* &var, T* val)
{
  var=val;
}

inline void
_tm_shared_write_f (TM_ARGDECL float &var, float val)
{
  var=val;
}


/*
 * TM_LOCAL_PERSISTENT_READ
 */

inline intptr_t
_tm_local_persistent_read (TM_ARGDECL const intptr_t &var)
{
  return (var);
}

template<typename T>
inline T*
_tm_local_persistent_read_p (TM_ARGDECL T* const &var)
{
  return (var);
}

inline float
_tm_local_persistent_read_f (TM_ARGDECL const float &var)
{
  return (var);
}


/*
 * TM_LOCAL_PERSISTENT_WRITE
 */

inline void
_tm_local_persistent_write (TM_ARGDECL intptr_t &var, intptr_t val)
{
  var=val;
}

template<typename T>
inline void
_tm_local_persistent_write_p (TM_ARGDECL T* &var, T* val)
{
  var=val;
}

inline void
_tm_local_persistent_write_f (TM_ARGDECL float &var, float val)
{
  var=val;
}


#  endif /* !STM */


/* =============================================================================
 * Implementing inline functions (common)
 * =============================================================================
 */

/*
 * TM_SHARED_READ
 */

template<typename T>
inline T
_tm_shared_read_cpp (TM_ARGDECL T const &var)
{
  /*
   * If type T is not a pointer and no specialization for it exists,
   * this will cause an error
   */
   return _tm_shared_read_p(TM_ARG var);
}

template<>
inline intptr_t
_tm_shared_read_cpp<intptr_t> (TM_ARGDECL intptr_t const &var)
{
  return _tm_shared_read(TM_ARG var);
}

template<>
inline float
_tm_shared_read_cpp<float> (TM_ARGDECL float const &var)
{
  return _tm_shared_read_f(TM_ARG var);
}


/*
 * TM_SHARED_WRITE
 */

template<typename T>
inline void
_tm_shared_write_cpp (TM_ARGDECL T &var, T val)
{
  /*
   * If type T is not a pointer and no specialization for it exists,
   * this will cause an error
   */
  _tm_shared_write_p(TM_ARG var, val);
}

template<>
inline void
_tm_shared_write_cpp<intptr_t> (TM_ARGDECL intptr_t &var, intptr_t val)
{
  _tm_shared_write(TM_ARG var, val);
}

template<>
inline void
_tm_shared_write_cpp<float> (TM_ARGDECL float &var, float val)
{
  _tm_shared_write_f(TM_ARG var, val);
}


/*
 * TM_LOCAL_PERSISTENT_READ
 */

template<typename T>
inline T
_tm_local_persistent_read_cpp (TM_ARGDECL T const &var)
{
  /*
   * If type T is not a pointer and no specialization for it exists,
   * this will cause an error
   */
  return _tm_local_persistent_read_p(TM_ARG var);
}

template<>
inline intptr_t
_tm_local_persistent_read_cpp<intptr_t> (TM_ARGDECL intptr_t const &var)
{
  return _tm_local_persistent_read(TM_ARG var);
}

template<>
inline float
_tm_local_persistent_read_cpp<float> (TM_ARGDECL float const &var)
{
  return _tm_local_persistent_read_f(TM_ARG var);
}


/*
 * TM_LOCAL_PERSISTENT_WRITE
 */

template<typename T>
inline void
_tm_local_persistent_write_cpp (TM_ARGDECL T &var, T val)
{
  /*
   * If type T is not a pointer and no specialization for it exists,
   * this will cause an error
   */
  _tm_local_persistent_write_p(TM_ARG var, val);
}

template<>
inline void
_tm_local_persistent_write_cpp<intptr_t> (TM_ARGDECL intptr_t &var, intptr_t val)
{
  _tm_local_persistent_write(TM_ARG var, val);
}

template<>
inline void
_tm_local_persistent_write_cpp<float> (TM_ARGDECL float &var, float val)
{
  _tm_local_persistent_write_f(TM_ARG var, val);
}


#endif /* C++ */


#endif /* TM_ANNOTATE_H */


/* =============================================================================
 *
 * End of tm_annotate.h
 *
 * =============================================================================
 */
